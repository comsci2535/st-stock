<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart_order_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
        ->select('a.*,c.*')
        ->from('carts_order c')
        ->join('products a','a.product_id = c.product_id','left')
        ->order_by('c.updated_at', 'DESC')
        ->get();
        
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
        ->select('a.*,c.*')
        ->from('carts_order c')
        ->join('products a','a.product_id = c.product_id','left')
        ->get();

        return $query->num_rows();
    }

    public function get_cart_count($product_id) 
    {
        $query = $this->db
        ->select('c.*')
        ->from('carts_order c')
        ->where('c.product_id' , $product_id)
        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['keyword'])
            ->or_like('a.excerpt', $param['keyword'])
            ->or_like('a.detail', $param['keyword'])
//            ->or_like('c.customer_name', $param['keyword'])
//            ->or_like('c.sell_name', $param['keyword'])
            ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['search']['value'])
            ->or_like('a.excerpt', $param['search']['value'])
            ->or_like('a.detail', $param['search']['value'])
            ->or_like('c.customer_name', $param['search']['value'])
            ->or_like('c.sell_name', $param['search']['value'])
            ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
            ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['cart_id']) ) 
            $this->db->where('a.cart_id', $param['cart_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( isset($param['proarraId']) )
            $this->db->where_in('c.cart_id', $param['proarraId']);

        if(isset($param['cart_quantity']))
           $this->db->where('c.cart_quantity > ' , '0');

         if(isset($param['draw_cart']) )
           $this->db->where('c.draw > ' , '0');

    }
    

    public function insert($value) {
        $this->db->insert('carts_order', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
        ->where('product_id', $id)
        ->set('cart_quantity','cart_quantity+'.$value['cart_quantity'],FALSE)
        ->set('updated_at',$value['updated_at'])
        ->set('updated_by',$value['updated_by'])
        ->update('carts_order');
        return $query;
    }

    public function update_uncart($id, $product_id,$value)
    {
       $cart = $query = $this->db
        ->select('cart_quantity')
        ->from('carts_order c')
        ->where('c.cart_id ' , $id)
        ->get()->result();

        if($cart->cart_quantity == $value['cart_qty']){
            $query = $this->db->where('cart_id', $id);
            $this->db->delete('carts_order');
        }else{
          $query = $this->db
            ->where('cart_id ', $id)
            ->set('cart_quantity','cart_quantity-'.$value['cart_qty'],FALSE)
            ->update('carts_order');
        }
       
        return $query;
    }

    public function update_uncart_order($ids){
        foreach ($ids as  $id) {
             $query = $this->db->where('cart_id', $id);
             $this->db->delete('carts_order');
        }
    }

    public function update_draw($id,$value)
    {
       $query = $this->db
                        ->where('product_id', $id)
                        ->update('carts_order', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
        ->where_in('cart_id', $id)
        ->update('carts_order', $value);
        return $query;
    }  

    public function order_cart_remove($id){
        $query = $this->db
            ->where('cart_id ', $id)
            ->set('cart_quantity','cart_quantity-1')
            ->update('carts_order');
        return  $query;   
    }  

}
