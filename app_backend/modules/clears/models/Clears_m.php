<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Clears_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('products_order a')
                        ->join('products b', 'a.product_id = b.product_id')
                        ->get();
        return $query;
    }

    public function get_count() 
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('products_order a')
                        ->join('products b', 'a.product_id = b.product_id')
                        ->get();
        return $query->num_rows();
    }

    public function get_product_order_detail()
    {
        $query = $this->db
                ->select('*')
                ->from('products_order o')
                ->get();
        return $query;
    } 

    public function delete()
    {
        $query = $this->db->delete('products_order');
        return $query;
    }    

}
