<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main', 'method' => 'post')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="username">Username</label>
                    <div class="col-sm-7">
                        <input name="username" value="<?php echo isset($info->username) ? $info->username : NULL ?>" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="password">Password</label>
                    <div class="col-sm-7">
                        <input name="password" value="<?php echo isset($info->password) ? $info->password : NULL ?>" type="password" class="form-control" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="fname">ชื่อ นามสกุล</label>
                    <div class="col-sm-4">
                        <input name="fname" value="<?php echo isset($info->fname) ? $info->fname : NULL ?>" type="text" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <input name="lname" value="<?php echo isset($info->lname) ? $info->lname : NULL ?>" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="email">อีเมล</label>
                    <div class="col-sm-7">
                        <input name="email" value="<?php echo isset($info->email) ? $info->email : NULL ?>" type="email" class="form-control" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="phone">เบอร์โทรศัพท์</label>
                    <div class="col-sm-7">
                        <input name="phone" value="<?php echo isset($info->phone) ? $info->phone : NULL ?>" type="text" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="type" value="customer">
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->user_id) ? encode_id($info->user_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>




