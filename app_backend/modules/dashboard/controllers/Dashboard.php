<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
    private $_title = 'แผงควบคุม';
    private $_pageExcerpt = 'แสดงรายการโดยรวมของระบบ';
    private $_grpContent = 'grpContent';
    
    public function __construct() {
        parent::__construct();
        $this->load->model('orders/orders_m');
        $this->status=array('กำลังดำเนินการ','จัดเตรียม','ส่งแล้ว','ยกเลิก');
        $this->load->library('login/users_library');
        $this->load->model('logs/logs_model');
    }

    public function index() {
        $this->users_library->check_login();
        $this->load->module('template');
        
        if ( $this->session->firstTime )
            Modules::run('utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        
        $input['length']=100;
        $data['logLogin'] = $this->logs_model->getLogAllUser($input)->result();
        //arr( $data['logLogin']);exit();
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
             
        $this->template->layout($data);
    }

    public function data_orders(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['created_at'] = date('Y-m-d');
        $info = $this->orders_m->get_rows($input);
        $infoCount = $this->orders_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $arr_url = array();
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->order_id);

            $sum_order = $this->orders_m->get_sum_order_detail($rs->order_id);

            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
           
            if($rs->invoice):
                $arr_url[] = array(
                    'link' => site_url("{$this->router->class}/print/{$id}"),
                    'title' => 'ปริ้นใบกำกับภาษี',
                    'icon'  => 'la la-print'
                );
            endif;

            $arr_url[] = array(
                'link' => site_url("{$this->router->class}/printbox/{$id}"),
                'title' => 'ปริ้นใบติดกล่อง',
                'icon'  => 'la la-print'
            );

            $arr_url[] = array(
                'link' => site_url("{$this->router->class}/printcheck/{$id}"),
                'title' => 'ปริ้นใบจัดเตรียมสินค้า',
                'icon'  => 'la la-print'
            );

            $action[1][] = table_action($arr_url);
            unset($arr_url);

            switch ($rs->order_status) {
                case 0 : $style = '<span class="m-badge m-badge--default m-badge--wide">กำลังดำเนินการ</span>'; break; //กำลังดำเนินการ
                case 1 : $style = '<span class="m-badge m-badge--warning m-badge--wide">จัดเตรียม</span>'; break; //จัดเตรียม
                case 2 : $style = '<span class="m-badge m-badge--success m-badge--wide">ส่งแล้ว</span>'; break; //ส่งแล้ว
                case 3 : $style = '<span class="m-badge m-badge--danger m-badge--wide">ยกเลิก</span>'; break;  //ยกเลิก
                default: $style = '<span class="m-badge m-badge--default m-badge--wide">กำลังดำเนินการ</span>'; break;
            }
            $statusMethod = site_url("orders/action/status");
            $statusPicker = form_dropdown('statusPicker', $this->status, $rs->order_status, "class='form-control statusPicker' data-method='{$statusMethod}' data-id='{$id}' data-style='btn-flat {$style}'");

            $active = $rs->active ? "checked" : null;
            $status_print = '';
            if($rs->print_box > 0):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบติดกล่อง</span><br>';
            endif;
            if($rs->print_invoice > 0):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบกำกับภาษี</span><br>';
            endif;
            if($rs->print_check > 0):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบจัดเตรียมสินค้า</span>';
            endif;
            
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['order_code'] = $rs->order_code.'<br><small>'.$status_print.'</small>';
            $column[$key]['fullname'] = $rs->customer_fullname;
            $column[$key]['tel'] = $rs->customer_tel;
            $column[$key]['total'] = isset($sum_order->sum_price)? number_format($sum_order->sum_price) : 0;
            $column[$key]['total_list'] = isset($sum_order->sum_qty)? number_format($sum_order->sum_qty) : 0;
            $column[$key]['vat'] = number_format(($sum_total*0.07),2);
            $column[$key]['buy'] = number_format($sum_total - ($sum_total*0.07),2);
            // $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['active'] =  $style;
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    
}
