<div class="col-xl-12">
	<div class="m-portlet m-portlet--mobile ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						รายการสั่งซื้อวันนี้
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
		<div class="m-portlet__body">

			<table id="data-list" class="table table-hover dataTable responsive " width="100%">
				<thead>
					<tr>
						<th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
						<th>OrderID</th>
						<th>ชื่อ-นามสกุล</th>
						<th>เบอร์โทร</th>
						<th>จำนวนรายการ (หน่วย)</th>
						<th>จำนวนเงิน</th>
						<th>ภาษีมูลค่าเพิ่ม</th>
						<th>ราคาขาย</th>
						<th>สถานะ</th>
					</tr>
				</thead>
				<tfoot align="right">
					<tr>
						<th colspan="4" style="text-align:right"></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="col-xl-12">

	<!--begin:: Widgets/Audit Log-->
	<div class="m-portlet m-portlet--full-height ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Login Log
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
						<!-- <li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget4_tab1_content" role="tab">
								Today
							</a>
						</li> -->
						<!-- <li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab2_content" role="tab">
								Week
							</a>
						</li>
						<li class="nav-item m-tabs__item">
							<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget4_tab3_content" role="tab">
								Month
							</a>
						</li> -->
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="tab-content">
					<div class="tab-pane active" id="m_widget4_tab1_content">
						<div class="m-scrollable" data-scrollable="true" data-height="400" style="height: 400px; overflow: hidden;">
							<div class="m-list-timeline m-list-timeline--skin-light">
								<div class="m-list-timeline__items">
									<?php if(!empty($logLogin)){ foreach ($logLogin as $key => $rs) { ?>
										<div class="m-list-timeline__item">
											<span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
											<span class="m-list-timeline__text"><?=$rs->fullname?></span>
											<span class="m-list-timeline__time" style="width: 85px !important;"><?php echo get_timeago(strtotime($rs->created_at));?></span>
										</div>
									<?php } }?>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="m_widget4_tab2_content">
					</div>
					<div class="tab-pane" id="m_widget4_tab3_content">
					</div>
				</div>
			</div>
		</div>
		<!--end:: Widgets/Audit Log-->
	</div>
	