<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Draw_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        //$this->_condition($param);

        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        /*
        SELECT `d`.`product_id`, `p`.`title`, `p`.`product_color`, `p`.`product_code`, `p`.`barcode`, `p`.`active`, `p`.`product_img`, `p`.`product_quantity`, `c`.`cart_quantity`, SUM(`d`.`quantity`) AS `quantity_sum` FROM `orders_detail` `d` LEFT JOIN `orders` `o` ON `d`.`order_id` = `o`.`order_id` LEFT JOIN `products` `p` ON `d`.`product_id` = `p`.`product_id` LEFT JOIN `carts` `c` ON `d`.`product_id` = `c`.`product_id` WHERE `o`.`order_status` != 3 AND `p`.`active` = 1 AND d.draw IS NULL GROUP BY `d`.`product_id` ORDER BY `d`.`order_id` ASC LIMIT 10
         */
        
        $query = $this->db
        ->select('d.product_id,p.title,p.product_color,p.product_code,p.barcode,p.active,p.product_img,p.product_quantity,c.cart_quantity')
        ->select_sum('d.quantity','quantity_sum')
        ->from('orders_detail d')
        ->join('orders o','d.order_id = o.order_id','left')
        ->join('products p','d.product_id = p.product_id','left')
        ->join('carts c','d.product_id = c.product_id','left')
        ->where('o.order_status != 3')
        ->where('p.active = 1')
        ->where('d.draw', null, false)
        ->where('o.created_at > "2019-07-27 11:17"')
        ->order_by('d.order_id','ASC')
        ->group_by('d.product_id')
        ->get();

        return $query;
    }

    public function get_count($param) 
    {
     $query = $this->db
     ->select('d.product_id,p.title,p.product_color,p.product_code,p.barcode,p.active,p.product_img,p.product_quantity,c.cart_quantity')
     ->select_sum('d.quantity','quantity_sum')
     ->from('orders_detail d')
     ->join('orders o','d.order_id = o.order_id','left')
     ->join('products p','d.product_id = p.product_id','left')
     ->join('carts c','d.product_id = c.product_id','left')
     ->where('o.order_status != 3')
     ->where('p.active = 1')
     ->where('d.draw', null, false)
     ->where('o.created_at > "2019-07-27 11:17"')
     ->order_by('d.order_id','ASC')
     ->group_by('d.product_id')
     ->get();

     return $query->num_rows();
 }

 private function _condition($param) 
 {   
        // START form filter 
    if ( isset($param['keyword']) && $param['keyword'] != "" ) {
        $this->db
        ->group_start()
        ->like('a.title', $param['keyword'])
        ->or_like('a.excerpt', $param['keyword'])
        ->or_like('a.detail', $param['keyword'])
        ->group_end();
    }
    if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
        $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
    }
    if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
        $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
    }     
    if ( isset($param['active']) && $param['active'] != "" ) {
        $this->db->where('a.active', $param['active']);
    }         
        // END form filter

    if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
        $this->db
        ->group_start()
        ->like('a.title', $param['search']['value'])
        ->or_like('a.excerpt', $param['search']['value'])
        ->or_like('a.detail', $param['search']['value'])
        ->group_end();
    }

    if ( isset($param['order']) ){
        if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
        if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
        if ( $this->router->method =="data_index" ) {
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
            if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
        } else if ( $this->router->method =="data_trash" ) {
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
        }
        $this->db
        ->order_by($columnOrder, $param['order'][0]['dir']);
    } 

    if ( isset($param['repoId']) ) 
        $this->db->where('a.repoId', $param['repoId']);

    if ( isset($param['recycle']) )
        $this->db->where('a.recycle', $param['recycle']);

}

public function detailorder($arr_code='')
{
      /*SELECT o.order_id,d.order_detail_id,d.quantity,d.product_id ,SUM(d.quantity) AS quantity_sum
FROM orders_detail d
LEFT JOIN orders o ON o.order_id = d.order_id
WHERE  `o`.`order_status` = 0 AND d.draw IS NULL  AND d.product_id IN
(SELECT product_id FROM products WHERE active = 1)
GROUP BY d.product_id,d.order_detail_id
ORDER BY d.order_id*/

        // Sub Query
$this->db->select('product_id')->from('products')->where('active ',1)->where_in('product_id',$arr_code);
$subQuery =  $this->db->get_compiled_select();


$query = $this->db
->select('o.order_id,d.order_detail_id,d.quantity,d.product_id,o.order_code,o.customer_fullname,o.order_status,o.created_at')
->select_sum('d.quantity','quantity_sum')
->from('orders_detail d')
->join('orders o','d.order_id = o.order_id','left')
->join('products p','d.product_id = p.product_id','left')
->where('o.order_status !=',3)
->where('p.active',1)
->where('o.created_at > "2019-07-27 11:17"')
->where('d.draw', null, false)
->where("d.product_id IN ($subQuery)", NULL, FALSE)
->order_by('d.order_id','ASC')
->group_by('d.order_id')
->get();
return $query;
}

public function get_order_print() 
{
    $query = $this->db
    ->select('d.product_id,p.title,p.product_color,p.product_code,p.barcode,p.active,p.product_img,p.product_quantity,c.cart_quantity')
    ->select_sum('d.quantity','quantity_sum')
    ->from('orders_detail d')
    ->join('orders o','d.order_id = o.order_id','left')
    ->join('products p','d.product_id = p.product_id','left')
    ->join('carts c','d.product_id = c.product_id','left')
    ->where('o.order_status != 3')
    ->where('p.active = 1')
    ->where('o.created_at > "2019-07-27 11:17"')
    ->where('d.draw', null, false)
    ->order_by('d.order_id','ASC')
    ->group_by('d.product_id')
    ->get();

    return $query;
}

public function update_draw($ids)
{
    $ids_ = explode(",", $ids);

    $arr=array();

    foreach ($ids_ as $key => $item) {
       array_push($arr,$item);
    }
    
    $query_num_draw = $this->db
    ->select_max('draw_print','num_max')
    ->get('orders');

    $query_ck = $this->db
    ->select('order_id')
    ->from('orders')
    ->where('draw_date',date("Y-m-d h:i:sa"))
    ->get();

    $res2 = $query_num_draw->result_array();
    $result = $res2[0]['num_max'];

    $result+=1;

    $query = $this->db
    ->set('draw_print', $result)
    ->set('draw_date',date("Y-m-d h:i:sa"))
    ->where_in('order_id', $arr)
    ->update('orders');

        $query_detail = $this->db
        ->set('draw','Y')
        ->where_in('order_id', $id)
        ->update('orders_detail');


        return $query;
    }

}
