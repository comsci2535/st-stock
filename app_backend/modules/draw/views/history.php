
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>สินค้า</th>
                            <th>รหัสบาร์โค๊ด</th>
                            <th>รูป</th>
                            <th>จำนวนสินค้าที่อยู่ในสต๊อก</th>
                            <th>จำนวนในการเบิก</th>
                            <th>จำนวนคงเหลือปัจจุบัน</th>
                            
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>