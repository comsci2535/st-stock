<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     <?php echo $breadcrumb[1][0]?>
                 </h3>
             </div>
         </div>
         <div class="m-portlet__head-tools">

         </div>
     </div>


     <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
     <div class="m-portlet__body">
        <div class="col-12">

            <?php //print_r($datas)?>

            <table class="table table-bordered">
                <thead class="table-primary">
                  <tr>
                    <th>OrderID</th>
                    <th>ชื่อ - นามสกุล</th>
                    <th>สถานะ</th>
                    <th>วันที่</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($datas as $key => $data) {
                   ?>
                   <tr class="clickable table-info" data-toggle="collapse" data-target="#group-of-rows-<?=$key?>" aria-expanded="false" aria-controls="group-of-rows-<?=$key?>">
                    <td><i class="fa fa-plus" aria-hidden="true"></i> <?=$data['order_code']?></td>
                    <td><?=$data['customer_fullname']?></td>
                    <td><?=$data['order_status']?></td>  
                    <td><?=$data['created_at']?></td>
                </tr>
            </tbody>
            <tbody id="group-of-rows-<?=$key?>" class="collapse">
                <?php foreach ($data['order_detail'] as $i => $value) { ?>
                    <tr>
                        <td colspan="2">&nbsp;&nbsp;&nbsp;- <?=$value->title." (".$value->product_color.")"?></td>
                        <td>จำนวน <?=$value->quantity?> รายการ</td>
                        <td>ราคาขาย <?=$value->product_price?> บาท</td>  
                    </tr>
                <?php }?>
            </tbody>
        <?php }?>
    </table>



</div>


</div>
<div class="m-portlet__foot m-portlet__foot--fit">

    <div class="m-form__actions">
        <div class="row">
            <div class="col-2">
            </div>
            <div class="col-10">
                <!-- <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button> -->
            </div>
        </div>

    </div>
</div>
<?php echo form_close() ?>

<!--end::Form-->
</div>



</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->repoId;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->repoId;?>';

</script>




