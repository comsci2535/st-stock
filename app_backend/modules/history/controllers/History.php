<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class History extends MX_Controller {

    private $_title = "ประวัติเบิกสต๊อก";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ___ประวัติเบิกสต๊อก";
    private $_grpContent = "history";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->library('m_pdf');
        $this->load->model("history_m");
        $this->load->model("orders/orders_m");

        $this->status=array('รับออเดอร์','แพ็คสินค้า','ส่งแล้ว','ยกเลิก');
    }
    
    public function index(){

        $this->load->module('template');
        

        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ประวัติการ".$this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/history";
        
        $this->template->layout($data);

    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->history_m->get_rows($input);
        $infoCount = $this->history_m->get_count($input);
        $column = array();


        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->draw_print);
            $action = array();
            $action[1][] = table_view(site_url("{$this->router->class}/view/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['draw_date'] = $rs->draw_date == null ? '-' : DateThai($rs->draw_date);
            $column[$key]['draw_print'] = $rs->draw_print;
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);

        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function view($id=""){
        $this->load->module('template');

        $id = decode_id($id);

        $info = $this->history_m->get_order_print($id);

        if ($info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $row = $info->result();

        $data['datas'] = $row;

        $product_arr = array();
        foreach($row as $key => $value){
            array_push($product_arr, $value->product_id);
        }

        $info_detail = $this->history_m->detailorder($product_arr);

        $order_detail = $info_detail->result();

        $order_arr = array();
        foreach ($order_detail as $key => $item) {
         array_push($order_arr, $item->order_id);
     }

     $data['order_list'] =  $order_arr;
     $data['draw_date'] = $row[0]->draw_date;


     // page detail
     $data['pageHeader'] = $this->_title;
     $data['pageExcerpt'] = $this->_pageExcerpt;
     $data['contentView'] = "{$this->router->class}/print";
     $data['pageScript'] = "scripts/backend/draw/print.js";

     $this->template->layout($data);
 }

 public function drowdetail()
 {
    $this->load->module('template');

    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ดูรายละเอียดรายการสั่งซื้อ', site_url("{$this->router->class}/drowdetail"));

    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $product_arr = array();
    foreach($arrayId as $key => $item_id){
        $input['product_id'] = decode_id($item_id);
        array_push($product_arr, $input['product_id']);
    }
    $info = $this->draw_m->detailorder($product_arr);

    $column = array();
    foreach ($info->result() as $key => $rs) {
     $column[$key]['order_code'] = $rs->order_code;
     $column[$key]['customer_fullname'] = $rs->customer_fullname;
     $column[$key]['order_status'] = $this->status[$rs->order_status];
     $column[$key]['created_at'] = $rs->created_at;
            //ดึง order detail
     $info_detail = $this->orders_m->get_order_detail($rs->order_id);
     $info_detail = $info_detail->result();
     $column[$key]['order_detail'] = $info_detail;
 }

 $data['datas'] = $column;

         // page detail
 $data['pageHeader'] = $this->_title;
 $data['pageExcerpt'] = $this->_pageExcerpt;
 $data['contentView'] = "{$this->router->class}/preview";

 $this->template->layout($data);

}

public function drowprint()
{
    

}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->Draw_m->get_rows($input);
    $infoCount = $this->Draw_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->repoId);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}    

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();
        foreach ( $input['id'] as &$rs ) 
            $rs = decode_id($rs);
        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;
        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->Draw_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->Draw_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->Draw_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->Draw_m->update_in($input['id'], $value);
        }   
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    }
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));        
}  

public function sumernote_img_upload(){
        		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
   $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
}

echo $picture;

}

public function deletefile($ids){

  $arrayName = array('file' => $ids);

  echo json_encode($arrayName);
}

}
