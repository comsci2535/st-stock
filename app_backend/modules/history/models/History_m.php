<?php
defined('BASEPATH') or exit('No direct script access allowed');

class History_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        //$this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        // SELECT * FROM `orders` WHERE NOT `draw_print` =0 GROUP BY `draw_print` ORDER by `draw_print` ASC
        
        $query = $this->db
                       ->select('*')
                        ->from('orders')
                        ->where('draw_print !=  0')
                        ->order_by('draw_print','desc')
                        ->group_by('draw_print')
                        ->get();
                        
        return $query;
    }

    public function get_count($param) 
    {
       $query = $this->db
                       ->select('*')
                        ->from('orders')
                        ->where('draw_print !=  0')
                        ->order_by('draw_print','desc')
                        ->group_by('draw_print')
                        ->get();


        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['repoId']) ) 
            $this->db->where('a.repoId', $param['repoId']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
   public function detailorder($arr_code='')
   {
      /*SELECT o.order_id,d.order_detail_id,d.quantity,d.product_id ,SUM(d.quantity) AS quantity_sum
FROM orders_detail d
LEFT JOIN orders o ON o.order_id = d.order_id
WHERE  `o`.`order_status` = 0 AND d.draw IS NULL  AND d.product_id IN
(SELECT product_id FROM products WHERE active = 1)
GROUP BY d.product_id,d.order_detail_id
ORDER BY d.order_id*/

        // Sub Query
        $this->db->select('product_id')->from('products')->where('active ',1)->where_in('product_id',$arr_code);
        $subQuery =  $this->db->get_compiled_select();
         

        $query = $this->db
                        ->select('o.order_id,d.order_detail_id,d.quantity,d.product_id,o.order_code,o.customer_fullname,o.order_status,o.created_at,o.draw_date')
                        ->select_sum('d.quantity','quantity_sum')
                        ->from('orders_detail d')
                        ->join('orders o','d.order_id = o.order_id','left')
                        ->join('products p','d.product_id = p.product_id','left')
                        ->where('o.order_status !=',3)
                        ->where('p.active',1)
                        ->where('o.created_at > "2019-07-27 11:17"')
                        ->where('d.draw', null, false)
                        ->where("d.product_id IN ($subQuery)", NULL, FALSE)
                        ->order_by('d.order_id','ASC')
                        ->group_by('d.order_id')
                        ->get();
             
        return $query;
   }

   public function get_order_print($draw_print="") 
    {
        $query = $this->db
                        ->select('d.product_id,p.title,p.product_color,p.product_code,p.barcode,p.active,p.product_img,p.product_quantity,c.cart_quantity,o.draw_date')
                        ->select_sum('d.quantity','quantity_sum')
                        ->from('orders_detail d')
                        ->join('orders o','d.order_id = o.order_id','left')
                        ->join('products p','d.product_id = p.product_id','left')
                        ->join('carts c','d.product_id = c.product_id','left')
                        ->where('o.order_status != 3')
                        ->where('p.active = 1')
                        ->where('o.created_at > "2019-07-27 11:17"')
                        ->where('o.draw_print',$draw_print)
                        ->order_by('d.order_id','ASC')
                        ->group_by('d.product_id')
                        ->get();
                        
        return $query;
    }

    public function update_draw($id)
    {
        $query = $this->db
        ->set('draw_print', 'Y')
        ->where('order_id', $id)
        ->update('orders');

        $query_detail = $this->db
        ->set('draw', 'Y')
        ->where('order_id', $id)
        ->update('orders_detail');

        return $query;
    }

}
