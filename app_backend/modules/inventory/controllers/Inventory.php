<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Inventory extends MX_Controller {

    private $_title = "inventory";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ___ประวัติ";
    private $_grpContent = "inventory";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->library('m_pdf');
        $this->load->model("inventory_m");
        $this->load->model("products/products_m");
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[0][] = action_stock_print(site_url("{$this->router->class}/printstock"));
        
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->inventory_m->get_product_all($input);
        $infoCount = $this->inventory_m->get_count_all($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }

        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->product_id);
            $action = array();
            $action[1][] = table_view(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            // $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['img'] = '<img src="'.$this->config->item('root_url').$rs->product_img.'" class="img-thumbnail" style="width:100px !important;max-width:100px!important;">';
            $column[$key]['title'] = $rs->title." (".$rs->product_color.")";
            $column[$key]['product_quantity'] = $rs->product_quantity;
            $column[$key]['product_price_cost'] = $rs->product_price_cost;   
            //$column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function data_view(){
        $input = $this->input->post();
        $input['id'] = decode_id($input['id']);
        $input['recycle'] = 0;
        $info = $this->inventory_m->get_rows($input);
        $infoCount = $this->inventory_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->id);
            $order_id = encode_id($rs->order_id);
            $action = array();
            if(!empty($rs->order_id)){
              $action[1][] = table_view_detail(site_url("orders/edit/{$order_id}"));
          }else{
             $action[1][] = $rs->message;  
         }
         $active = $rs->active ? "checked" : null;
         $column[$key]['DT_RowId'] = $id;
         $column[$key]['qty'] = $rs->qty;
         $column[$key]['type'] = $rs->type;
         $column[$key]['created_at'] = datetime_table($rs->created_at);
         $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
     }
     $data['data'] = $column;
     $data['recordsTotal'] = $info->num_rows();
     $data['recordsFiltered'] = $infoCount;
     $data['draw'] = $input['draw'];
     $this->output
     ->set_content_type('application/json')
     ->set_output(json_encode($data));
 }

 public function create(){
    $this->load->module('template');

    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/save");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/form";

    $this->template->layout($data);
}

public function save(){
    $input = $this->input->post(null, true);
    $value = $this->_build_data($input);
    $result = $this->inventory_m->insert($value);
    if ( $result ) {
     Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
 } else {
     Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
 }
 redirect(site_url("{$this->router->class}"));
}

public function edit($id=""){
    $this->load->module('template');

    $data['id'] = $id;
    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/update");

    $id = decode_id($id);
    $input['product_id'] = $id;
    $info = $this->products_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ประวัติการทำรายการ', site_url("{$this->router->class}/edit"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/view";

    $data['pageScript'] = "scripts/backend/inventory/view.js";

    $this->template->layout($data);
}

public function update(){
    $input = $this->input->post(null, true);
    $id = decode_id($input['id']);
    $value = $this->_build_data($input);
    $result = $this->inventory_m->update($id, $value);
    if ( $result ) {
     Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
 } else {
     Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
 }
 redirect(site_url("{$this->router->class}"));
}

private function _build_data($input){

    $value['title'] = $input['title'];
    $value['excerpt'] = $input['excerpt'];
    $value['detail'] = $input['detail'];
    if ( $input['mode'] == 'create' ) {
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $this->session->users['user_id'];
    } else {
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
    }
    return $value;
}

public function excel(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->inventory_m->get_rows($input);
    $fileName = "inventory";
    $sheetName = "Sheet name";
    $sheetTitle = "Sheet title";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $styleH = excel_header();
    $styleB = excel_body();
    $colums = array(
        'A' => array('data'=>'รายการ', 'width'=>50),
        'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
        'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
    );
    $fields = array(
        'A' => 'title',
        'B' => 'excerpt',
        'C' => 'created_at',            
        'D' => 'updated_at',
    );
    $sheet->setTitle($sheetName);

        //title
    $sheet->setCellValue('A1', $sheetTitle);
    $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
    $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    $sheet->getRowDimension(1)->setRowHeight(20);

        //header
    $rowCount = 2;
    foreach ( $colums as $colum => $data ) {
        $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
        $sheet->SetCellValue($colum.$rowCount, $data['data']);
        $sheet->getColumnDimension($colum)->setWidth($data['width']);
    }

        // data
    $rowCount++;
    foreach ( $info->result() as $row ){
        foreach ( $fields as $key => $field ){
            $value = $row->{$field};
            if ( $field == 'created_at' || $field == 'updated_at' )
                $value = datetime_table ($value);
            $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
            $sheet->SetCellValue($key . $rowCount, $value); 
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $rowCount++;
    }
    $sheet->getRowDimension($rowCount)->setRowHeight(20);  
    $writer = new Xlsx($spreadsheet);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
    $writer->save('php://output');
    exit; 
}

public function pdf(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }

    $info = $this->inventoryrts_m->get_rows($input);

    $data['info'] = $info->result();

        //$data = [];
        //load the view and saved it into $html variable
    $html=$this->load->view("{$this->router->class}/pdf", $data, true);

        //this the the PDF filename that user will get to download
    $pdfFilePath = date('Ymdhis')."-order.pdf";

        //load mPDF library
    $this->load->library('m_pdf');

       //generate the PDF from the given html
    $this->m_pdf->pdf->WriteHTML($html);

        //download it.
    $this->m_pdf->pdf->Output($pdfFilePath, "D");   
}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->inventory_m->get_rows($input);
    $infoCount = $this->inventory_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->repoId);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}    

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();
        foreach ( $input['id'] as &$rs ) 
            $rs = decode_id($rs);
        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;
        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->inventory_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->inventory_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->inventory_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->inventory_m->update_in($input['id'], $value);
        }   
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    }
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));        
}  

public function sumernote_img_upload(){
		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
     $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
 }

 echo $picture;

}

public function deletefile($ids){

  $arrayName = array('file' => $ids);

  echo json_encode($arrayName);
}

public function printstock()
{
    $this->load->module('template');

    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้นสินค้าคงคลัง', site_url("{$this->router->class}/printstock"));

    $input = $this->input->post();

    $info = $this->inventory_m->get_product_all($input);
    $infoCount = $this->inventory_m->get_count_all($input);

    $stock = $info->result();

    $data['datas'] =  $stock;
    
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print";
    $data['pageScript'] = "scripts/backend/{$this->router->class}/print.js";

    $this->template->layout($data);

}



public function inventory_history($type , $ids ,$data,$mesesge=NULL){
    $arrayId = array();
    $arrayValue = array();
    if($type == 'add'){
        for ($i=0; $i < sizeof($data); $i++) { 
            array_push($arrayId, $ids+$i);
        }
        var_dump($arrayId);
        foreach ($data as $key => $value) {
            array_push($arrayValue,
                array(
                    'product_id' => $arrayId[$key], 
                    'product_quantity' => $value['product_quantity'],
                    'created_at'=> db_datetime_now(),
                    'created_by'=> $this->session->users['user_id'],
                    'type' => 'add',
                    'message' => "เพิ่มสินค้าใหม่ โดย ".$this->session->users['Name']
                )
            );
        } 
        $result = $this->inventory_m->insert_batch($arrayValue); 
    }else if($type == 'append' && !empty($data)){
        array_push($arrayValue,
            array(
                'product_id' => $ids, 
                'product_quantity' => (int)$data,
                'created_at'=> db_datetime_now(),
                'created_by'=> $this->session->users['user_id'],
                'type' => 'append',
                'message' => $mesesge
            )
        );
        $result = $this->inventory_m->insert_batch($arrayValue); 
    }else if($type == 'cut' && !empty($data)){
        array_push($arrayValue,
            array(
                'product_id' => $ids, 
                'product_quantity' => (int)$data,
                'created_at'=> db_datetime_now(),
                'created_by'=> $this->session->users['user_id'],
                'type' => 'cut',
                'message' => $mesesge."โดย ".$this->session->users['Name']
            )
        );
        $result = $this->inventory_m->insert_batch($arrayValue); 
    }else if($type == 'sell'){

     foreach ($ids as $key => $value) {

        array_push($arrayValue,
            array(
                'product_id' => $value->product_id, 
                'product_quantity' => "-".$value->quantity,
                'created_at'=> db_datetime_now(),
                'created_by'=> $this->session->users['user_id'],
                'type' => 'sell',
                'order_detail_id' => $value->order_detail_id,
                'message' => "ขายสินค้า โดย ".$this->session->users['Name']
            )
        );

    }
    $result = $this->inventory_m->insert_batch($arrayValue);
}else if($type == 'cancle'){
 foreach ($data as  $value) {
    array_push($arrayValue,
        array(
            'product_id' => $value['product_id'], 
            'product_quantity' => $value['quantity'],
            'created_at'=> db_datetime_now(),
            'created_by'=> $this->session->users['user_id'],
            'type' => 'cancle',
            'order_detail_id' => $value['order_detail_id'],
            'message' => "ยกเลิกสินค้า โดย ".$this->session->users['Name']
        )
    );
}

$result = $this->inventory_m->insert_batch($arrayValue);
}else if($type == 'cart'){
    array_push($arrayValue,
        array(
            'product_id' => $ids, 
            'product_quantity' => "-".$data,
            'created_at'=> db_datetime_now(),
            'created_by'=> $this->session->users['user_id'],
            'type' => 'cart',
            'message' => $mesesge
        )
    );
    $result = $this->inventory_m->insert_batch($arrayValue); 
}else if($type == 'uncart'){
    array_push($arrayValue,
        array(
            'product_id' => $ids, 
            'product_quantity' => $data,
            'created_at'=> db_datetime_now(),
            'created_by'=> $this->session->users['user_id'],
            'type' => 'uncart',
            'message' => $mesesge." โดย ".$this->session->users['Name']
        )
    );
    $result = $this->inventory_m->insert_batch($arrayValue); 
}else if($type == 'draw'){
    array_push($arrayValue,
        array(
            'product_id' => $ids, 
            'product_quantity' => $data,
            'created_at'=> db_datetime_now(),
            'created_by'=> $this->session->users['user_id'],
            'type' => 'draw',
            'message' => $mesesge
        )
    );
    $result = $this->inventory_m->insert_batch($arrayValue); 
}

}

}
