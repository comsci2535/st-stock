<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inventory_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_product_all($param)
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,c.cart_quantity')
                        ->from('products a')
                        ->join('carts_order c' , 'a.product_id = c.product_id', 'LEFT')
                        ->order_by('a.product_id', 'desc')
                        ->group_by('a.product_id')
                        ->get();
        return $query;
    }

    public function get_count_all(){
        $this->_condition($param);
         
        $query = $this->db
                        ->select('a.*,c.cart_quantity')
                        ->from('products a')
                        ->join('carts_order c' , 'a.product_id = c.product_id', 'LEFT')
                        ->order_by('a.product_id', 'desc')
                        ->group_by('a.product_id')
                        ->get();

        return $query->num_rows();
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('b.id,b.created_at,b.product_quantity as qty ,o.order_id,b.type,b.message')
                        ->from('inventory b')
                        ->join('products a' , 'a.product_id = b.product_id', 'LEFT')
                        ->join('orders_detail d' , 'b.order_detail_id = d.order_detail_id', 'LEFT')
                        ->join('orders o' , 'o.order_id = d.order_id', 'LEFT')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
         
        $query = $this->db
                        ->select('b.id,b.created_at,b.product_quantity as qty ,o.order_id,b.type,b.message')
                        ->from('inventory b')
                        ->join('products a' , 'a.product_id = b.product_id', 'LEFT')
                        ->join('orders_detail d' , 'b.order_detail_id = d.order_detail_id', 'LEFT')
                        ->join('orders o' , 'o.order_id = d.order_id', 'LEFT')
                        ->get();
        // echo $this->db->last_query();
        // exit();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.product_price_cost', $param['keyword'])
                    ->or_like('a.product_quantity', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(b.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }

        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        /*if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.created_at', $param['search']['value'])
                    ->or_like('b.type', $param['search']['value'])
                    ->or_like('b.message', $param['search']['value'])
                    ->group_end();
        }*/
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db->like('a.title', $param['search']['value']);
        }    

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 0) $columnOrder = "b.created_at";
            if ($param['order'][0]['column'] == 1) $columnOrder = "b.product_quantity";
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.type";                 
          
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['id']) ) 
            $this->db->where('a.product_id', $param['id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('inventory', $value);
        return $this->db->insert_id();
    }

    public function insert_batch($value) {
        $this->db->insert_batch('inventory', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('id', $id)
                        ->update('inventory', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('id', $id)
                        ->update('inventory', $value);
        return $query;
    }    

}
