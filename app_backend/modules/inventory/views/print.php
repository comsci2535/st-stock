<style type="text/css">
  body {
    background: rgb(204,204,204); 
  }
  .book{
    margin: 0 auto;
  }
  page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
    width: 21cm;
    height: auto; 
    font-size: 14px;
  }
  page h4{
    font-size: 14px;
  }
  page .bd-tb{
    border-top: 1px solid;
    border-bottom: 1px solid;
  }
  page .bd-b{
    border-bottom: 1px solid #eee;
  }
  page .bd-t{
    border-top: 0.5px solid;
  }
  .table-bordered th, .table-bordered td {
    border: 1px solid #607D8B;
  }
  .table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #607D8B;
  }
</style>
<div class="col-md-12">
  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
           <?php echo $breadcrumb[1][0]?>
         </h3>
       </div>
     </div>
     <div class="m-portlet__head-tools">

      <div class="m-portlet__head-tools">
        <input type="hidden" name="ID[]" value="<?=$arrayId?>">
        <div class="btn-group mr-2" role="group" aria-label="1 group">
          <button type='button' onclick="printDrow()" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
        </div>
      </div>

    </div>
  </div>


  <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
  <div class="m-portlet__body">
    <div class="col-12">

      <div class="form-group m-form__group row">
        <div class="book" id="stock">
          <style type="text/css">
            @media print {
              table, th, td
              {
                border-collapse:collapse;
                border: 1px solid black;
                padding: 5px;
              }
              table{
                width:100%;
              }
              .text-right{
                text-align: right !important;
              }
            }
          </style>
          <page size="A4">
            <div class="page col-12 pt-3">
              <h6 class="text-right">วันที่ <?=DateThai(date('d-m-Y H:i:s'))?></h6>
              <input type="hidden" class="order_list_p" name="p_num[]" value="<?=$order_list_p?>">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">รายการ</th>
                    <th scope="col">จำนวนที่นับได้</th>
                    <th scope="col">จำนวนสินค้าคงคลัง</th>
                    <th scope="col">จำนวนที่คลาดเคลื่อน</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($datas as $key => $value) { ?>
                    <tr>
                      <th scope="row" width="5%"><?=$key+1?></th>
                      <td width="50%"><?=$value->title?></td>
                      <td width="15%"></td>
                      <td width="15%" style="text-align:right;"><?=$value->product_quantity +  $value->cart_quantity;?></td>
                      <td width="15%"></td>
                    </tr>
                  <?php } ?>

                </tbody>
              </table>
            </div>
          </page>

        </div>
      </div>





    </div>


  </div>
  <div class="m-portlet__foot m-portlet__foot--fit">

    <div class="m-form__actions">
      <div class="row">
        <div class="col-2">
        </div>
        <div class="col-10">
          <!-- <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button> -->
        </div>
      </div>

    </div>
  </div>
  <?php echo form_close() ?>

  <!--end::Form-->
</div>



</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->repoId;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->repoId;?>';

  </script>




