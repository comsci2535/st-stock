
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ชื่อสินค้า : <?php echo !empty($info->title) ? $info->title.' ('.$info->product_color.')' : '';?> , จำนวนสินค้าคงคลัง : <?php echo !empty($info->product_quantity) ? $info->product_quantity : 0;?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label">วันที่</label>
                <div class="col-4">
                    <input type="text" name="dateRange" class="form-control" value="" autocomplete="off"/>
                    <input type="hidden" name="createStartDate"  value=""/>
                    <input type="hidden" name="createEndDate"  value=""/>
                </div>
            </div>
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>วันที่</th>
                            <th>จำนวนสินค้า</th>
                            <th>สถานะ</th>
                            <th>รายละเอียด</th>
                        </tr>
                    </thead>
                </table>
            </form>
            <input type="hidden" id="text-id" value="<?php echo !empty($id)? $id : 0;?>">
        </div>
    </div>
</div>