<?php 

/**
 * 
 */
class Logs_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function insert_log($username,$class,$method,$messege)
	{

		$ip = $this->input->ip_address();
		$data = array(
			'username'   => $username, 
			'ip_address' => $ip, 
			'class'      => $class,
			'method'     => $method,
			'messege'    => $messege, 
			'created_at' => date('Y-m-d H:i:s')
		);

		$this->db->insert('backend_logs',$data);

		return $this->db->insert_id();
	}

	public function getLogAll() {

		$query = $this->db->get('backend_logs');

		return $query->result();
	}

	public function getLogAllUser($param) {

		if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $this->db->order_by('a.created_at','desc');
        $query = $this->db
                        ->select('a.*,b.fullname')
                        ->from('backend_logs a')
                        ->join('users b','b.username=a.username','inner')
                        ->get();
        return $query;
	}
}

?>