<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require APPPATH.'libraries/barcode/BarcodeGenerator.php'; 
require APPPATH.'libraries/barcode/BarcodeGeneratorHTML.php';
require APPPATH.'libraries/barcode/BarcodeGeneratorPNG.php'; 

class Orders extends MX_Controller {

    private $_title = "Orders";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ___Orders";
    private $_grpContent = "orders";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        
        $this->status=array('รับออเดอร์','แพ็คสินค้า','ส่งแล้ว','ยกเลิก');
        $this->load->library('ckeditor');
        $this->load->model("orders_m");
        $this->load->model("products/products_m");
        $this->load->model("cart/cart_order_m","cart_m");
        $this->load->model("provinces/provinces_m");
        $this->load->model("info/info_m");
        $this->load->model("catalogs/catalogs_m");
        $this->load->model("supply/supply_m");
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );

        $action[0][] = action_printcheckandbox(site_url("{$this->router->class}"));
        // $action[1][] = action_printcheck(site_url("{$this->router->class}"));
        // $action[1][] = action_printbox(site_url("{$this->router->class}"));
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       // $action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/action/trash");
        //$action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));

        $data['dateRang']           = date('01/m/Y').' ถึง '.date('d/m/Y');
        $data['createStartDate']    = date('Y-m-01');
        $data['createEndDate']      = date('Y-m-d');

        // status

        $data['status'] =  $this->status;
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->orders_m->get_rows($input);
        $infoCount = $this->orders_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $arr_url = array();
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->order_id);
            $sum_order = $this->orders_m->get_sum_order_detail($rs->order_id);

            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));

            if($rs->invoice):
                $arr_url[] = array(
                    'link' => site_url("{$this->router->class}/print/{$id}"),
                    'title' => 'ปริ้นใบกำกับภาษี',
                    'icon'  => 'la la-print',
                    'class' => '',
                    'data-attr' => ''
                );
            endif;

            // $arr_url[] = array(
            //     'link' => site_url("{$this->router->class}/printbox/{$id}"),
            //     'title' => 'ปริ้นใบติดกล่อง',
            //     'icon'  => 'la la-print'
            // );

            $arr_url[] = array(
                'link' => site_url("{$this->router->class}/printcheck/{$id}"),
                // 'title' => 'ปริ้นใบจัดเตรียมสินค้า',
                'title' => 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง',
                'icon'  => 'la la-print',
                'class' => '',
                'data-attr' => ''
            );
            
            if($rs->supply == 1){
             $tracking_code =  $rs->tracking_code;
         }else{
            $tracking_code = "";
        }
        $arr_url[] = array(
                'link' => 'javascript:void(0)', //site_url("{$this->router->class}/------/{$id}"),
                'title' => 'บันทึกเลขพัสดุ',
                'icon'  => 'la la-send',
                'class' => 'btn-package',
                'data-attr' => $id,
                'data-value' => $tracking_code
            );
        

        $action[1][] = table_action($arr_url);
        unset($arr_url);

        switch ($rs->order_status) {
                case 0 : $style = 'btn-default'; break; //กำลังดำเนินการ
                case 1 : $style = 'btn-warning'; break; //จัดเตรียม
                case 2 : $style = 'btn-success'; break; //ส่งแล้ว
                case 3 : $style = 'btn-danger disabled'; break;  //ยกเลิก
                default: $style = 'btn-default'; break;
            }
            $statusMethod = site_url("orders/action/status");
            $statusPicker = form_dropdown('statusPicker', $this->status, $rs->order_status, "class='form-control statusPicker' data-method='{$statusMethod}' data-id='{$id}' data-style='btn-flat {$style}'");

            $total      = 0;
            $sum_total  = 0;
            //คำนวณยอดรวม
            $info_detail = $this->orders_m->get_detail($rs->order_id);
            foreach($info_detail->result() as $value){
                $total      = $value->product_price * $value->quantity;
                $sum_total += $total;
            }

            $active = $rs->active ? "checked" : null;
            $status_print = '';
            // if($rs->print_box > 0):
            //     $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบติดกล่อง</span><br>';
            // endif;
            if($rs->print_invoice > 0):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบกำกับภาษี</span><br>';
            endif;
            if($rs->print_check > 0):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบสั่งสินค้า/ใบติดกล่อง</span><br>';
            endif;

            if(!empty($rs->tracking_code)):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> เลขพัสดุ</span>';
            endif;
            
            if($rs->supply == 1){
              $order_code = "THDFL".$rs->order_code;
          }else{
              $order_code  = $rs->tracking_code;
          }

          $column[$key]['DT_RowId'] = $id;
          $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
          $column[$key]['order_code'] = $rs->order_code.'<br><small>'.$status_print.'</small>';
          $column[$key]['fullname'] = $rs->customer_fullname;
          $column[$key]['tel'] = $rs->customer_tel;
          $column[$key]['total_list'] = isset($sum_order->sum_qty)? $sum_order->sum_qty : 0;
          $column[$key]['total'] = number_format($sum_total,2);
            $column[$key]['tracking'] =  $order_code;//
            $column[$key]['vat'] = number_format((($sum_total*7)/107),2);
            $column[$key]['buy'] = number_format($sum_total - (($sum_total*7)/107),2);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['active'] = $statusPicker;
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
  public function data_product(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['cart_quantity'] = 0;
        //$input['draw_cart'] = 1;
        $info = $this->cart_m->get_rows($input);

        $infoCount = $this->cart_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->cart_id);

            $input['catalog_id'] = $rs->catalog_id;
            $catalogs_name = $this->catalogs_m->get_rows($input)->row()->title;

            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            if($rs->cart_quantity <= 0){
              $column[$key]['checkbox'] = "<i class='fa fa-lock'></i>";
          }else{
              $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single num' data-title='{$rs->title}'>";
          }
          $column[$key]['img'] = '<img src="'.$this->config->item('root_url').$rs->product_img.'" class="img-thumbnail" style="width:150px !important;">';
          $column[$key]['title'] = $rs->title.' ('.$rs->product_color.')';
          $column[$key]['brand'] = $catalogs_name;
          $column[$key]['price_cost'] = number_format($rs->product_price_cost,2);
          $column[$key]['price_sell'] = number_format($rs->product_price_sell,2);
          $column[$key]['quantity'] = number_format($rs->cart_quantity);
          $column[$key]['customer_name'] = $rs->customer_name;
          $column[$key]['sell_name'] = $rs->sell_name;
      }
      $data['data'] = $column;
      $data['recordsTotal'] = $info->num_rows();
      $data['recordsFiltered'] = $infoCount;
      $data['draw'] = $input['draw'];
      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode($data));
  }

  public function data_productid(){
    $input = $this->input->post();
    $arrayId = $input['arrayId'];
    $arrProId = array();
    foreach($arrayId as $item_id){
        array_push($arrProId, decode_id($item_id));
    }

    $input['proarraId'] = $arrProId;

    $input['recycle'] = 0;
    $input['active'] = 1;
    $info = $this->cart_m->get_rows($input);

    $column = array();
    
    foreach ($info->result() as $key => $rs) {
        $id = $rs->cart_id;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['title'] = $rs->title;
        $column[$key]['cost'] = $rs->product_price_cost;
        $column[$key]['product_price_sell'] = number_format($rs->product_price_sell);
        $column[$key]['product_quantity'] = $rs->cart_quantity;
        $column[$key]['product_id'] = $rs->product_id;
        $column[$key]['customer_name'] = $rs->customer_name;
        $column[$key]['sell_name'] = $rs->sell_name;
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}

public function data_productnum(){
    $input  = $this->input->post();
    //$value  = $this->_build_data_product_order($input);
    //$info   = $this->products_m->product_order($value);
    $input_key['cart_id'] = $input['cart_id'];
    //$qty    = $this->cart_m->get_rows($input_key)->row()->cart_quantity;
    $this->cart_m->order_cart_remove($input['cart_id']);
    $qty    = $this->cart_m->get_rows($input_key)->row()->cart_quantity;
    $data['qty']        = $qty;
    $data['cart_id'] = $input['cart_id'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}

public function create(){
    $this->load->module('template');

    $info = $this->provinces_m->viewAll();
    $data['provinces'] = $info;
    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/save");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/productList";


    $data['supply'] = $this->supply_m->get_all()->result();


    $data['pageScript'] = "scripts/backend/orders/productList.js";

    $this->template->layout($data);
}

public function save(){
    if(!isset($this->session->users['user_id'])){
        redirect(base_url('login/check_login'),'refresh');
        exit();
    }
    $input = $this->input->post(null, true);
    $value = $this->_build_data($input);

    $result = $this->orders_m->insert($value);

    if ( $result ) {

        $values = $this->_build_data_detail($input, $result);

        $this->orders_m->insert_batch($values);

        //Log inventory
        $info_detail = $this->orders_m->get_order_detail($result);

        Modules::run('inventory/inventory_history', 'sell',$info_detail->result(),$values); 
        $this->cart_m->update_uncart_order($this->input->post('cart_id'));
        Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        echo site_url("{$this->router->class}");
        //redirect('{$this->router->class}', 'refresh');
    } else {
        Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
    }


}

public function edit($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['order_id'] = $id;
    $info = $this->orders_m->get_rows($input);
    $info_detail = $this->orders_m->get_order_detail($id);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();

    $into_provin = $this->provinces_m->get_ProvincesBykey($info->provinces_id);
    $data['provinces'] = $into_provin->row();

    $into_amphures = $this->provinces_m->get_AmphuresBykey($info->amphures_id);
    $data['amphures'] = $into_amphures->row();

    $into_districts = $this->provinces_m->get_DistrictsBykey($info->districts_id);
    $data['districts'] = $into_districts->row();

    $into_provin_v = $this->provinces_m->get_ProvincesBykey($info->invoice_provinces_id);
    $data['provinces_v'] = $into_provin_v->row();

    $into_amphures_v = $this->provinces_m->get_AmphuresBykey($info->invoice_amphures_id);
    $data['amphures_v'] = $into_amphures_v->row();

    $into_districts_v = $this->provinces_m->get_DistrictsBykey($info->invoice_districts_id);
    $data['districts_v'] = $into_districts_v->row();

    $data['status'] = $this->status[$info->order_status];
    $data['info'] = $info;
    $data['info_detail'] = $info_detail;
    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/update");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));

    $data['supply'] = $this->supply_m->get_all()->result();

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/form";

    $this->template->layout($data);
}

public function update(){
    $input = $this->input->post(null, true);
    $id = decode_id($input['id']);
    $value = $this->_build_data($input);
    $result = $this->orders_m->update($id, $value);
    if ( $result ) {
     Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
 } else {
     Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
 }
 redirect(site_url("{$this->router->class}"));
}

public function printmultiple(){
    $this->load->module('template');
    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $arrProId = array();
    foreach($arrayId as $item_id){
        array_push($arrProId, decode_id($item_id));
    }
    $input['proarraId'] = $arrProId;
    $info = $this->orders_m->get_rows($input);
    $column = array();

    foreach ($info->result() as $key => $rs) {
        $column[$key]['order_code'] = $rs->order_code;
        $column[$key]['customer_fullname'] = $rs->customer_fullname;
        $column[$key]['customer_tel'] = $rs->customer_tel;
        $column[$key]['customer_address'] = $rs->customer_address;
        $column[$key]['zip_code'] = $rs->zip_code;

        $into_provin = $this->provinces_m->get_ProvincesBykey($rs->provinces_id);
        $column[$key]['provinces'] = $into_provin->row()->name_th;

        $into_provin = $this->provinces_m->get_AmphuresBykey($rs->amphures_id);
        $column[$key]['amphures'] = $into_provin->row()->name_th;

        $into_provin = $this->provinces_m->get_DistrictsBykey($rs->districts_id);
        $column[$key]['districts'] = $into_provin->row()->name_th;

        //ดึง order detail
        $info_detail = $this->orders_m->get_order_detail($rs->order_id);
        $info_detail = $info_detail->result();
        $column[$key]['order_detail'] = $info_detail;
    }
    $data['data'] = $column;

    if (count($arrProId)< 1) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้นใบกำกับภาษี', site_url("{$this->router->class}/print"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/printMulti";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function print($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['order_id'] = $id;
    $info = $this->orders_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;
    $data['grpContent'] = $this->_grpContent;


    if($info->invoice_type > 0):
        $provinces_id           = $info->provinces_id;
        $amphures_id            = $info->amphures_id;
        $districts_id           = $info->districts_id;
        $data['zip_code']       = $info->zip_code;
        $data['tel']            = $info->customer_tel;
        $data['address']        = $info->customer_address;
    else:
        $provinces_id           = $info->invoice_provinces_id;
        $amphures_id            = $info->invoice_amphures_id;
        $districts_id           = $info->invoice_districts_id;
        $data['zip_code']       = $info->invoice_zip_code;
        $data['tel']            = $info->invoice_tel;
        $data['address']        = $info->invoice_address;
    endif;

    $into_provin = $this->provinces_m->get_ProvincesBykey($provinces_id);
    $data['provinces'] = $into_provin->row();

    $into_amphures = $this->provinces_m->get_AmphuresBykey($amphures_id);
    $data['amphures'] = $into_amphures->row();

    $into_districts = $this->provinces_m->get_DistrictsBykey($districts_id);
    $data['districts'] = $into_districts->row();

        //ดึง order detail
    $info_detail = $this->orders_m->get_order_detail($id);
    $data['order_detail'] =  $info_detail->result();

    //     //update print_invoice 
    // $value['print_invoice']     = 1;
    // $this->orders_m->update($info->order_id, $value);

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function updateprintinvoice(){
    //update print_invoice 
    $input = $this->input->post();
    $value['print_invoice']     = 1;
    $this->orders_m->update($input['order_id'], $value);
}

public function printbox($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['order_id'] = $id;
    $info = $this->orders_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;
    $data['order_id']   = encode_id($info->order_id);
    $data['grpContent'] = $this->_grpContent;

    $into_provin = $this->provinces_m->get_ProvincesBykey($info->provinces_id);
    $data['provinces'] = $into_provin->row();

    $into_amphures = $this->provinces_m->get_AmphuresBykey($info->amphures_id);
    $data['amphures'] = $into_amphures->row();

    $into_districts = $this->provinces_m->get_DistrictsBykey($info->districts_id);
    $data['districts'] = $into_districts->row();

        //ดึง order detail
    $info_detail = $this->orders_m->get_order_detail($id);
    $data['order_detail'] =  $info_detail->result();

        //update print 
    $value['print_box']     = 1;
    $this->orders_m->update($info->order_id, $value);

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print_box";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function printboxmultiple(){
    $this->load->module('template');
    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $arrProId = array();
    foreach($arrayId as $item_id){
        array_push($arrProId, decode_id($item_id));
    }
    $input['proarraId'] = $arrProId;
    $info = $this->orders_m->get_rows($input);
    $column = array();

    foreach ($info->result() as $key => $rs) {
        $column[$key]['order_code'] = $rs->order_code;
        $column[$key]['customer_fullname'] = $rs->customer_fullname;
        $column[$key]['customer_tel'] = $rs->customer_tel;
        $column[$key]['customer_address'] = $rs->customer_address;
        $column[$key]['zip_code'] = $rs->zip_code;

        $into_provin = $this->provinces_m->get_ProvincesBykey($rs->provinces_id);
        $column[$key]['provinces'] = $into_provin->row()->name_th;

        $into_provin = $this->provinces_m->get_AmphuresBykey($rs->amphures_id);
        $column[$key]['amphures'] = $into_provin->row()->name_th;

        $into_provin = $this->provinces_m->get_DistrictsBykey($rs->districts_id);
        $column[$key]['districts'] = $into_provin->row()->name_th;

            //update print 
        $value['print_box']     = 1;
        $this->orders_m->update($rs->order_id, $value);

    }

    $data['info'] = $column;

    if (count($arrProId)< 1) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }    
        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print_boxmuti";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function printcheck($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['order_id'] = $id;
    $info = $this->orders_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;
    $data['grpContent'] = $this->_grpContent;

    $into_provin = $this->provinces_m->get_ProvincesBykey($info->provinces_id);
    $data['provinces'] = $into_provin->row();

    $into_amphures = $this->provinces_m->get_AmphuresBykey($info->amphures_id);
    $data['amphures'] = $into_amphures->row();

    $into_districts = $this->provinces_m->get_DistrictsBykey($info->districts_id);
    $data['districts'] = $into_districts->row();

        //ดึง order detail
    $info_detail = $this->orders_m->get_order_detail($id);
    $data['order_detail'] =  $info_detail->result();

    $code = "THDFL".$info->order_code;
    $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
    $generator_img = new Picqer\Barcode\BarcodeGeneratorPNG();
    
    $border = 2;//กำหนดความหน้าของเส้น Barcode
    $height = 50;//กำหนดความสูงของ Barcode

    $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
    $data['barcode'] = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

    $data['sup'] = $this->orders_m->get_sup($id)->row();
        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print_check";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function updateprintcheck(){
    //update print_check
    $input = $this->input->post();
    $value['print_check']     = 1;
    $this->orders_m->update($input['order_id'], $value);
}

public function printcheckmultiple(){

    $this->load->module('template');

    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $column = array();

    foreach($arrayId as $key => $item_id){

        $input['order_id'] = decode_id($item_id);

        $info = $this->orders_m->get_rows($input)->row();

        $info_detail = $this->orders_m->get_order_detail($info->order_id);
        $detail_arr = array();
        foreach($info_detail->result() as $item){
            array_push($detail_arr, array(
                'product_color' => $item->product_color,
                'quantity' => $item->quantity,
                'title' => $item->title
            ));
        }


        $column[$key] = array(
            'order_code' => $info->order_code,
            'order_detail' => $detail_arr
        );

            //update print 
        $value['print_check']     = 1;
        $this->orders_m->update($input['order_id'], $value);
    }

    $data['info'] = $column;
        //ดึง order detail

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

    $data['sup'] = $this->orders_m->get_sup($id)->row();
        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print_checkmuti";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

public function printcheckandboxmultiple(){

    $this->load->module('template');

    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $column = array();
    if(isset($arrayId)){
        foreach($arrayId as $key => $item_id){

            $input['order_id'] = decode_id($item_id);

            $info = $this->orders_m->get_rows($input)->row();

            $info_detail = $this->orders_m->get_order_detail($info->order_id);
            $detail_arr = array();
            foreach($info_detail->result() as $item){
                array_push($detail_arr, array(
                    'product_color' => $item->product_color,
                    'quantity' => $item->quantity,
                    'title' => $item->title
                ));
            }

                //ดึงจังหวัด ของลูกค้า
            $into_provin = $this->provinces_m->get_ProvincesBykey($info->provinces_id);
            $provinces   = $into_provin->row();

            $into_amphures = $this->provinces_m->get_AmphuresBykey($info->amphures_id);
            $amphures      = $into_amphures->row();

            $into_districts = $this->provinces_m->get_DistrictsBykey($info->districts_id);
            $districts      = $into_districts->row();

            $code = "THDFL".$info->order_code;
            $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            $generator_img = new Picqer\Barcode\BarcodeGeneratorPNG();

            $border = 2;
            $height = 50;

            $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
            $data['barcode'] = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);

            $column[$key] = array(
                'order_code'        => $info->order_code,
                'customer_fullname' => $info->customer_fullname,
                'customer_tel'      => $info->customer_tel,
                'customer_address'  => $info->customer_address,
                'zip_code'          => $info->zip_code,
                'provinces'         => $provinces,
                'amphures'          => $amphures,
                'districts'         => $districts,
                'order_detail'      => $detail_arr,
                'sup_title'         => $info->sup_title,
                'collect'           => $info->collect,
                'img_barcode'       => $data['img_barcode'],
                'barcode'           => $data['barcode'],
                'collect_price'     => $info->collect_price,
            );

                //update print 
            $value['print_check']     = 1;
            $this->orders_m->update($input['order_id'], $value);
        }
    }

    $data['info'] = $column;

        //ดึงข้อมูลบริษัท
    $info_com = $this->info_m->get_rows('');
    $info_com = $info_com->row();
    $data['company']  =  $info_com;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/print_checkandboxmuti";
    $data['pageScript'] = "scripts/backend/orders/print.js";

    $this->template->layout($data);
}

private function _build_data($input){
    $provinces_id = $this->provinces_m->get_ProvincesByName($input['provinces_name'])->id;
    $amphures_id = $this->provinces_m->get_AmphuresByName($input['amphures_name'],$provinces_id)->id;
    $districts_id = $this->provinces_m->get_DistrictsByName($input['districts_name'],$amphures_id)->id;
    
    $provinces_id_vat = NULL;
    $amphures_id_vat = NULL;
    $districts_id_vat = NULL;
    if($input['dupicate'] != 1){
        if(!empty($input['provinces_name_vat'])){
            $provinces_id_vat = $this->provinces_m->get_ProvincesByName($input['provinces_name_vat'])->id;
        }
        if(!empty($input['amphures_name_vat'])){
            $amphures_id_vat = $this->provinces_m->get_AmphuresByName($input['amphures_name_vat'],$provinces_id_vat)->id;
        }
        if(!empty($input['districts_name_vat'])){
            $districts_id_vat = $this->provinces_m->get_DistrictsByName($input['districts_name_vat'],$amphures_id_vat)->id;
        } 
    }

    $order_code = decode_id($input['order_code']);
    if(empty($order_code)){
        $value['order_code']        = $this->orders_m->set_order_id()->order_code;
    }



    $value['customer_fullname'] = $input['customer_fullname'];
    $value['customer_tel']      = $input['customer_tel'];
    $value['customer_address']  = $input['customer_address'];
    $value['provinces_id']      = $provinces_id;
    $value['amphures_id']       = $amphures_id;
    $value['districts_id']      = $districts_id;
    $value['zip_code']          = $input['zip_code'];
    $value['invoice']           = ($input['invoice'])? $input['invoice'] : 0;
    $value['invoice_type']      = ($input['dupicate'])? $input['dupicate'] : 0;
    $value['invoice_no']        = $input['vat_code'];
    $value['invoice_address']   = $input['customer_address_vat'];
    $value['invoice_provinces_id']    = $provinces_id_vat;
    $value['invoice_amphures_id']     = $amphures_id_vat;
    $value['invoice_districts_id']    = $districts_id_vat;
    $value['invoice_zip_code']        = $input['zip_code_vat'];
    $value['invoice_tel']       = $input['invoice_tel'];

    $value['supply']            = $input['supply'];
    $value['collect']           = ($input['collect'])? $input['collect'] : 0;
    $value['collect_price']     = ($input['collect_price'])? str_replace(",","",$input['collect_price']) : NULL;

    if($value['invoice'] > 0){
        $value['invoice_code']  = $this->orders_m->set_invoice_id()->invoice_code;
    }

    if($input['supply'] == 1){
      $sup = "THDFL".$value['order_code'];
  }else{
      $sup = "";  
  }

  if ( $input['mode'] == 'create' ) {
    $value['tracking_code']       =  $sup;
    $value['created_at'] = db_datetime_now();
    $value['created_by'] = $this->session->users['user_id'];
    $value['updated_at'] = db_datetime_now();
    $value['updated_by'] = $this->session->users['user_id'];
} else {
    $value['updated_at'] = db_datetime_now();
    $value['updated_by'] = $this->session->users['user_id'];
}
return $value;
}

private function _build_data_detail($input,$id){

    $detail_arr = array();
    if(isset($input['product_id']) && count($input['product_id']) <= 0){
        Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        echo site_url("{$this->router->class}");
        exit();
    }
    $i=0;
    if (is_array($input['product_id']) || is_object($input['product_id']))
    {
        foreach($input['product_id'] as $key => $item){
            if($input['product_price_sell'][$key] > 0){

            }

            //$this->cart_m->update_ordercart(decode_id($item),$input['quantity'][$i]);

            array_push($detail_arr, array(
                'order_id'      => $id,
                'product_id'    => decode_id($item),
                'product_price' => intval(str_replace(',', '', $input['product_price_sell'][$key])),
                'quantity'      => $input['quantity'][$i],
                'created_at'    => db_datetime_now()
            ));
            $i++;
        }

        return $detail_arr;
    }
}

private function _build_data_product_order($input){
    if ( ! function_exists('generateRandomString')){
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }
    }

    if($this->session->userdata('userId') == '') {
        $this->session->set_userdata('userId', generateRandomString());
    }

    $products_order = $this->db->get_where('products_order', array('product_id' => decode_id($input['pro_id']), 'product_order_session' => $this->session->userdata('userId')));

    $value['product_id'] = decode_id($input['pro_id']);
    $value['product_order_total'] = $input['type'];
    $value['product_order_session'] = $this->session->userdata('userId');

    if ($products_order->num_rows()>0) {
        $value['updated_at'] = db_datetime_now();
    } else {
        $value['created_at'] = db_datetime_now();
    }
    return $value;
}

public function excel(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->orders_m->get_rows($input);
    $fileName = "orders";
    $sheetName = "Sheet name";
    $sheetTitle = "Sheet title";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $styleH = excel_header();
    $styleB = excel_body();
    $colums = array(
        'A' => array('data'=>'รายการ', 'width'=>50),
        'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
        'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
    );
    $fields = array(
        'A' => 'title',
        'B' => 'excerpt',
        'C' => 'created_at',            
        'D' => 'updated_at',
    );
    $sheet->setTitle($sheetName);

        //title
    $sheet->setCellValue('A1', $sheetTitle);
    $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
    $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    $sheet->getRowDimension(1)->setRowHeight(20);

        //header
    $rowCount = 2;
    foreach ( $colums as $colum => $data ) {
        $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
        $sheet->SetCellValue($colum.$rowCount, $data['data']);
        $sheet->getColumnDimension($colum)->setWidth($data['width']);
    }

        // data
    $rowCount++;
    foreach ( $info->result() as $row ){
        foreach ( $fields as $key => $field ){
            $value = $row->{$field};
            if ( $field == 'created_at' || $field == 'updated_at' )
                $value = datetime_table ($value);
            $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
            $sheet->SetCellValue($key . $rowCount, $value); 
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $rowCount++;
    }
    $sheet->getRowDimension($rowCount)->setRowHeight(20);  
    $writer = new Xlsx($spreadsheet);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
    $writer->save('php://output');
    exit; 
}

public function pdf(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->orders_m->get_rows($input);
    $data['info'] = $info;

    $mpdfConfig = [
        'default_font_size' => 9,
        'default_font' => 'garuda'
    ];
    $mpdf = new \Mpdf\Mpdf($mpdfConfig);
    $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
    $mpdf->WriteHTML($content);


    $download = FALSE;
    $saveFile = FALSE;
    $viewFile = TRUE;
    $fileName = "orders.pdf";
    $pathFile = "uploads/pdf/orders/";
    if ( $saveFile ) {
        create_dir($pathFile);
        $fileName = $pathFile.$fileName;
        $mpdf->Output($fileName, 'F');
    }
    if ( $download ) 
        $mpdf->Output($fileName, 'D');
    if ( $viewFile ) 
        $mpdf->Output();
}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->orders_m->get_rows($input);
    $infoCount = $this->orders_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->order_id);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
} 

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();
        foreach ( $input as $rs ) 
            $rs = decode_id($rs->id);
        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;

        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->orders_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            foreach ($input['id'] as $id) {
             $result = $this->orders_m->update_in(decode_id($id), $value);
             if($result):
                $valueorder['order_id']     = decode_id($id);
                $info_order = $this->orders_m->get_rows($valueorder)->row();
                if(!empty($info_order)):
                    if($info_order->order_status != 3):
                        $info =  $this->orders_m->get_detail(decode_id($id))->result();
                        foreach($info as $item){
                            $inputset['product_id'] = $item->product_id;
                            $inputset['quantity']   = $item->quantity;
                            $this->products_m->product_cancel($inputset);
                        }
                        unset($info);
                    endif;
                endif;
            endif;
        }  
    }
    if ( $type == "restore" ) {
        $value['active'] = 0;
        $value['recycle'] = 0;
        $result = $this->orders_m->update_in($input['id'], $value);
    }
    if ( $type == "delete" ) {
        $value['active'] = 0;
        $value['recycle'] = 2;
        $result = $this->orders_m->update_in($input['id'], $value);
    } 
    if ( $type == "tracking" ) {
        $value['tracking_code'] = $input['tracking_code'];
        $result = $this->orders_m->update(decode_id($input['id']), $value);
    }  
    if ( $type == "status" ) {
        $value['order_status'] = $input['status'];
        foreach ($input['id'] as $id) {
            $result = $this->orders_m->update_in(decode_id($id), $value);
            if($result){
                if($input['status'] == 3){
                    if(isset($id)){
                            //foreach($input['id'] as $code){
                        $info =  $this->orders_m->get_detail(decode_id($id))->result();
                        $data = array();
                        foreach($info as $item){
                            $inputset['product_id'] = $item->product_id;
                            $inputset['quantity']   = $item->quantity;
                            
                            array_push($data, 
                                array(
                                    'order_detail_id' => $item->order_detail_id, 
                                    'product_id' => $item->product_id,
                                    'quantity' => $item->quantity,
                                )
                            );

                            $this->products_m->product_cancel($inputset);
                            
                        }
                        Modules::run('inventory/inventory_history', 'cancle',$data,$data); 
                        unset($info);
                            //}
                    }
                }
            }
        }
    }   

    if ( $result ) {
        $toastr['type'] = 'success';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
    } else {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
    }
    $data['success'] = $result;
    $data['toastr'] = $toastr;
}
$this->output
->set_content_type('application/json')
->set_output(json_encode($data));        
}

public function sumernote_img_upload(){
		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
     $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
 }

 echo $picture;

}

public function deletefile($ids){
    $arrayName = array('file' => $ids);
    echo json_encode($arrayName);
}

public function delete_product_list(){
    $input = $this->input->post();
    $quantity_restore = '';

    $this->db->where('product_id', decode_id($input['pro_id']));
    $this->db->where_in('product_order_session', $this->session->userdata('userId'));
    $del = $this->db->delete('products_order');

    $quantity_restore = 'product_quantity+'.$input['quantity'];
    $this->db->set('product_quantity', $quantity_restore, FALSE);
    $this->db->where_in('product_id', decode_id($input['pro_id']));
    $this->db->update('products');
    $data['data'] = $del;
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}

public function delete_product_order_list(){
    $quantity_restore = '';
    $product_order = $this->orders_m->get_product_order_detail($this->session->userdata('userId'))->result();

    if(isset($product_order) && count($product_order) > 0):
        foreach($product_order as $product):

            $this->db->where('product_id', $product->product_id);
            $this->db->where('product_order_id', $product->product_order_id);
            $this->db->where('product_order_session', $product->product_order_session);
            $this->db->delete('products_order');

            $quantity_restore = 'cart_quantity+'.$product->product_order_total;
            //$draw_restore = 'draw+'.$product->product_order_total;
            $this->db->set('cart_quantity', $quantity_restore, FALSE);
            //$this->db->set('draw', $draw_restore, FALSE);
            $this->db->where('product_id', $product->product_id);
            $this->db->update('carts');

        endforeach;
    endif;

    $data['data'] = true;
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}
}
