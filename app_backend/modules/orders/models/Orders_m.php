<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_sup($order_id)
    {
        $query = $this->db
        ->select('a.*,s.title')
        ->from('orders a')
        ->join('supply s','a.supply = s.supply_id', 'left')
        ->where('order_id',$order_id)
        ->get();

        return $query;
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
        ->select('a.*,s.title as sup_title')
        ->from('orders a')
        ->join('supply s','a.supply = s.supply_id','left')
        ->get();

        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
        ->select('a.*')
        ->from('orders a')
        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.title', $param['keyword'])
            ->or_like('a.excerpt', $param['keyword'])
            ->or_like('a.detail', $param['keyword'])
            ->group_end();
        }

        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
            ->group_start()
            ->like('a.order_code', $param['search']['value'])
            ->or_like('a.customer_fullname', $param['search']['value'])
            ->or_like('a.customer_tel', $param['search']['value'])
            ->or_like('a.tracking_code', $param['search']['value'])
            ->group_end();
        }else{
            if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
                $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
            }
            if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
                $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
            }   

            if (!empty($param['order_year'])):
                $this->db->where('YEAR(a.created_at)', $param['order_year']);
            endif;

            if (!empty($param['order_month'])):
                $this->db->where('MONTH(a.created_at)', $param['order_month']);
            endif;

            if (!empty($param['order_day'])):
                $this->db->where('DAY(a.created_at)', $param['order_day']);
            endif;
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.order_code";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.customer_fullname";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 6) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 7) $columnOrder = "a.updated_at";
                if ($param['order'][0]['column'] == 8) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 10) $columnOrder = "a.tracking_code";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
            ->order_by($columnOrder, $param['order'][0]['dir']);
        }

        if(isset($param['datasupply'])){
            $this->db->where_in('a.supply', $param['datasupply']);
        } 
        
        if ( isset($param['order_id']) ) 
            $this->db->where('a.order_id', $param['order_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( isset($param['proarraId']) )
            $this->db->where_in('a.order_id', $param['proarraId']);

        if ( isset($param['created_at']) )
            $this->db->like('a.created_at', $param['created_at']);

        if ($param['order_status'] !='')
            $this->db->where('a.order_status', $param['order_status']);

        if (!empty($param['tracking_code']))
            $this->db->where(array('a.tracking_code' => NULL));
    }
    
    public function insert($value) {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }

    public function insert_batch($value) {
        $query = $this->db->insert_batch('orders_detail', $value);
        if($query){
            $this->db->where_in('product_order_session', $this->session->userdata('userId'));
            $this->db->delete('products_order');
            $this->session->unset_userdata('userId');
        }
    }
    
    public function update($id, $value)
    {
        $query = $this->db
        ->where('order_id', $id)
        ->update('orders', $value);
        return $query;
    }

    public function update_vat($id, $value)
    {
        $query = $this->db
        ->where('order_code', $id)
        ->update('orders', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
        ->where_in('order_id', $id)
        ->update('orders', $value);
        return $query;
    }  

    public function get_order_detail($value)
    {
        $query = $this->db
        ->select('*')
        ->from('orders_detail o')
        ->join('products p', 'p.product_id = o.product_id', 'inner')
        ->where('o.order_id', $value)
        ->get();
        return $query;
    }

    public function get_detail($value)
    {
        $query = $this->db
        ->select('*')
        ->from('orders_detail a')
        ->where('a.order_id', $value)
        ->get();
        return $query;
    }
    
    public function get_join_order_detail($value)
    {
        $query = $this->db
        ->select('*')
        ->from('orders_detail a')
        ->join('orders b', 'a.order_id = b.order_id', 'inner')
        ->join('products c', 'a.product_id = c.product_id', 'inner')
        ->where('a.order_id', $value)
        ->get();
        return $query;
    }

    public function get_product_order_detail($id)
    {
        $query = $this->db
        ->select('*')
        ->from('products_order o')
        ->where_in('o.product_order_session', $id)
        ->get();
        return $query;
    } 
    
    public function insert_invoice($value) {
        $this->db->insert('invoice', $value);
        return $this->db->insert_id();
    }

    public function get_invoice(){
        $query = $this->db->select('*')
        ->from('invoice')
        ->order_by('invoice_number', 'DESC')
        ->get();
        return $query;
    }

    public function set_order_id(){

        $query = "INSERT
        INTO `auto_order_id`
        (`id`)
        VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
        CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
        DATE_FORMAT(NOW(), '%Y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
        LPAD(
        IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
        (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
        MAX(SUBSTR(`id`, 7))
        FROM `auto_order_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
        WHERE SUBSTR(`id`, 1, 6) = DATE_FORMAT(NOW(), '%Y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
        ORDER BY `id` DESC
        LIMIT 1
        )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
        + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
        1
        ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                    )
                    )
                )";
                $order_code = null;
                $query = $this->db->query($query);
                if($query){
                    $this->db->select_max('id','order_code');
                    $this->db->order_by('id', 'DESC');
                    $this->db->limit(1);
                    $order_code = $this->db->get('auto_order_id')->row();
                }
                return $order_code;
        }

            public function set_invoice_id(){

                $query = "INSERT
                INTO `auto_invoice_id`
                (`id`)
                VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
                CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
                DATE_FORMAT(NOW(), 'Rcpt%y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
                LPAD(
                IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
                (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
                MAX(SUBSTR(`id`, 11))
                FROM `auto_invoice_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
                WHERE SUBSTR(`id`, 1, 8) = DATE_FORMAT(NOW(), 'Rcpt%y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
                ORDER BY `id` DESC
                LIMIT 1
                )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
                + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
                1
                ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                    )
                    )
                )";
                $invoice_code = null;
                $query = $this->db->query($query);
                if($query){
                    $this->db->select_max('id','invoice_code');
                    $this->db->order_by('id', 'DESC');
                    $this->db->limit(1);
                    $invoice_code = $this->db->get('auto_invoice_id')->row();
                }
                return $invoice_code;
            }

            public function get_sum_order_detail($order_id){
                $this->db->select_sum('product_price','sum_price');
                $this->db->select_sum('quantity','sum_qty');
                $this->db->limit(1);
                $this->db->where('order_id', $order_id);
                $query = $this->db->get('orders_detail')->row();
                return $query;
            }

        }
