
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group mr-2" role="group" aria-label="1 group">
                   
                </div>
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
        
            <div class="form-group m-form__group row">
                <label for="order_year" class="offset-2 col-1 col-form-label">ปี</label>
                <div class="col-4">
                    <select name="order_year" id="order_year" class="form-control m-input select2">
                        <?php
                        for($i = 2017; $i <= date('Y'); $i++):
                            $selected = '';
                            if($i == date('Y')):
                                $selected = 'selected';
                            endif;
                            echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                        endfor;

                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_month" class="offset-2 col-1 col-form-label">เดือน</label>
                <div class="col-4">
                    <select name="order_month" id="order_month" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        $month_arr = array( "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                        if(isset($month_arr) && count($month_arr)):
                            foreach($month_arr as $key => $item):
                                $selectedm = '';
                                if(($key+1) == date('m')):
                                    $selectedm = 'selected';
                                endif;
                                
                                echo '<option value="'.($key+1).'" '.$selectedm.'>'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-1 col-form-label">สถานะ</label>
                <div class="col-4">
                    <select name="order_status" id="order_status" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        if(isset($status) && count($status)):
                            foreach($status as $key => $item):
                            echo '<option value="'.$key.'">'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label"></label>
                <div class="col-4">
                    <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                    <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
                </div>
            </div>
            <br>
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>
                                #
                                <!-- <input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"> -->
                            </th>
                            <th>OrderID</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทร</th>
                            <th>จำนวนรายการ (หน่วย)</th>
                            <th>จำนวนเงิน</th>
                            <th>ภาษีมูลค่าเพิ่ม</th>
                            <th>ราคาขาย</th>
                            <th>แก้ไข</th>
                            <th>สถานะ</th>
                            <th>Tracking</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>



<!--begin::Modal-->
<div class="modal fade" id="m_modal_tracking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">เพิ่มเลขที่นำส่งสินค้า</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">Tracking:</label>
                    <input type="text" id="tracking_code" class="form-control" placeholder="กรุณากรอกข้อมูล">
                    <input type="hidden" id="text-order-code">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
            <button type="button" id="btn-tracking-send" class="btn btn-primary">บันทึกข้อมูล</button>
        </div>
    </div>
</div>
</div>