<style>
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
/* page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
} */
page[size="A4"] {  
  width: 21cm;
  height: auto; 
}
.page-break	{ display: none; }
table{
    width: 100%;
}
.table-print th{
    border-bottom: 0.1px solid;
    border-top: 0.1px solid;
    padding-top: 5px;
    padding-bottom: 5px;
}
.table-print p{
    margin: 0px 7px 0px 7px;
}
.p-border{
    padding: 10px;
}
.table-bottom{
        width: 100%;
        position: relative;
        top: 528px;
    }
.note{
    position: relative;
    width: 100%;
    top: 520px;
    border-top: 0.1px solid;
}



@media (min-width: 992px) {
    .table-bottom{
        width: 100%;
        position: relative;
        top: 528px;
    }
    .note{
        position: relative;
        width: 100%;
        top: 520px;
        border-top: 0.1px solid;
    }
}
@media (min-width: 1200px) {
    .table-bottom{
        width: 100%;
        position: relative;
        top: 526px;
    }
    .note{
        position: relative;
        width: 100%;
        top: 520px;
        border-top: 0.1px solid;
    }
}
.customer{
    border-top: 0.1px solid;
}

.note tr td p{
    margin: 0px 7px 0px 7px;
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม print
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div  class="m-portlet__body">
            <page id="printarea" size="A4">

            <style type="text/css"  media="print">
                @media print {
                    
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print {
                        border-collapse: collapse;
                    }
                    .table-print th{
                        border-bottom: 0.1px solid;
                        border-top: 0.1px solid;
                        padding-top: 5px;
                        padding-bottom: 5px;
                    }
                    .table-print p{
                        margin: 0px 6px 0px 7px;
                    }
                    .p-border{
                        padding: 0px;
                    }
                    
                    .customer{
                        border-top: 0.1px solid;
                    }
                   
                    .note tr td p{
                        margin: 0px 7px 0px 7px;
                    }
                    .table-bottom{
                            width: 100%;
                            bottom: 0;
                            
                        
                    }
                    .note{
                        
                        width: 100%;
                        border-top: 0.1px solid;
                    }
                    .page-break	{ 
                        display: block; 
                        page-break-before: always; 
                        /* position: absolute;
                        bottom: 0px; */
                    }
                    

                    /* @page {
                        size: A4;
                        margin: 10%;
                        @bottom-center {
                            content: "Page " counter(page);
                        }
                    } */
                }
                /* @page table-bottom :last {
                    width: 100%;
                    position: absolute; 
                    bottom: 0;
                }

                @page note :last {
                    position: absolute;
                    width: 100%;
                    bottom: 100px;
                    border-top: 0.1px solid;
                } */

                

             
                
                </style>
                <div class="p-border">
                    <table style="width: 100%;">
                        <tr>
                            <td > 
                                <h3><?=$company->title?></h3>
                            </td>
                            <td valign="top" style="text-align: right;">  
                                <h4>ใบเสร็จรับเงิน/ใบกำกับภาษี</h4>
                            </td>
                        </tr>
                        <tr>
                            <td style="line-height: 6px;">
                            <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                                <p>Tel. <?=$company->tel?></p>
                                <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                            </td>
                        </tr>
                    </table>

                    <table class="table-print" style="width: 100%; margin-top: 10px;">
                        <tr class="customer" >
                            <td colspan="3" style="width: 350px; padding-top: 10px; padding-bottom: 10px;">
                                <p>
                                    <p>ชื่อลูกค้า : <?=$info->customer_fullname?></p>
                                    <p>ที่อยู่ : <?=$address.'<br>ตำบล'.$districts->name_th.' อำเภอ'.$amphures->name_th.' จังหวัด'.$provinces->name_th.' '.$zip_code?></p>
                                    <p><strong> เลขประจำตัวผู้เสียภาษี:</strong> <?=isset($info->invoice_no)? $info->invoice_no : '-';?></p>
                                    <p>เบอร์โทร : <?=$tel;?></p>
                                </p>
                            </td>
                            <td valign="top" colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                                <p>
                                    <p><strong>เลขที่บิล :</strong> <?=$info->invoice_code;?></p>
                                    <p>วันที่ : <?=date('d/m/Y H:i:s')?></p>
                                    <p>เงื่อนไขชำระ : -</p>
                                    <p><strong>เลขที่สั่งซื้อสินค้า :</strong> <?=$info->order_code;?></p>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th style="text-align: center;"><p>ลำดับ</p></th>
                            <th><p>รายการสินค้า</p></th>
                            <th style="text-align: center;"><p>จำนวน</p></th>
                            <th style="text-align: right;"><p>หน่วยละ</p></th>
                            <th style="text-align: right;"><p>จำนวน</p></th>
                        </tr>
                        <?php 
                        $sumtotal = 0;
                        $total = 0;
                        
                        if(isset($order_detail) && count($order_detail) > 0){
                            // for($a=0;$a<30;$a++){}
                            $len = count($order_detail);
                            foreach($order_detail as $key => $item){
                                $total      = $item->product_price*$item->quantity;
                                $sumtotal  += $total; 
                        ?>
                        <tr>
                            <td style="text-align: center;"><p><?=$key+1?></p></td>
                            <td><p><?=$item->title?></p></td>
                            <td style="text-align: center;"><p><?=$item->quantity?></p></td>
                            <td style="text-align: right;"><p><?=number_format($item->product_price,2)?></p></td>
                            <td style="text-align: right;"><p><?=number_format($total,2)?></p></td>
                        </tr>
                        
                        <?php if(($key+1)%10==0){ ?>
                      
                        </table>
                        <div class="page-break"></div>
                        <table class="table-print" style="width: 100%; margin-top: 10px;">
                      
                        <?php } ?>

                        <?php if($key == $len - 1){ ?>
                         <div class="footer" style="position: fixed;  bottom: 0;">
                        <table class="note" >
                            <tr>
                                <td valign="top" colspan="3" style="padding-top: 10px;">
                                    <p>หมายเหตุ</p>
                                </td>
                                <td style="text-align: right; padding-top: 10px;">
                                    <p >จำนวนเงิน :</p>
                                    <p>ส่วนลด : </p>
                                    <p>รวมเป็นเงิน :</p>
                                </td>
                                <td style="text-align: right; padding-top: 10px;">
                                    <p><?=number_format($sumtotal,2)?></p>
                                    <p>0</p>
                                    <p><?=number_format($sumtotal,2)?></p>
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="2"> 
                                    <p>
                                    ได้รับสินค้า / บริการ  ตามรายกานี้ข้างบนนี้ไว้ถูกต้อ
                                    <br>
                                    และอยู่ในสภาพที่เรียบร้อยทุกประการ</p>
                                </td>
                            </tr>
                        </table>   

                        <table class="table-bottom" style="">
                            <tr>
                                <td style="border: 1px solid;">
                                    <div style="text-align: center;">
                                        <p>ผู้รับสินค้า________________วันที่________/________/________</p>
                                    </div>
                                </td>
                                <td style="border: 1px solid;">
                                    <div style="text-align: center;">
                                        <p>ในนาม บริษัท มีดี88ช็อป จำกัด</p>
                                        <p>_____________________</p>
                                        <p>ผู้รับมอบอำนาจ</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </div>
                        <?php } ?>
                        
                        <?php } }?>
                    
                    
                </div>
            
            
            </page>              
        </div>    
    </div>
</div>

<script>
    //set par fileinput;
    var required_icon   = ''; 
    var file_image      = '';
    var file_id         = '';
    var deleteUrl       = '';

</script>




