<style>
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  /* height: auto;  */
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ใบกำกับภาษี
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div  class="m-portlet__body">
            <?php //echo arr($data)?>

            <div id="printarea">
            <?php 
                if(isset($data) && count($data) > 0){
                foreach($data as $item){ ?>      
            <page  size="A4">
                <div style="padding: 20px;">
                    <table style="width: 100%;">
                        <tr>
                            <td > 
                                <h3><?=$company->title?></h3>
                            </td>
                            <td >                               
                                <h4 style="border-radius: 10px; border: 2px solid; text-align: center; padding: 10px;">ใบเสร็จรับเงิน</h4>
                            </td>
                        </tr>
                        <tr>
                            <td style="line-height: 6px;">
                            <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                                <p>Tel. <?=$company->tel?></p>
                                <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                            </td>
                        </tr>
                    </table>
                    <br>
                     <p style="font-size: 16px;color: red">ขนส่ง : <?=$sup->title?></p>

                    <table style="width: 100%; margin-top: 10px;">
                        <tr>
                            <td colspan="3" style="border: 1px solid;">
                                <div style="line-height: 8px; padding: 2px;">
                                    <p>นามลูกค้า : <?=$item['customer_fullname']?></p>
                                    <p>ที่อยู่ : <?=$item['customer_address'].'<br><br><br>ตำบล'.$item['districts'].' อำเภอ'.$item['amphures'].' จังหวัด'.$item['provinces'].' '.$item['zip_code']?></p>
                                    <p>เลขประจำตัวผู้เสียภาษี: -</p>
                                    <p>เบอร์โทร : <?=$item['customer_tel']?></p>
                                </div>
                            </td>
                            <td valign="top" colspan="2" style="border: 1px solid;">
                                <div style="line-height: 8px; padding: 2px;">
                                    <p>เลขที่บิล : <?=$item['order_code']?></p>
                                    <p>วันที่ : <?=date('d/m/Y H:i:s')?></p>
                                    <p>เงื่อนไขชำระ : -</p>
                                    
                                </div>
                            </td>
                        </tr>
                        <tr style="text-align: center;">
                            <td style="border: 1px solid;"><p>ลำดับ</p></td>
                            <td style="border: 1px solid;"><p>รายการสินค้า</p></td>
                            <td style="border: 1px solid;"><p>จำนวน</p></td>
                            <td style="border: 1px solid;"><p>หน่วยละ</p></td>
                            <td style="border: 1px solid;"><p>จำนวน</p></td>
                        </tr>
                        <?php 
                        $sumtotal = 0;
                        $total = 0;
                        if(isset($item['order_detail']) && count($item['order_detail']) > 0){
                            foreach($item['order_detail'] as $key => $detail){
                                $total      = $detail->product_price*$detail->quantity;
                                $sumtotal  += $total; 
                        ?>
                        <tr style="text-align: center;">
                            <td style="border-left: 1px solid; border-right: 1px solid;"><p><?=$key+1?></p></td>
                            <td style="border-left: 1px solid; border-right: 1px solid;"><p><?=$detail->title?></p></td>
                            <td style="border-left: 1px solid; border-right: 1px solid;"><p><?=$detail->quantity?></p></td>
                            <td style="border-left: 1px solid; border-right: 1px solid;"><p style="text-align: right; margin-right: 10px;"><?=$item->product_price?></p></td>
                            <td style="border-left: 1px solid; border-right: 1px solid;"><p style="text-align: right; margin-right: 10px;"><?=$total?></p></td>
                        </tr>
                        <?php }}?>

                        <tr>
                            <td valign="top" colspan="3" style="border: 1px solid;">
                                <div style="line-height: 8px; padding: 2px;">
                                    <p>หมายเหตุ</p>
                                </div>
                            </td>
                            <td style="border: 1px solid;">
                                <div style="line-height: 8px; padding: 2px;">
                                    <p>จำนวนเงิน :</p>
                                    <p>ส่วนลด : </p>
                                    <p>รวมเป็นเงิน :</p>
                                </div>
                            </td>
                            <td style="border: 1px solid;">
                                <div style="line-height: 8px; padding: 2px;">
                                    <p style="text-align: right; margin-right: 10px;"><?=$sumtotal?></p>
                                    <p style="text-align: right; margin-right: 10px;">0</p>
                                    <p style="text-align: right; margin-right: 10px;"><?=$sumtotal?></p>
                                </div>
                            </td>
                        </tr> 
                    </table>

                    <table style="width: 100%; margin-top: 10px;">
                        <tr>
                            <td colspan="2"> 
                                <div style="margin-top: 20px; margin-bottom: 20px; line-height: 8px;">
                                    <p>ได้รับสินค้า / บริการ  ตามรายกานี้ข้างบนนี้ไว้ถูกต้อ</p>
                                    <p>และอยู่ในสภาพที่เรียบร้อยทุกประการ</p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="border: 1px solid;">
                                <div style="text-align: center;">
                                    <p>ผู้รับสินค้า_____วันที่__/__/__</p>
                                </div>
                            </td>
                            <td style="border: 1px solid;">
                                <div style="text-align: center;">
                                    <p>ในนาม บริษัท มีดี88ช็อป จำกัด</p>
                                    <p>___________</p>
                                    <p>ผู้รับมอบอำนาจ</p>
                                </div>
                            </td>
                        </tr>
                    </table>


                </div>
            </page> 
            <?php } }?>
            </div>


        </div>    
    </div>
</div>

<script>
    //set par fileinput;
    var required_icon   = ''; 
    var file_image      = '';
    var file_id         = '';
    var deleteUrl       = '';

</script>




