<style>
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}

table{
    width: 100%;
}
.table-print tr td{
    border: 0px solid;
}

.table-print td{
    border: 0px solid;
}
.table-print th{
    border: 0px solid;
}

.table-print tr{
    border: 1px solid;
}
.table-print p{
    margin: 0px 7px 0px 7px;
}
.table-print .box-check {
    padding: 20px;
}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม print
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div  class="m-portlet__body">
            <page id="printarea" size="A4">
                <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print {
                        border-collapse: collapse;
                    }
                    .table-print tr{
                        border: 1px solid;
                    }   
                    .table-print td{
                        border: 1px solid;
                    }
                    .table-print th{
                        border: 1px solid;
                    }
                    .table-print p{
                        margin: 0px 0px 0px 7px;
                    }
                    .table-print .box-check {
                        padding: 20px;
                    }
                }
                </style>
                <div style="padding: 20px;">
                    <table class="table-print" style="width: 100%;">
                        <tr>
                            <td style="width: 50%;" valign="top">
                                <div class="box-check">
                                    <p><u><strong>ผู้ส่ง</strong></u></p>
                                    <p><?=$company->title?></p>
                                    <p><?=$company->excerpt?></p>
                                    <p>Tel. <?=$company->tel?></p>
                                    <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                                    
                                </div>
                            </td>
                            <td style="width: 50%;" valign="top">
                                <div class="box-check">
                                    <p><u><strong>ผู้รับ</strong></u></p>
                                    <p><?=$info->customer_fullname?></p>
                                    <p><?=$info->customer_address.'<br>ตำบล'.$districts->name_th.' อำเภอ'.$amphures->name_th.' จังหวัด'.$provinces->name_th.' '.$info->zip_code?></p>
                                    <p>Tel. <?=$info->customer_tel?></p>
                                    <br>
                                    <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?=$info->order_code?></p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </page>                 
        </div>    
    </div>
</div>




