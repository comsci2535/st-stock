<style>
    page {
      background: white;
      display: block;
      margin: 0 auto;
      margin-bottom: 0.5cm;
      box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
      width: 21cm;
      height: 29.7cm; 
  }

  table{
    width: 100%;
}
.table-print tr td{
    border: 1px solid;
}

.table-print td{
    border: 1px solid;
}
.table-print th{
    border: 1px solid;
}
.table-print, .table-sender, .table-recipient p{
    margin: 0px 0px 0px 7px;
}

.table-sender tr td{
    width: 24%;
}

.table-recipient{
    width: 100%;
    position: relative;
    top: 460px;
    left: 65px;
}
.table-sender{
    width: 50%;
    position: relative;
    top: 491px;
    left: 0px;
}
.barcode_img{
    display: none;
}
.collect {
    border: 2px solid #32b312;
    border-radius: 6px;
    padding: 24px 20px;
    text-align: center;
    font-size: 20px;
    color: #2aa50b;
    width: 80%;
    margin-left: 55px;
}
@media (min-width: 992px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
        left: 65px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}
@media (min-width: 1200px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
        left: 65px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     แบบฟอร์ม
                 </h3>
             </div>
         </div>
         <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                <input id="order-id-printcheck" type="hidden" value="<?=$info->order_id?>">
                <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </div>
    <div  class="m-portlet__body">
        <page id="printarea" size="A4">
            <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print, .table-sender, .table-recipient{
                        border-collapse: collapse;
                    }
                    .table-print tr{
                        border: 1px solid;
                    }   
                    .table-print td{
                        border: 1px solid;
                    }
                    .table-print th{
                        border: 1px solid;
                    }
                    .table-print, .table-sender, .table-recipient p{
                        margin: 0px 0px 0px 7px;
                    }
                    .table-sender{
                        width: 100%;
                        position: relative;
                        top: 400px;
                        left: 0px;
                    }
                    .table-recipient{
                        width: 100%;
                        position: relative;
                        top: 420px;
                        left: 65px;
                    }
                    .barcode{
                        display: none;
                    }
                    .collect {
                        border: 2px solid #32b312;
                        border-radius: 6px;
                        padding: 24px 20px;
                        text-align: center;
                        font-size: 20px;
                        color: #2aa50b;
                        width: 70%;
                        margin-left: 55px;
                    }
                }
            </style>

            <div style="padding: 20px;">

                <table style="width: 100%;">
                    <tr>
                        <td > 
                            <h3><?=$company->title?></h3>
                        </td>
                        <td >                               
                            <h4 style="border-radius: 10px; border: 2px solid; text-align: center; padding: 10px;">ใบสั่งสินค้า</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 6px;">
                            <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                            <p>Tel. <?=$company->tel?></p>
                            <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                            <br>
                        </td>
                    </tr>
                </table>
                <br>
                <p style="font-size: 16px;color: red">ขนส่ง : <?=$sup->title?></p>
                <table class="table-print" style="width: 100%;">
                    <tr>
                        <th style="width: 150px;text-align: center;"><p>เลขที่สั่งซื้อ</p></th>
                        <th><p>รายการสินค้า</p></th>
                        <th style="width: 100px;text-align: center;"><p>จำนวน</p></th>
                    </tr>
                    <?php
                    $sumtotal = 0;
                    if(isset($order_detail) && count($order_detail) > 0){
                        $rowspan = count($order_detail);
                        $i =0;
                        foreach($order_detail as $key => $item){
                            $sumtotal  += $item->quantity;
                            ?>
                            <!-- style="height: 300px;" -->
                            <tr>
                                <?php
                                if($key == 0):
                                    ?>
                                    <td valign="top" rowspan="<?=$rowspan?>" style="text-align: left;"><p style="padding-left:5px;"><?=$info->order_code?></p></td>
                                    <?php
                                endif;
                                ?>
                                <td style=""><p style="padding-left:5px;"><?=$item->title?> <?=isset($item->product_color) ? '('.$item->product_color.')' : '';?></p></td>
                                <td style="text-align: center;"><p><?=$item->quantity?></p></td>
                            </tr>
                            <?php 
                            $i++;
                        }
                    }

                    $top =240;
                    $top2 =200;
                    $i= $i * 20;
                    $top = $top - $i;
                    $top2 = $top2 - $i;
                    ?>

                    <tr>
                        <th valign="top" colspan="2" style="text-align: right;">
                            <p style="padding-right:5px;">รวมทั้งหมด</p>
                        </th>
                        <th style="text-align: center;">
                            <p><?=$sumtotal?></p>
                        </th>
                    </tr> 
                </table>

                <table class="table-sender" style="<?='top :'.$top.'px'?> ">
                    <tr>
                        <td valign="top" style="width: 50%;">
                            <div class="box-check">
                                <p><u><strong>ผู้ส่ง</strong></u></p>
                                <p><?=$company->title?></p>
                                <p><?=$company->excerpt?></p>
                                <p>Tel. <?=$company->tel?></p>
                                <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                            </div>
                        </td>
                        <td style="width: 50%;" >
                            <div class="box-check">
                              <?php if($info->collect == 1){?>
                             <div class="collect">
                                 เก็บเงินปลายทาง<br>
                                 จำนวน <?=$info->collect_price?> บาท
                             </div>
                             <?php }?>
                         </div>
                     </td>
                 </tr>
             </table>
             <table class="table-recipient" style="<?='top :'.$top2.'px'?> ">
                <tr>
                    <td style="width: 50%;">
                        <img style="height: 100px" src="<?=$this->config->item('template'); ?>assets/app/media/img/logos/stock-logo.png">
                    </td>
                    <td style="width: 50%;" valign="top">
                        <div class="box-check">
                            <p><u><strong>ผู้รับ</strong></u></p>
                            <p><?=$info->customer_fullname?></p>
                            <p><?=$info->customer_address.'<br>ตำบล '.$districts->name_th.' อำเภอ '.$amphures->name_th.' <br>จังหวัด '.$provinces->name_th.' <br>รหัสไปรษณีย์ '.$info->zip_code?></p>
                            <p>เบอร์โทรศัพท์. <?=$info->customer_tel?></p>
                            <br>
                            <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?=$info->order_code?></p>
                            <?php if( $info->supply == 1){?>
                            <br>
                            <div class="barcode" style="    margin-left: -30px;"><?=$barcode?></div>
                            <div class="barcode_img" style="    margin-left: -40px;"><?=$img_barcode?></div>
                            <p style="margin: 0px 0 0 50px;"><?="THDFL".$info->order_code?></p>
                        <?php }?>
                        </div>
                    </td>
                </tr>
            </table>

        </div>
    </page>                 
</div>    
</div>
</div>




