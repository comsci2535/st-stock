<style>
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}

table{
    width: 100%;
}
.table-print tr td{
    border: 1px solid;
}

.table-print td{
    border: 1px solid;
}
.table-print th{
    border: 1px solid;
}
.table-print p{
    margin: 0px 0px 0px 7px;
}


</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม print
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div  class="m-portlet__body">
            <page id="printarea" size="A4">
                <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print {
                        border-collapse: collapse;
                    }
                    .table-print tr{
                        border: 1px solid;
                    }   
                    .table-print td{
                        border: 1px solid;
                    }
                    .table-print th{
                        border: 1px solid;
                    }
                    .table-print p{
                        margin: 0px 0px 0px 7px;
                    }
                }
                </style>
                
                <div style="padding: 20px;">
                    <table style="width: 100%;">
                        <tr>
                            <td > 
                                <h3><?=$company->title?></h3>
                            </td>
                            <td >                               
                                <h4 style="border-radius: 10px; border: 2px solid; text-align: center; padding: 10px;">ใบสั่งสินค้า</h4>
                            </td>
                        </tr>
                        <tr>
                            <td style="line-height: 6px;">
                                <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                                <p>Tel. <?=$company->tel?></p>
                                <br>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table class="table-print" style="width: 100%;">
                        <tr>
                            <th style="width: 150px;text-align: left;"><p>เลขที่สั่งซื้อ</p></th>
                            <th><p>รายการสินค้า</p></th>
                            <th style="width: 100px;text-align: center;"><p>จำนวน</p></th>
                        </tr>
                        <?php
                       
                        $sumtotal = 0;
                        if(isset($info) && count($info) > 0):
                            foreach($info as $item):
                                if(isset($item['order_detail']) && count($item['order_detail']) > 0):
                                    foreach($item['order_detail'] as $key => $detail):
                                        $sumtotal += $detail['quantity'];
                                ?>
                            <tr style="">
                                <?php
                                if($key == 0):
                                ?>
                                    <td valign="top" rowspan="<?=count($item['order_detail'])?>" style="text-align: left;"><p><?=$item['order_code'];?></p></td>
                                <?php
                                endif;
                                ?>
                                <td style=""><p><?=$detail['title']?> <?=isset($detail['product_color']) ? '('.$detail['product_color'].')' : '';?></p></td>
                                <td style="text-align: center;"><p><?=$detail['quantity']?></p></td>
                            </tr>
                            <?php 
                                    endforeach;
                                endif;
                            endforeach;
                        endif;
                        ?>

                        <tr>
                            <th valign="top" colspan="2">
                                <p>รวมทั้งหมด</p>
                            </th>
                            <th style="text-align: center;">
                                <p><?=$sumtotal?></p>
                            </th>
                        </tr> 
                    </table>
                </div>
            </page>                 
        </div>    
    </div>
</div>




