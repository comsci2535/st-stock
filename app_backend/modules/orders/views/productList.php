<style type="text/css">
    @media (min-width: 992px){
        .modal-lg {
            max-width: 1224px;
        }
    }
</style>
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button id="btn-buy" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-cart-plus"></i> สังซื้อ</button>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <form role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>
                                <input name="tbCheckAll" type="checkbox" class="icheck tb-check-all">
                            </th>
                            <th>รูป</th>
                            <th>ชื่อสินค้า</th>
                            <th>แบรน</th>
                            <th>ราคาต้นทุน (บ.)</th>
                            <!-- <th>ราคาขาย (บ.)</th> -->
                            <th>ยอดในตะกร้าสินค้า (หน่วย)</th>
                            <th>ชื่อลูกค้า</th>
                            <th>ชื่อคนขาย</th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="session-buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">รายการสังซื้อ</h5>
                <button type="button" id="btn-order-delete-product" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="m-wizard m-wizard--2 m-wizard--success m-wizard--step-first" id="m_wizard">

                    <!--begin: Message container -->
                    <div class="m-portlet__padding-x">

                        <!-- Here you can put a message or alert -->
                    </div>

                    <!--end: Message container -->

                    <!--begin: Form Wizard Head -->
                    <div class="m-wizard__head m-portlet__padding-x">

                        <!--begin: Form Wizard Progress -->
                        <div class="m-wizard__progress">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                            </div>
                        </div>

                        <!--end: Form Wizard Progress -->

                        <!--begin: Form Wizard Nav -->
                        <div class="m-wizard__nav">
                            <div class="m-wizard__steps">
                                <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                    <a href="#" class="m-wizard__step-number">
                                        <span><i class="fa  fa-cart-plus "></i></span>
                                    </a>
                                    <div class="m-wizard__step-info">
                                        <div class="m-wizard__step-title">
                                            1. Order
                                        </div>
                                        <div class="m-wizard__step-desc">
                                            เพิ่มจำนวนสินคาที่ต้องการ
                                        </div>
                                    </div>
                                </div>
                                <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                    <a href="#" class="m-wizard__step-number">
                                        <span><i class="fa  fa-cart-arrow-down"></i></span>
                                    </a>
                                    <div class="m-wizard__step-info">
                                        <div class="m-wizard__step-title">
                                            2. Detail
                                        </div>
                                        <div class="m-wizard__step-desc">
                                            รายละเอียดการจัดส่ง
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <!--end: Form Wizard Nav -->
                    </div>

                    <!--end: Form Wizard Head -->

                    <!--begin: Form Wizard Form-->
                    <div class="m-wizard__form">
                       <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'id'=>'m_form', 'novalidate' =>'novalidate')) ?>
                       <!--  <form class="m-form m-form--label-align-left- m-form--state-" id="m_form" > -->

                        <!--begin: Form Body -->
                        <div class="m-portlet__body">

                            <!--begin: Form Wizard Step 1-->
                            <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="col-xl-12">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="form-group m-form__group row">
                                                    <table id="table-product" class="table table-striped m-table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th class="text-left" style="width: 30%;">ชื่อสินค้า</th>
                                                                <th class="text-left" style="width: 20%;">ราคาต้นทุน</th>
                                                                <th style="width: 10%;">ราคา</th>
                                                                <!-- <th style="width: 10%;">สินค้าคงเหลือ</th> -->
                                                                <th style="width: 10%;">จำนวน</th>
                                                                <th style="width: 15%;">ชื่อลูกค้า</th>
                                                                <th style="width: 15%;">ชื่อผู้ขายในตะกร้า</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Step 1-->

                            <!--begin: Form Wizard Step 2-->
                            <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h3 class="m-form__heading-title">ข้อมูลลูกค้า</h3>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="form-col-form-label">ชื่อ-นามสกุล<span class="text-danger"> *</span></label>
                                                    <input type="text" name="customer_fullname" class="form-control m-input" placeholder="ชื่อ-นามสกุล" required id="customer_fullname">
                                                    <label id="name-error-dup" class="error-dup" for="name" style="display: none;"></label>
                                                    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="form-col-form-label">เบอร์โทร<span class="text-danger"> *</span></label>
                                                    <input type="text" name="customer_tel" class="form-control m-input phone-with-add" placeholder="เบอร์โทร" value="" maxlength="15" required>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <label class="form-col-form-label">ที่อยู่<span class="text-danger"> *</span></label>
                                                    <textarea name="customer_address" id="" cols="" rows="3" class="form-control m-input"  placeholder="ที่อยู่" required></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group m-form__group row">
                                                <input type="hidden" id="datajson" name="datajson" value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                                    <input type="text" class="form-control m-input" id="district" placeholder="ตำบล"  name="districts_name">
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                                    <input type="text" class="form-control m-input" id="amphoe" placeholder="อำเภอ"  name="amphures_name">
                                                </div>                                                
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-col-form-label">จังหวัด<span class="text-danger"> *</span></label>
                                                    <input type="text" class="form-control m-input" id="province" placeholder="จังหวัด"  name="provinces_name">
                                                </div>
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger"> *</span></label>
                                                    <input type="text" class="form-control m-input" id="zipcode" placeholder="รหัสไปรษณีย์"  name="zip_code">
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="m-checkbox m-checkbox--state-primary">
                                                        <input type="checkbox" name="invoice" id="invoice" value="1"> ต้องการใบกำกับภาษี
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="m-view">  
                                                <hr>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-6 m-form__group-sub">
                                                        <label class="m-checkbox m-checkbox--state-primary">
                                                            <input type="checkbox" name="dupicate" id="dupicate" value="1"> ใช้ที่อยู่จัดส่ง
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-col-form-label">เลขที่ผู้เสียภาษี<span class="text-danger"> *</span></label>
                                                        <input type="text" name="vat_code" class="form-control m-input" placeholder="เลขที่ผู้เสียภาษี" value="ลูกค้าไม่ได้แจ้งเลขที่ผู้เสียภาษี" maxlength="64" required>
                                                    </div>
                                                </div>
                                                <div class="m-view-vat">  
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <label class="form-col-form-label">เบอร์โทรศัพท์<span class="text-danger"> *</span></label>
                                                            <input type="text" name="invoice_tel" class="form-control m-input phone-with-add" placeholder="เบอร์โทรศัพท์" value=""  required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <label class="form-col-form-label">ที่อยู่<span class="text-danger"> *</span></label>
                                                            <textarea name="customer_address_vat"  cols="" rows="3" class="form-control m-input"  placeholder="ที่อยู่" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <input type="hidden" id="datajson_vat" name="datajson_vat" value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                                            <input type="text" class="form-control m-input" id="district_vat" placeholder="ตำบล"  name="districts_name_vat">
                                                        </div>
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                                            <input type="text" class="form-control m-input" id="amphoe_vat" placeholder="อำเภอ"  name="amphures_name_vat">
                                                        </div>                                                
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">จังหวัด<span class="text-danger"> *</span></label>
                                                            <input type="text" class="form-control m-input" id="province_vat" placeholder="จังหวัด"  name="provinces_name_vat">
                                                        </div>
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger"> *</span></label>
                                                            <input type="text" class="form-control m-input" id="zipcode_vat" placeholder="รหัสไปรษณีย์"  name="zip_code_vat">
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>  

                                            <!-- จัดส่ง -->
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">การจัดส่ง</h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <label class="form-col-form-label">การขนส่ง<span class="text-danger"> *</span></label>
                                                        <select class="select2" name="supply">
                                                            <?php foreach ($supply as $key => $item) { ?>
                                                                <option value="<?=$item->supply_id?>"><?=$item->title?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-6 m-form__group-sub">
                                                    <label class="m-checkbox m-checkbox--state-primary">
                                                        <input type="checkbox" name="collect" id="payment" value="1"> เก็บปลายทาง
                                                        <span></span>
                                                    </label>
                                                </div>

                                            <div class="form-group m-form__group row m-view-sup">
                                                <div class="col-lg-12">
                                                    <label class="form-col-form-label">จำนวนเงินเก็บปลายทาง<span class="text-danger"> *</span></label>
                                                    <input type="text" name="collect_price" class="form-control m-input amount product_price" placeholder="จำนวนเงินเก็บปลายทาง" value=""  required>
                                                </div>
                                            </div>


                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Step 2-->
                        </div>

                        <!--end: Form Body -->

                        <!--begin: Form Actions -->
                        <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                            <div class="m-form__actions">
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 m--align-left">
                                        <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                            <span>
                                                <i class="la la-arrow-left"></i>&nbsp;&nbsp;
                                                <span>ย้อนกลับ</span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 m--align-right">
                                        <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                            <span>
                                                <i class="la la-check"></i>&nbsp;&nbsp;
                                                <span>บันทึก</span>
                                            </span>
                                        </a>
                                        <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                            <span>
                                                <span>ถัดไป</span>&nbsp;&nbsp;
                                                <i class="la la-arrow-right"></i>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>

                        <!--end: Form Actions -->
                    </form>
                </div>

                <!--end: Form Wizard Form-->
            </div>


        </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div> -->
        </div>
    </div>
</div>
<!--end::Modal-->