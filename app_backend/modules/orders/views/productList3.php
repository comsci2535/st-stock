<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button id="btn-buy" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-cart-plus"></i> สังซื้อ</button>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <form role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>
                                <input name="tbCheckAll" type="checkbox" class="icheck tb-check-all">
                            </th>
                            <th>รูป</th>
                            <th>ชื่อสินค้า</th>
                            <th>แบรน</th>
                            <th>ราคาต้นทุน</th>
                            <th>ราคาขาย</th>
                            <th>จำนวน</th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="session-buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-cart-plus"></i> รายการสังซื้อ</h5>
                <button type="button" id="btn-order-delete-product" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!--Begin::Main Portlet-->
				<div class="">
                    <!--begin: Form Wizard-->
                    <div class="m-wizard m-wizard--1 m-wizard--success" id="m_wizard">
                        <!--begin: Message container -->
                        <div class="m-portlet__padding-x">
                            <!-- Here you can put a message or alert -->
                        </div>

                        <!--end: Message container -->

                        <!--begin: Form Wizard Head -->
                        <div class="m-wizard__head m-portlet__padding-x">

                            <!--begin: Form Wizard Progress -->
                            <div class="m-wizard__progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <!--end: Form Wizard Progress -->

                            <!--begin: Form Wizard Nav -->
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>1</span></span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Product List
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span><span>2</span></span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Detail
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <!--end: Form Wizard Nav -->
                            </div>

                            <!--end: Form Wizard Head -->

                            <!--begin: Form Wizard Form-->
                            <div class="m-wizard__form">

                            <!--
                            1) Use m-form--label-align-left class to alight the form input lables to the right
                            2) Use m-form--state class to highlight input control borders on form validation
                            -->
                            <!-- <form class="m-form m-form--label-align-left- m-form--state-" id="m_form"> -->
                            <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'id'=>'m_form')) ?>
                                <!--begin: Form Body -->
                                <div class="m-portlet__body">

                                    <!--begin: Form Wizard Step 1-->
                                    <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="col-xl-12">
                                                    <div class="m-form__section m-form__section--first">
                                                        <div class="form-group m-form__group row">
                                                            <table id="table-product" class="table table-striped m-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th class="text-left">ชื่อสินค้า</th>
                                                                        <th>ราคา</th>
                                                                        <th>สินค้าคงเหลือ</th>
                                                                        <th>จำนวน</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end: Form Wizard Step 1-->
                                    <!--begin: Form Wizard Step 2-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                        <div class="row">
                                            <div class="col-xl-8 offset-xl-2">
                                                <div class="m-form__section m-form__section--first">
                                                    <div class="m-form__heading">
                                                        <h3 class="m-form__heading-title">ข้อมูลลูกค้า</h3>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <label class="form-col-form-label">ชื่อ-นามสกุล<span class="text-danger">*</span></label>
                                                            <input type="text" name="customer_fullname" class="form-control m-input" placeholder="ชื่อ-นามสกุล" value="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <label class="form-col-form-label">เบอร์โทร<span class="text-danger">*</span></label>
                                                            <input type="text" name="customer_tel" class="form-control m-input" placeholder="เบอร์โทร" value="" maxlength="10" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <label class="form-col-form-label">ที่อยู่<span class="text-danger">*</span></label>
                                                            <textarea name="customer_address" id="" cols="" rows="3" class="form-control m-input"  placeholder="ที่อยู่" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">จังหวัด<span class="text-danger">*</span></label>
                                                            <select class="form-control m-input m-input--square select2" id="provinces" name="provinces_id" required>
                                                                <option value="">เลือก</option>
                                                                <?php
                                                                if(isset($provinces) && count($provinces) > 0):
                                                                    foreach($provinces as $province):
                                                                        echo '<option value="'.$province->id.'">'.$province->name_th.'</option>';
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                                
												            </select>
                                                        </div>
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">อำเภอ<span class="text-danger">*</span></label>
                                                            <select class="form-control m-input m-input--square select2" id="amphures" name="amphures_id" required>
                                                                <option value="">เลือก</option>                      
												            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">ตำบล<span class="text-danger">*</span></label>
                                                            <select class="form-control m-input m-input--square select2" id="districts" name="districts_id" required>
                                                                <option value="">เลือก</option> 
												            </select>
                                                        </div>
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger">*</span></label>
                                                            <input type="text" name="zip_code" class="form-control m-input" placeholder="รหัสไปรษณีย์" maxlength="5" value="" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-6 m-form__group-sub">
                                                            <label class="m-checkbox m-checkbox--state-primary">
                                                                <input type="checkbox" name="invoice" value="1"> ต้องการใบกำกับภาษี
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 2-->
                                </div>

                                <!--end: Form Body -->

                                <!--begin: Form Actions -->
                                <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                    <div class="m-form__actions m-form__actions" style="border-top: 1px solid #ebedf2;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                    <span>
                                                        <i class="la la-arrow-left"></i>&nbsp;&nbsp;
                                                        <span>ย้อนกลับ</span>
                                                    </span>
                                                </button>
                                                <button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" style="float: right;">
                                                    <span>
                                                        <i class="la la-check"></i>&nbsp;&nbsp;
                                                        <span>บันทึก</span>
                                                    </span>
                                                    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                                                </button>
                                                <button class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next" style="float: right;">
                                                    <span>
                                                        <span>ถัดไป</span>&nbsp;&nbsp;
                                                        <i class="la la-arrow-right"></i>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Actions -->
                                <?php echo form_close() ?>

                            <!--end::Form-->
                        </div>
                        <!--end: Form Wizard Form-->
                    </div>
                    <!--end: Form Wizard-->
                </div>
            </div>
           
        </div>
    </div>
</div>
<!--end::Modal-->