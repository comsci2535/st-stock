<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Products extends MX_Controller {

    private $_title = "ข้อมูลสินค้า";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ข้อมูลสินค้า";
    private $_grpContent = "products";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->library('uploadfile_library');
        $this->load->model("products_m");
        $this->load->model("catalogs/catalogs_m");
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[0][] = action_barcode_multi(site_url("{$this->router->class}/barcode"));
        $action[1][] = action_refresh(site_url("{$this->router->class}"));
       // $action[1][] = action_filter();
        $action[1][] = action_add(site_url("{$this->router->class}/create"));
        //$action[2][] = action_export_group($export);
        $action[3][] = action_trash_multi("{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("{$this->router->class}/trash"));

        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->products_m->get_rows($input);
        $infoCount = $this->products_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }

        $input['cart_qty'] = Null;
        
        foreach ($info->result() as $key => $rs) {
            $input['catalog_id'] = $rs->catalog_id;
            $catalogs_name = $this->catalogs_m->get_rows($input)->row()->title;

            $id = encode_id($rs->product_id);
            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['img'] = '<img src="'.$this->config->item('root_url').$rs->product_img.'" class="img-thumbnail" style="width:100px !important;max-width:100px!important;">';
            $column[$key]['title'] = $rs->title.' ('.$rs->product_color.')';
            $column[$key]['brand'] = $catalogs_name;
            $column[$key]['price_cost'] = number_format($rs->product_price_cost,2);
            $column[$key]['price_sell'] = number_format($rs->product_price_sell,2);
            $column[$key]['quantity'] = '<span id="span-text-'.$rs->product_id.'">'.number_format($rs->product_quantity).'</span>';
            // $column[$key]['input'] = '<div class="input-group" id="text-'.$rs->product_id.'">
            //                         <input type="number" class="form-control text-cart"  max="'.number_format($rs->product_quantity).'" min="0" placeholder="ระบุจำนวน">
            //                         <div class="input-group-append">
            //                         <button type="button" class="btn btn-info btn-add-cart" data-id="'.$id.'" data-row-id="text-'.$rs->product_id.'"><i class="fas fa-cart-plus" aria-hidden="true"></i></button>
            //                         </div>
            //                         </div>';
            $column[$key]['input'] = '<div class="input-group btn-add-cart-open-modal" id="text-'.$rs->product_id.'" data-toggle="modal" data-target="#exampleModal" data-id="'.$rs->product_id.'"  data-max="'.number_format($rs->product_quantity).'" data-min="0" >
                                    
                                    <button type="button" class="btn btn-info  btn-add-cart-open" data-id="'.$id.'" data-row-id="text-'.$rs->product_id.'" id="btn-'.$rs->product_id.'"><i class="fas fa-cart-plus" aria-hidden="true"></i></button>
                                    </div>';      
            $column[$key]['sell_name'] = $rs->sell_name;
            $column[$key]['customer_name'] = $rs->customer_name;
            $column[$key]['active'] = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function create(){
        $this->load->module('template');
        $input['active'] = 1;
        $input['recycle'] = 0;
        $info = $this->catalogs_m->get_rows($input);
        $data['catalog'] = $info->result();

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/create";
        
        $this->template->layout($data);
    }
    
    public function save(){
        $ids =array();
        $input = $this->input->post(null, true);
        $value_arr = $this->_build_datas($input);
        $result = $this->products_m->insert_batch($value_arr);
        if ( $result ) {
           //Log inventory
           Modules::run('inventory/inventory_history', 'add',$result,$value_arr);  
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
       } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
       }
       redirect(site_url("{$this->router->class}"));
   }

   public function edit($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['product_id'] = $id;
    $info = $this->products_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;
    $input['active'] = 1;
    $input['recycle'] = 0;
    $catalogs = $this->catalogs_m->get_rows($input);
    $data['catalog'] = $catalogs->result();

    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/update");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/form";

    $this->template->layout($data);
}

public function update(){
    $input = $this->input->post(null, true);
    $id = decode_id($input['id']);
    $value = $this->_build_data($input);
    $result = $this->products_m->update($id, $value);
    if ( $result ) {
       if(!empty($input['product_quantity_new'])){
           Modules::run('inventory/inventory_history', 'append',$id,str_replace(",","",$input['product_quantity_new']),"เพิ่มจำนวนสินค้า โดย ".$this->session->users['Name']); 
       }
       if(!empty($input['product_quantity_minus'])){
           Modules::run('inventory/inventory_history', 'cut',$id,str_replace(",","","-".$input['product_quantity_minus']),"ลดจำนวนสินค้า โดย ".$this->session->users['Name']); 
       }  
       Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
   } else {
       Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
   }
   redirect(site_url("{$this->router->class}"));
}

private function _build_datas($input){
    if(count($input['product_color']) > 0 && count($input['product_price_cost']) > 0 && count($input['product_price_sell']) > 0 && count($input['product_quantity']) > 0 ){
        $i    = 0;
        $value_arr = array();
        $path = 'products';
        $img  = '';
        foreach($input['product_color'] as $item){

            $upload = $this->uploadfile_library->do_upload("file_$i",TRUE,$path);
            $file = '';
            if(isset($upload['index'])){
                $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                $outfile = $input['outfile'];
                if(isset($outfile)){
                    $this->load->helper("file");
                    unlink($outfile);
                }
                $img = $file;
            }
            array_push($value_arr,
                array(
                    'title'                 => $input['title']
                    ,'slug'                 => $input['slug']
                    ,'catalog_id'           => $input['catalog_id']
                    ,'detail'               => $input['detail']
                    ,'product_color'        => $item
                    ,'product_price_cost'   => str_replace(",","",$input['product_price_cost'][$i])
                    ,'product_price_sell'   => str_replace(",","",$input['product_price_sell'][$i])
                    ,'product_quantity'     => str_replace(",","",$input['product_quantity'][$i])
                    ,'barcode'              => $input['barcode'][$i]
                    ,'product_img'          => $img
                    ,'active'               => 1
                    ,'created_at'           => db_datetime_now()
                    ,'updated_at'           => db_datetime_now()
                    ,'created_by'           => $this->session->users['user_id']
                )
            );
            $i++;
        }
    }

    return $value_arr;
}

private function _build_data($input){
    $product_quantity   = str_replace(",","",$input['product_quantity']);
    if(!empty($input['product_quantity_new'])){
        $product_quantity   = str_replace(",","",$input['product_quantity']) + str_replace(",","",$input['product_quantity_new']);
    }
    if(!empty($input['product_quantity_minus'])){
        $product_quantity   = str_replace(",","",$input['product_quantity']) - str_replace(",","",$input['product_quantity_minus']);
    }

    $value['title']                 = $input['title'];
    $value['slug']                  = $input['slug'];
    $value['catalog_id']            = $input['catalog_id'];
    $value['detail']                = $input['detail'];
    $value['product_color']         = $input['product_color'];
    $value['product_price_cost']    = str_replace(",","",$input['product_price_cost']);
    $value['product_price_sell']    = str_replace(",","",$input['product_price_sell']);
    $value['product_quantity']      = $product_quantity;
    $value['barcode']               = $input['barcode'];


    $path = 'products';
    $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

    $file = '';
    if(isset($upload['index'])){
        $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
        $outfile = $input['outfile'];
        if(isset($outfile)){
            $this->load->helper("file");
            unlink($outfile);
        }
        $value['product_img'] = $file;
    }

    if ( $input['mode'] == 'create' ) {
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $this->session->users['user_id'];
    } else {
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
    }

    return $value;
}

public function barcodemulti(){

    $this->load->module('template');

    $input = $this->input->post();
    $arrayId = $input['arr_code'];
    
    $product_arr = array();
    foreach($arrayId as $key => $item_id){
        $input['product_id'] = decode_id($item_id);
            // arr($input['product_id']);
        $info = $this->products_m->get_rows($input)->row();

        array_push($product_arr, array(
            'barcode'            => $info->barcode,
            'title'              => $info->title,
            'product_price_sell' => $info->product_price_sell
        ));
    }
    $data['info'] = $product_arr;

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('บาร์โค้ด', site_url("{$this->router->class}/barcodemulti"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/barcode_muti";
    $data['pageScript'] = "scripts/backend/products/barcode.js";

    $this->template->layout($data);
}

public function excel(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->products_m->get_rows($input);
    $fileName = "products";
    $sheetName = "Sheet name";
    $sheetTitle = "Sheet title";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $styleH = excel_header();
    $styleB = excel_body();
    $colums = array(
        'A' => array('data'=>'รายการ', 'width'=>50),
        'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
        'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
    );
    $fields = array(
        'A' => 'title',
        'B' => 'excerpt',
        'C' => 'created_at',            
        'D' => 'updated_at',
    );
    $sheet->setTitle($sheetName);

        //title
    $sheet->setCellValue('A1', $sheetTitle);
    $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
    $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    $sheet->getRowDimension(1)->setRowHeight(20);

        //header
    $rowCount = 2;
    foreach ( $colums as $colum => $data ) {
        $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
        $sheet->SetCellValue($colum.$rowCount, $data['data']);
        $sheet->getColumnDimension($colum)->setWidth($data['width']);
    }

        // data
    $rowCount++;
    foreach ( $info->result() as $row ){
        foreach ( $fields as $key => $field ){
            $value = $row->{$field};
            if ( $field == 'created_at' || $field == 'updated_at' )
                $value = datetime_table ($value);
            $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
            $sheet->SetCellValue($key . $rowCount, $value); 
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $rowCount++;
    }
    $sheet->getRowDimension($rowCount)->setRowHeight(20);  
    $writer = new Xlsx($spreadsheet);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
    $writer->save('php://output');
    exit; 
}

public function pdf(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->products_m->get_rows($input);
    $data['info'] = $info;

    $mpdfConfig = [
        'default_font_size' => 9,
        'default_font' => 'garuda'
    ];
    $mpdf = new \Mpdf\Mpdf($mpdfConfig);
    $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
    $mpdf->WriteHTML($content);


    $download = FALSE;
    $saveFile = FALSE;
    $viewFile = TRUE;
    $fileName = "products.pdf";
    $pathFile = "uploads/pdf/products/";
    if ( $saveFile ) {
        create_dir($pathFile);
        $fileName = $pathFile.$fileName;
        $mpdf->Output($fileName, 'F');
    }
    if ( $download ) 
        $mpdf->Output($fileName, 'D');
    if ( $viewFile ) 
        $mpdf->Output();
}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->products_m->get_rows($input);
    $infoCount = $this->products_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->product_id);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}    

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();

        if (is_array($input['id']) || is_object($input['id']))
        {
            foreach ( $input['id'] as &$rs ){
                $rs = decode_id($rs);
            }
        }else{
            $rs = decode_id($input['id']);
        }

        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;

        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->products_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->products_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->products_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->products_m->update_in($input['id'], $value);
        } 

        if ( $type == "cart" ) {
            $value['product_quantity'] = $this->input->post('balance');
            $result = $this->products_m->update(decode_id($input['id']), $value);

            $data['sell_name'] = $this->input->post('sell_name');
            $data['customer_name'] = $this->input->post('customer_name');
            $data['cart_qty'] = $this->input->post('cart_qty');
            Modules::run('inventory/inventory_history', 'cart',decode_id($input['id']),$this->input->post('cart_qty'),'หยิบใส่ตะกร้าสินค้า โดย '.$this->session->users['Name']);
            Modules::run('cart/cartinfo', $input['id'],$data);
           
        }


        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    }
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));        
}  

public function sumernote_img_upload(){
		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
   $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
}

echo $picture;

}

public function deletefile($ids){

  $arrayName = array('file' => $ids);

  echo json_encode($arrayName);
}

}
