<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('products a')
                        ->order_by('a.product_id', 'desc')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('products a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.product_quantity', $param['keyword'])
                    ->or_like('a.product_price_cost', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['title']) && $param['title'] != "" ) {
            $this->db->like('a.title', $param['title']);
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.product_quantity', $param['search']['value'])
                    ->or_like('a.product_color', $param['search']['value'])
                    ->or_like('a.product_price_cost', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['product_id']) ) 
            $this->db->where('a.product_id', $param['product_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( isset($param['proarraId']) )
            $this->db->where_in('a.product_id', $param['proarraId']);

        if ( !empty($param['catalog_id']))
            $this->db->where('a.catalog_id', $param['catalog_id']);

    }
    
    public function insert($value) {
        $this->db->insert('products', $value);
        return $this->db->insert_id();
    }

    public function insert_batch($value) {
        $this->db->insert_batch('products', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('product_id', $id)
                        ->update('products', $value);
        return $query;
    }

    public function update_uncart($id, $value)
    {
        $query = $this->db
                        ->where('product_id', $id)
                        ->set('product_quantity','product_quantity+'.$value['cart_qty'],FALSE)
                        ->update('products');
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('product_id', $id)
                        ->update('products', $value);
        return $query;
    }

    public function product_order($value)
    {
        $info = $this->db->get_where('products_order', array('product_id' => $value['product_id'], 'product_order_session' => $this->session->userdata('userId')));

        if ($info->num_rows()>0) {
            if ($value['product_order_total'] > 0) {
                $values     = 'product_order_total+1';
                $values_pro = 'product_quantity-1';
                $cart_pro   = 'cart_quantity-1';
                $draw_pro   =  'draw-1';
            } else {
                $values     = 'product_order_total-1';
                $values_pro = 'product_quantity+1';
                $cart_pro   = 'cart_quantity+1'; 
                $draw_pro   =  'draw+1';
            }
            /**Table Products */
            $this->db->set('cart_quantity', $cart_pro, FALSE);
            $this->db->set('draw', $draw_pro, FALSE);
            $this->db->where_in('product_id', $value['product_id']);
            $this->db->update('carts');

            /**Table products Order*/
            $this->db->set('updated_at', $value['updated_at']);
            $this->db->set('product_order_total', $values, FALSE);
            $this->db->where_in('product_id', $value['product_id']);
            $this->db->where_in('product_order_session', $this->session->userdata('userId'));
            $query = $this->db->update('products_order');

            return $query;
        } else {
            $this->db->set('cart_quantity', 'cart_quantity-1', FALSE);
            $this->db->set('draw', 'draw-1', FALSE);
            $this->db->where_in('product_id', $value['product_id']);
            $this->db->update('carts');

            $this->db->insert('products_order', $value);
            return $this->db->insert_id();
        }
    }

    public function product_cancel($value)
    {
            $set = 'product_quantity+'.$value['quantity'];
            /**Table Products */
            $this->db->set('product_quantity', $set, FALSE);
            $this->db->where('product_id', $value['product_id']);
            $query = $this->db->update('products');
            return $query;
        
    }

}
