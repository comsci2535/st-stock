<style>
page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
    width: 21cm;
    /* height: 29.7cm;  */
    height: auto; 
}

.col-3 {
    margin: 5px;
    padding-top: 5px;
    width: 23.5%;
    text-align: center;
    border: 1px solid;
    max-width: 23.5%;
}
.row{
    padding: 30px;
}

.fontBarcode{
    font-size: 40px;
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        บาร์โค้ด
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                    <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div  class="m-portlet__body">
            <page id="printarea" size="A4">

            <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .col-3 {
                        margin: 3px;
                        padding-top: 5px;
                        width:23.5%; 
                        float:left;
                        text-align: center;
                        border: 1px solid;
                        min-height: 146px;
                        max-height: 150px;
                    }

                    @font-face {
                        font-family: fontBarcode;
                        src: url('../../../../template/metronic/assets/font/code-128.ttf');
                        }

                        .fontBarcode{
                            font-family: fontBarcode;
                            font-size: 40px;
                        }
                    
                }
                </style>

                    <div class="row">
                        <?php 
                            if(isset($info) && count($info) > 0){
                                foreach($info as $key => $item){
                        ?>

                        <div class="col-3">
                            <span><?=$item['title']?></span>
                            <br>
                            <span>ราคา : <?=number_format($item['product_price_sell'])?> บาท</span>
                            <br>
                            <span class="fontBarcode"><?=$item['barcode']?></span>
                            <br>
                            <span><?=$item['barcode']?></span>
                        </div>

                        <?php 
                                }
                            }
                        ?>

                    </div>
            </page>                 
        </div>    
    </div>
</div>




