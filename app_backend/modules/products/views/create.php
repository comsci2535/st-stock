<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        แบบฟอร์ม สร้างใหม่
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>


        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">ชื่อสินค้า <span class="text-danger">*</span></label>
                <div class="col-sm-7">
                    <input value="" type="text" class="form-control m-input m-input--square" placeholder="ระบุชื่อสินค้า"
                        name="title" id="title" required>
                    <label id="name-error-dup" class="error-dup" for="name"
                        style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                </div>
            </div>
            <div class="form-group m-form__group row d-none">
                <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                <div class="col-sm-7">
                    <input value="" type="hidden" class="form-control m-input m-input--square" name="slug" id="slug">
                    <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug
                        นี้ในระบบแล้ว.</label>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="catalog_id">ชื่อแบรน <span
                        class="text-danger">*</span></label>
                <div class="col-sm-7">
                    <select id="catalog_id" name="catalog_id" class="form-control m-input m-input--square select2"
                        required>
                        <option value="">เลือก</option>
                        <?php
                            if(isset($catalog)):
                                $selected = '';
                                foreach($catalog as $item):
                                    if($info->catalog_id == $item->catalog_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                    
                                ?>
                        <option value="<?=$item->catalog_id;?>" <?=$selected?>>
                            <?=$item->title;?>
                        </option>
                        <?php
                                endforeach;
                            
                            endif;
                            ?>

                    </select>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                <div class="col-sm-7">
                    <textarea name="detail" rows="3" class="form-control summernote" id="detail"></textarea>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="product_color">รายละเอียดเพิ่มเติม <span
                        class="text-danger">*</span></label>
                <div class="col-sm-10 list-count">
                    <div class="row bg-light p-3 mt-4 more-details" id="tr-0">
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group m-form__group">
                                        <label for="">สี</label>
                                        <input value="" type="text"
                                            class="form-control m-input m-input--square product_color"
                                            name="product_color[0]" placeholder="ระบุสี" required>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="">จำนวน</label>
                                        <input value="" type="text"
                                            class="form-control m-input m-input--square product_quantity amount"
                                            name="product_quantity[0]" placeholder="ระบุจำนวน" required>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="">บาร์โค้ด</label>
                                        <input value="" type="text" class="form-control m-input m-input--square barcode"
                                            maxlength="15" placeholder="บาร์โค้ด 15 ตัวอักษร" name="barcode[0]"
                                            id="barcode" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group m-form__group">
                                        <label for="">ราคาต้นทุน</label>
                                        <input value="" type="text"
                                            class="form-control m-input m-input--square product_price_cost amount"
                                            name="product_price_cost[0]" placeholder="ระบุราคาต้นทุน" required>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="">ราคาขาย</label>
                                        <input value="" type="text"
                                            class="form-control m-input m-input--square product_price_sell amount"
                                            name="product_price_sell[0]" placeholder="ระบุราคาขาย">
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-3">
                                    <div class="form-group m-form__group">
                                        <label for="">รูปภาพ</label>
                                        <input name="file_0" type="file" class="file_create"
                                            data-preview-file-type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 align-self-center">
                            <div class="form-group m-form__group">
                                <button type="button" class="btn btn-danger btn-sm btn-detail-delete" data-id="tr-0"><i
                                        class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-2">
                    <div class="mt-3">
                        <button id="btn-detail-add" type="button" class="btn btn-success btn-sm"><i
                                class="fa fa-plus"></i> เพิ่ม</button>
                    </div>
                </div>
              
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>

            </div>
        </div>
        <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
        <input type="hidden" class="form-control" name="db" id="db" value="products">
        <input type="hidden" name="id" id="input-id"
            value="<?php echo isset($info->product_id) ? encode_id($info->product_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>



</div>