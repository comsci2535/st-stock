<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
            <div class="m-portlet__body">

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">บาร์โค้ด <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->barcode) ? $info->barcode : NULL ?>" type="text" class="form-control m-input m-input--square" maxlength="15"
                            placeholder="บาร์โค้ด 15 ตัวอักษร" name="barcode" id="barcode" required>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ชื่อสินค้า <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input m-input--square" name="title" id="title" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <!-- <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                    <div class="col-sm-7"> -->
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="hidden" class="form-control m-input m-input--square" name="slug" id="slug" >
                        <!-- <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label> -->
                    <!-- </div>
                </div> -->
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="catalog_id">ชื่อแบรน <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <select id="catalog_id" name="catalog_id" class="form-control m-input m-input--square select2" required>
                            <option value="">เลือก</option>
                            <?php
                            if(isset($catalog)):
                                $selected = '';
                                foreach($catalog as $item):
                                    if($info->catalog_id == $item->catalog_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                    
                                ?>
                                    <option value="<?=$item->catalog_id;?>" <?=$selected?>><?=$item->title;?></option>
                                <?php
                                endforeach;
                            
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control summernote" id="detail" required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>  

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_color">สี <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->product_color) ? $info->product_color : NULL ?>" type="text" class="form-control m-input m-input--square" name="product_color" id="product_color" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_price_cost">ราคาต้นทุน <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->product_price_cost) ? number_format($info->product_price_cost) : NULL ?>" type="text" class="form-control m-input m-input--square amount" name="product_price_cost" id="product_price_cost" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_price_sell">ราคาขาย </label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->product_price_sell) ? number_format($info->product_price_sell) : NULL ?>" type="text" class="form-control m-input m-input--square amount" name="product_price_sell" id="product_price_sell" required>
                    </div>
                </div>
                <?php 
                     $st_text = "";
                     $st_dis = "";
                    if ($this->router->method == 'edit') : 
                        $st_text = "(เดิม)"; 
                        $st_dis = "readonly";
                    endif;
                 ?>    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_quantity">จำนวนสินค้า <?=$st_text ?><span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input  value="<?php echo isset($info->product_quantity) ? number_format($info->product_quantity) : NULL ?>" type="text" class="form-control m-input m-input--square amount" name="product_quantity" id="product_quantity" <?=$st_dis?>>
                    </div>
                </div>
                <?php if ($this->router->method == 'edit') :  ?>
                <hr>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_quantity_new">จำนวนสินค้า <span class="text-danger">(เพิ่มใหม่)</span></label>
                    <div class="col-sm-7">
                        <input  type="text" class="form-control m-input m-input--square amount" name="product_quantity_new" placeholder="ใส่จำนวนสินค้าที่ต้องการเพิ่มเพื่อบวกเพิ่มกับสินค้าที่มีอยู่เดิม">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_quantity_minus">จำนวนสินค้า <span class="text-danger">(ลดลง)</span></label>
                    <div class="col-sm-7">
                        <input  type="text" class="form-control m-input m-input--square amount" name="product_quantity_minus" placeholder="ใส่จำนวนสินค้าที่ต้องการเอาออกเพื่อลบจำนวนสินค้าที่มีอยู่เดิม">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="reason">หมายเหตุ </label>
                    <div class="col-sm-7">
                       <textarea class="form-control m-input m-input--square" rows="4" placeholder="หมายเหตุของการเพิ่ม หรือ ลด จำนวนสินค้า" name="reason"></textarea>
                    </div>
                </div>
                <hr>
               <?php endif; ?>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">ไฟล์ <span class="text-danger">*</span></label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                        <input type="hidden" name="outfile" value="<?=(isset($info->product_img)) ? $info->product_img : ''; ?>">
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="products">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->product_id) ? encode_id($info->product_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true; 
    var file_image      = '<?=(isset($info->product_img)) ? $this->config->item('root_url').$info->product_img : ''; ?>';
    var file_id         = '<?=$info->product_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->product_id;?>';

</script>




