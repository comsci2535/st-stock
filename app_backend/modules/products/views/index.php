
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive  responsive" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>รูป</th>
                            <th>ชื่อสินค้า</th>
                            <th>แบรน</th>
                            <th>ราคาต้นทุน (บ.)</th>
                            <th>ราคาขาย (บ.)</th>
                            <th>คงเหลือ (หน่วย)</th>
                            <th>หยิบใส่ตะกร้า (หน่วย)</th>
                            <th>สร้าง</th>
                            <th>แก้ไข</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="cart_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">หยิบใส่ตะกร้า</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="modal-form" onsubmit="return false">
      <div class="modal-body">
          <input type="hidden" class="form-control" id="cart-id" name="id">
          <input type="hidden" class="form-control" id="cart-max" >
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">จำนวนที่จะใส่ตะกร้า: (สูงสุดที่จะใส่ได้ คือ <span id="cart-value-label"></span> ชิ้น)</label>
            <input type="number" class="form-control" id="cart-value" name="cart" min="0" required>
          </div>
          <div class="form-group">
            <label for="sell-name" class="col-form-label">ชื่อพนักขาย:</label>
            <input type="text" class="form-control" id="sell-name" name="sell-name" required>
          </div>
          <div class="form-group">
            <label for="customer-name" class="col-form-label">ชื่อลูกค้าที่จะขาย:</label>
            <input type="text" class="form-control" id="customer-name" name="customer-name" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก/ปิด</button>
        <button type="submit" id="btn-cart-send" class="btn btn-primary">ตกลง</button>
      </div>
      </form>
    </div>
  </div>
</div>


