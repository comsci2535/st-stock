<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Provinces extends MX_Controller {

    public function __construct(){
        parent::__construct();
        
        $this->load->model("provinces_m");
    }

    public function index(){
        //echo 'test';
       var_dump($this->provinces_m->viewAll());

        // $this->output
        //         ->set_content_type('application/json')
        //         ->set_output(json_encode($data));
    }

    public function viewAmphures(){
        $input = $this->input->post();
        $amphures_id = $input['provinces_id'];

        $data['amphures'] = $this->provinces_m->viewAmphuresBykey($amphures_id);

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function viewDistricts(){
        $input = $this->input->post();
        $districts_id = $input['districts_id'];

        $data['districts'] = $this->provinces_m->viewDistrictsBykey($districts_id);

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }


}
