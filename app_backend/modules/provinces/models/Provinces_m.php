<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Provinces_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function viewAll(){
        $query = $this->db->get('provinces');
        return $query->result();
    }

    public function viewAmphuresBykey($key){
        $query = $this->db->where('province_id',$key)->get('amphures');
        return $query->result();
    }

    public function viewDistrictsBykey($key){
        $query = $this->db->where('amphure_id',$key)->get('districts');
        return $query->result();
    }

    public function get_DistrictsBykey($key){
        $query = $this->db->where('id',$key)->get('districts');
        return $query;
    }

    public function get_AmphuresBykey($key){
        $query = $this->db->where('id',$key)->get('amphures');
        return $query;
    }

    public function get_ProvincesBykey($key){
        $query = $this->db->where('id',$key)->get('provinces');
        return $query;
    }

    public function get_DistrictsByName($name,$amphure_id){
        $query = $this->db->like('name_th',$name)->where('amphure_id',$amphure_id)->get('districts')->row();
        return $query;
    }

     public function get_AmphuresByName($name,$province_id){
        $query = $this->db->like('name_th',$name)->where('province_id',$province_id)->get('amphures')->row();
        return $query;
    }

    public function get_ProvincesByName($name){
        $query = $this->db->like('name_th',$name)->get('provinces')->row();
        return $query;
    }


}
