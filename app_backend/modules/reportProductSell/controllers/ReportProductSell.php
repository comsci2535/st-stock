<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportProductSell extends MX_Controller {

    private $_title = "สรุปจำนวนการขายสินค้ารายวัน";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ สรุปจำนวนการขายสินค้า";
    private $_grpContent = "reportProductSell";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("reportProductSell_m");
    }

    public function index(){
        $this->load->module('template');

        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            // 'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_export_group($export);
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['dateRang'] = date('01/m/Y').' ถึง '.date('d/m/Y');
        $data['createStartDate'] = date('Y-m-01');
        $data['createEndDate'] = date('Y-m-d');
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";

        $this->template->layout($data);
    }

    public function data_index(){
        $input = $this->input->post();

        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }

        $info = $this->reportProductSell_m->get_join_order_detail($input);

        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input;
            $this->session->set_userdata("condition", $condition);
        }

        foreach ($info->result() as $key => $rs) {
            $column[$key]['title'] = $rs->title.' ('.$rs->product_color.')';
            $column[$key]['brand'] = $rs->brand;
            $column[$key]['product_price_cost'] = number_format($rs->product_price_cost,2);
            $column[$key]['product_price'] = number_format($rs->product_price,2);
            $column[$key]['count_product'] = number_format($rs->quantity_buy);
            $column[$key]['total'] = number_format($rs->buy ,2);
            $column[$key]['vat'] = number_format($rs->vat,2);
            $column[$key]['buy'] = number_format($rs->buyed,2);
            $column[$key]['profit'] = number_format($rs->buy - ($rs->product_price_cost * $rs->quantity_buy),2);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $info->num_rows();
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');

        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        // $info = $this->reportProductSell_m->get_rows($input);
        $info = $this->reportProductSell_m->get_join_order_detail($input);
        $fileName = "สรุปจำนวนการขายสินค้า";
        $sheetName = "Sheet name";
        $sheetTitle = "สรุปจำนวนการขายสินค้า";

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'ชื่อสินค้า', 'width'=>20),
            'B' => array('data'=>'แบรน','width'=>15),
            'C' => array('data'=>'ราคาต้นทุน (บ.)', 'width'=>20,),
            'D' => array('data'=>'ราคาขาย (ต่อหน่วย)', 'width'=>20),
            'E' => array('data'=>'จำนวนขายทั้งหมด (หน่วย)', 'width'=>25),
            'F' => array('data'=>'จำนวนเงิน (บ.)', 'width'=>20),
            'G' => array('data'=>'ภาษีมูลค่าเพิ่ม (บ.)', 'width'=>20),
            'H' => array('data'=>'ราคาสินค้า (บ.)', 'width'=>20),
            'I' => array('data'=>'กำไร (บ.)', 'width'=>20),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'brand',
            'C' => 'product_price_cost',
            'D' => 'product_price',
            'E' => 'quantity_buy',
            'F' => 'buy',
            'G' => 'vat',
            'H' => 'buyed',
            'I' => 'profit',
        );
        $sheet->setTitle($sheetName);

        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('I1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('I1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);

        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }

        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                if ( $field == 'title')
                    $value = $row->title.' ('.$row->product_color.')';
                if ( $field == 'sum_price')
                    $value = number_format($row->sum_price,2);
                if ( $field == 'profit')
                    $value = number_format($rs->buy - ($rs->product_price_cost * $rs->quantity_buy),2);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value);
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit;
    }

    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');

        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->reportProductSell_m->get_rows($input);
        $data['info'] = $info;

        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);


        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "reportProductSell.pdf";
        $pathFile = "uploads/pdf/reportProductSell/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download )
            $mpdf->Output($fileName, 'D');
        if ( $viewFile )
            $mpdf->Output();
    }
}
