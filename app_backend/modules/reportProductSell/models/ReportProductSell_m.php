<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ReportProductSell_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_count($param)
    {
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }

        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_id', $param['search']['value'])
                    ->or_like('c.title', $param['search']['value'])
                    ->or_like('d.title', $param['search']['value'])
                    ->group_end();
        }

        $query = $this->db
                        ->select('c.title, c.product_color, d.title as brand, c.product_price_cost, a.product_price, a.product_id, sum(a.product_price) as sum_price, count(a.product_id) as count_product')
                        ->from('orders_detail a')
                        ->join('products c', 'a.product_id = c.product_id', 'INNER')
                        ->join('catalogs d', 'c.catalog_id = d.catalog_id', 'left')
                        ->group_by('a.product_id,a.product_price')
                        ->get();

        return $query->num_rows();
    }

    public function get_join_order_detail($param)
    {

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }

        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_id', $param['search']['value'])
                    ->or_like('c.title', $param['search']['value'])
                    ->or_like('d.title', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 0) $columnOrder = "c.title";
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 7) $columnOrder = "buyed";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        }

        $query = $this->db
                ->select('a.order_id,a.product_id, c.title, c.product_color, d.title as brand, c.product_price_cost,
                 a.product_price,sum(a.quantity) as quantity_buy ,
                 (product_price * sum(a.quantity)) as buy ,
                 ((product_price * (sum(a.quantity) * 7)/107)) as vat,
                ((product_price * sum(a.quantity)) - ((product_price * (sum(a.quantity) * 7))/107)) as buyed')
                ->from('orders_detail a')
                ->join('products c', 'a.product_id = c.product_id', 'INNER')
                ->join('catalogs d', 'c.catalog_id = d.catalog_id', 'left')
                ->group_by('a.product_id,a.product_price,a.order_id')
                ->order_by('a.product_id','ASC')
                ->get();

        return $query;
    }


}
