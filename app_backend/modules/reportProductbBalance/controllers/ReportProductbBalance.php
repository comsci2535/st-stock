<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportProductbBalance extends MX_Controller {

    private $_title = "สรุปสินค้าคงเหลือ";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ สรุปสินค้าคงเหลือ";
    private $_grpContent = "reportProductbBalance";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->library('m_pdf');
        $this->load->model("products/products_m");
        $this->load->model("catalogs/catalogs_m");
    }


    public function index(){
        $this->load->module('template');

        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            // 'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_export_group($export);
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";

        $input['recycle'] = 0;
        $info_catalogs = $this->catalogs_m->get_rows($input);
        $data['catalogs'] = $info_catalogs->result_array();

        $this->template->layout($data);
    }

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->products_m->get_rows($input);
        $infoCount = $this->products_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input;
            $this->session->set_userdata("condition", $condition);
        }

        foreach ($info->result() as $key => $rs) {
            $input['catalog_id'] = $rs->catalog_id;
            $catalogs_name = $this->catalogs_m->get_rows($input)->row()->title;

            // $column[$key]['img'] = '<img src="'.$this->config->item('root_url').$rs->product_img.'" class="img-thumbnail" style="width:150px !important;">';
            $column[$key]['title'] = $rs->title.' ('.$rs->product_color.')';
            $column[$key]['brand'] = $catalogs_name;
            $column[$key]['price_cost'] = number_format($rs->product_price_cost,2);
            $column[$key]['price_sell'] = number_format($rs->product_price_sell,2);
            $column[$key]['quantity']   = number_format($rs->product_quantity);
        }
        $data['data'] =  $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');

        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        // $info = $this->reportProductbBalance_m->get_rows($input);
        $info = $this->products_m->get_rows($input);
        $fileName = "สรุปสินค้าคงเหลือ";
        $sheetName = "Sheet name";
        $sheetTitle = "สรุปสินค้าคงเหลือ";

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'ชื่อสินค้า', 'width'=>20),
            'B' => array('data'=>'แบรน','width'=>15),
            'C' => array('data'=>'ราคาต้นทุน (บ.)','width'=>20),
            'D' => array('data'=>'ราคาขาย (บ.)','width'=>20),
            'E' => array('data'=>'คงเหลือ','width'=>20),
            'F' => array('data'=>'วันที่สร้าง', 'width'=>16),

        );
        $fields = array(
            'A' => 'title',
            'B' => 'brand',
            'C' => 'product_price_cost',
            'D' => 'product_price_sell',
            'E' => 'product_quantity',
            'F' => 'created_at',
        );
        $sheet->setTitle($sheetName);

        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('F1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('F1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);

        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }

        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            $input['catalog_id'] = $row->catalog_id;
            $catalogs_name = $this->catalogs_m->get_rows($input)->row()->title;

            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                if ( $field == 'title')
                    $value =   $row->title.' ('.$row->product_color.')';
                if ( $field == 'brand')
                    $value =  $catalogs_name;
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value);
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit;
    }

    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');

        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }

        $info = $this->reportProductbBalancerts_m->get_rows($input);

        $data['info'] = $info->result();

        //$data = [];
        //load the view and saved it into $html variable
        $html=$this->load->view("{$this->router->class}/pdf", $data, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = date('Ymdhis')."-order.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }
}
