
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction;?>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-2 col-form-label text-right">แบรน :</label>
                <div class="col-3">
                    <select name="catalog_id" id="catalog_id" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        if(isset($catalogs) && count($catalogs)):
                            foreach($catalogs as $key => $item):
                            echo '<option value="'.$item['catalog_id'].'">'.$item['title'].'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-2 col-form-label text-right">ชื่อสินค้า :</label>
                <div class="col-3">
                    <input type="text" name="pro_title" id="pro_title"class="form-control" placeholder="กรุณากรอกข้อมูล">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-2 col-form-label"></label>
                <div class="col-4">
                    <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                    <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
                </div>
            </div>

            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <!-- <th>รูป</th> -->
                            <th>ชื่อสินค้า</th>
                            <th>แบรน</th>
                            <th>ราคาต้นทุน (บ.)</th>
                            <th>ราคาขาย (บ.)</th>
                            <th>คงเหลือ (หน่วย)</th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>