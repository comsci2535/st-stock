<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Reports extends MX_Controller {

    private $_title = "สรุปการขายรายวัน";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ สรุปการขาย";
    private $_grpContent = "reports";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->status=array('รับออเดอร์','แพ็คสินค้า','ส่งแล้ว','ยกเลิก');
        $this->load->library('ckeditor');
        $this->load->library('m_pdf');
        $this->load->model("reports_m");
        $this->load->model("orders/orders_m");
        $this->load->model("supply/supply_m");
        $this->load->model("provinces/provinces_m");
    }
    
    public function index(){
        $this->load->module('template');
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            // 'pdf' => site_url("{$this->router->class}/pdf"),
        );
        $action[1][] = action_export_group($export);
        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['dateRang'] = date('01/m/Y').' ถึง '.date('d/m/Y');
        $data['createStartDate'] = date('Y-m-01');
        $data['createEndDate'] = date('Y-m-d');

        $data['supply'] = $this->supply_m->get_all()->result();

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        
        $input['recycle'] = 0;
        $info = $this->orders_m->get_rows($input);

        $infoCount = $this->orders_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->order_id);
            $sum_order = $this->reports_m->get_sum_order_detail($rs->order_id);

            $total      = 0;
            $sum_total  = 0;
            //คำนวณยอดรวม
            $info_detail = $this->reports_m->get_order_detail($rs->order_id);

            foreach($info_detail->result() as $value){
                $total      = $value->product_price * $value->quantity;
                $sum_total += $total;
                $vat = (($sum_total*7)/107);
                $buy = $sum_total - $vat;
                $cost = ($value->product_price_cost * $value->quantity);
                $profit = $sum_total - $cost;
            }

            $statusPicker = $this->status[$rs->order_status];

            $vat_code = 'IV'.substr($rs->order_code, 2,4 ).substr($rs->created_at,8,2).'/'.substr($rs->order_code, 6 );
            //$active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['order_code'] = $rs->order_code;
            $column[$key]['vat_code'] =  $vat_code;
            $column[$key]['fullname'] = $rs->customer_fullname;
            $column[$key]['tracking_code'] = $rs->tracking_code;
            $column[$key]['sup_title'] = $rs->sup_title;
            $column[$key]['collect_price'] = $rs->collect_price;
            $column[$key]['tel'] = $rs->customer_tel;
            // $column[$key]['total'] = isset($sum_order->sum_price)? $sum_order->sum_price : 0;
            $column[$key]['total_list'] = isset($sum_order->sum_qty)? $sum_order->sum_qty : 0;
            $column[$key]['total'] = number_format($sum_total,2);
            $column[$key]['vat'] = number_format($vat,2);
            $column[$key]['buy'] = number_format($buy,2);
            $column[$key]['cost'] = number_format($cost,2);
            $column[$key]['profit'] = ($rs->order_status != 3) ? number_format($profit,2): 'ไม่มี';
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['active'] = $statusPicker;
            // $column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->reports_m->get_rows($input);
        $fileName = "สรุปรายการขาย";
        $sheetName = "Sheet name";
        $sheetTitle = "สรุปรายการขาย";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'ชื่อ-นามสกุล','width'=>20),
            'B' => array('data'=>'เลขพัสดุ','width'=>20),
            'C' => array('data'=>'ขนส่ง','width'=>16),
            'D' => array('data'=>'เก็บเงินปลาย(บาท)','width'=>16),
            'E' => array('data'=>'OrderID', 'width'=>25),
            'F' => array('data'=>'เลขที่ใบกำกับภาษี', 'width'=>25),
            'G' => array('data'=>'ที่อยู่ที่จัดส่ง', 'width'=>100),
            'H' => array('data'=>'เบอร์โทร','width'=>16),
            'I' => array('data'=>'จำนวนขายทั้งหมด (หน่วย)', 'width'=>25),            
            'J' => array('data'=>'จำนวนเงิน (บ.)', 'width'=>20),
            'K' => array('data'=>'ภาษีมูลค่าเพิ่ม (บ.)', 'width'=>20),  
            'L' => array('data'=>'ราคาสินค้า (บ.)', 'width'=>20),
            'M' => array('data'=>'ต้นทุน (บ.)', 'width'=>20),
            'N' => array('data'=>'กำไร (บ.)', 'width'=>20),
            'O' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        );
        $fields = array(
            'A' => 'customer_fullname',
            'B' => 'tracking_code',
            'C' => 'sup_title',
            'D' => 'collect_price',
            'E' => 'order_code',
            'F' => 'vat_code',
            'G' => 'address',
            'H' => 'customer_tel',
            'I' => 'sum_qty',
            'J' => 'sum_price',
            'K' => 'vat',
            'L' => 'buy',
            'M' => 'cost',
            'N' => 'profit',
            'O' => 'created_at',            
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('K1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('K1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            $sum_order = $this->orders_m->get_sum_order_detail($row->order_id);

            $total      = 0;
            $sum_total  = 0;
            //คำนวณยอดรวม
            $info_detail = $this->orders_m->get_order_detail($row->order_id);
            foreach($info_detail->result() as $value){
                $total      = $value->product_price * $value->quantity;
                $sum_total += $total;
                $vat = (($sum_total*7)/107);
                $buy = $sum_total - $vat;
                $cost = ($value->product_price_cost * $value->quantity);
                $profit = $sum_total - $cost;
            }

            $vat_code = 'IV'.substr($row->order_code, 2,4 ).substr($row->created_at,8,2).'/'.substr($row->order_code, 6 );

            //ที่อยู่
            $into_provin = $this->provinces_m->get_ProvincesBykey($row->provinces_id);
            $provinces = $into_provin->row();

            $into_amphures = $this->provinces_m->get_AmphuresBykey($row->amphures_id);
            $amphures = $into_amphures->row();

            $into_districts = $this->provinces_m->get_DistrictsBykey($row->districts_id);
            $districts = $into_districts->row();

            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                if ( $field == 'vat_code' )
                    $value = $vat_code;
                if ( $field == 'sum_qty' )
                    $value = $sum_order->sum_qty;
                if ( $field == 'sum_price' )
                    $value = number_format($sum_total,2);
                if ( $field == 'vat' )
                    $value = number_format(($sum_total*7)/107,2);
                if ( $field == 'buy' )
                    $value = number_format($sum_total-(($sum_total*7)/107),2);
                if ( $field == 'cost' )
                    $value = number_format($cost,2);
                if ( $field == 'profit')
                    $value = ($row->order_status != 3) ? number_format($profit,2): 'ไม่มี';
                if ( $field == 'address')
                    $value = $row->customer_address." ตำบล".$districts->name_th." อำเภอ".$amphures->name_th." จังหวัด".$provinces->name_th." ".$row->zip_code;
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }

            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }

    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        
        $info = $this->reports_m->get_rows($input);
      
        $data['info'] = $info->result();

        //$data = [];
        //load the view and saved it into $html variable
        $html=$this->load->view("{$this->router->class}/pdf", $data, true);
 
        //this the the PDF filename that user will get to download
        $pdfFilePath = date('Ymdhis')."-order.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");   
    }
}
