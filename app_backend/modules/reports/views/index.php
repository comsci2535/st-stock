
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                   </h3>
               </div>
           </div>
           <div class="m-portlet__head-tools">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="form-group m-form__group row">
            <label for="created_at" class="offset-2 col-2 col-form-label">วันที่</label>
            <div class="col-4">
                <input type="text" name="dateRange" class="form-control" value="<?=$dateRang;?>" autocomplete="off"/>
                <input type="hidden" name="createStartDate"  value="<?=$createStartDate?>"/>
                <input type="hidden" name="createEndDate"  value="<?=$createEndDate?>"/>
            </div>
        </div>
        <div class="form-group m-form__group row">
            <label for="created_at" class="offset-2 col-2 col-form-label">เลือกการขนส่ง</label>
            <div class="col-4">
                <select class="select2" name="supply[]" id="supply" multiple>
                    <?php foreach ($supply as $key => $item) { ?>
                        <option value="<?=$item->supply_id?>"><?=$item->title?></option>
                    <?php }?>
                </select>
            </div>
        </div>    
        <div class="form-group m-form__group row">
            <label for="created_at" class="offset-2 col-2 col-form-label"></label>
            <div class="col-4">
                <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
            </div>
        </div>
        <form  role="form">
            <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                <thead>
                    <tr>
                        <!-- <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th> -->
                        <th>ชื่อ-นามสกุล</th>
                        <th>เลขพัสดุ</th>
                        <th>ขนส่ง</th>
                        <th>OrderID</th>
                        <th>เลขที่ใบกำกับภาษี</th>
                        <th>เบอร์โทร</th>
                        <th>จำนวนขายทั้งหมด (หน่วย)</th>
                        <th>จำนวนเงิน (บ.)</th>
                        <th>ภาษีมูลค่าเพิ่ม (บ.)</th>
                        <th>ราคาสินค้า (บ.)</th>
                        <th>ต้นทุน (บ.)</th>
                        <th>กำไร (บ.)</th>
                        <th>เมื่อวันที่</th>
                        <th>สถานะ</th>
                        <!-- <th></th> -->
                    </tr>
                </thead>
            </table>
        </form>

    </div>
</div>
</div>