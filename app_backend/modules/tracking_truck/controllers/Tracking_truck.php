<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Tracking_truck extends MX_Controller {

    private $_title = "Tracking Truck";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ Tracking Truck";
    private $_grpContent = "tracking_truck";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->status=array('รับออเดอร์','แพ็คสินค้า','ส่งแล้ว','ยกเลิก');
        $this->status_class=array('info','primary','success','danger');
        $this->load->library('m_pdf');
        $this->load->library('ckeditor');
        $this->load->model("orders/orders_m");
        $this->load->model("products/products_m");
        $this->load->model("provinces/provinces_m");
        $this->load->model("info/info_m");
        $this->load->model("catalogs/catalogs_m");
    }
    
    public function index(){
        $this->load->module('template');
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        $data['status'] =  $this->status;
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['tracking_code'] = 'Null';
        $info = $this->orders_m->get_rows($input);
        $infoCount = $this->orders_m->get_count($input);
        $column = array();
        $i = $input['start'] + 1;
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->order_id);
            $sum_order = $this->orders_m->get_sum_order_detail($rs->order_id);

            $total      = 0;
            $sum_total  = 0;
            //คำนวณยอดรวม
            $info_detail = $this->orders_m->get_detail($rs->order_id);
            foreach($info_detail->result() as $value){
                $total      = $value->product_price * $value->quantity;
                $sum_total += $total;
            }

            $status_print = '';

            if(!empty($rs->tracking_code)):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> เลขพัสดุ</span>';
            endif;
            
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['no'] = $i;
            $column[$key]['order_code'] = $rs->order_code.'<br><small>'.$status_print.'</small>';
            $column[$key]['fullname'] = $rs->customer_fullname;
            $column[$key]['tel'] = $rs->customer_tel;
            $column[$key]['total_list'] = isset($sum_order->sum_qty)? $sum_order->sum_qty : 0;
            $column[$key]['total'] = number_format($sum_total,2);
            $column[$key]['vat'] = number_format(($sum_total*0.07),2);
            $column[$key]['buy'] = number_format($sum_total - ($sum_total*0.07),2);

            $column[$key]['input'] = '<div class="input-group" id="text-'.$rs->order_id.'">
                                        <input type="text" class="form-control text-tracking" placeholder="ระบุ tracking">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary btn-add-tracking" data-id="'.$id.'" data-row-id="text-'.$rs->order_id.'"><i class="fa fa-save" aria-hidden="true"></i></button>
                                        </div>
                                    </div>';
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['active'] = '<span class="m-badge m-badge--'.$this->status_class[$rs->order_status].' m-badge--wide">'.$this->status[$rs->order_status].'</span>';
            $i++;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            
            if ( $type == "tracking" ) {
                $value['tracking_code'] = $input['tracking_code'];
                $result = $this->orders_m->update(decode_id($input['id']), $value);
            } 
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
}
