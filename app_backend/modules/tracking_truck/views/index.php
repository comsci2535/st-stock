
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group mr-2" role="group" aria-label="1 group">
                   
                </div>
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label">วันที่</label>
                <div class="col-4">
                    <input type="text" name="dateRange" class="form-control" value="<?=$dateRang;?>" autocomplete="off"/>
                    <input type="hidden" name="createStartDate"  value="<?=$date?>"/>
                    <input type="hidden" name="createEndDate"  value="<?=$date?>"/>

                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-1 col-form-label">สถานะ</label>
                <div class="col-4">
                    <select name="order_status" id="order_status" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        if(isset($status) && count($status)):
                            foreach($status as $key => $item):
                            echo '<option value="'.$key.'">'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label"></label>
                <div class="col-4">
                    <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                    <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
                </div>
            </div>
            <br>
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>OrderID</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทร</th>
                            <th>จำนวนรายการ (หน่วย)</th>
                            <th>จำนวนเงิน</th>
                            <th>แก้ไข</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>