<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Vat extends MX_Controller {

    private $_title = "ใบกำกับภาษีแบบย่อ";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ ___ใบกำกับภาษีแบบย่อ";
    private $_grpContent = "vat";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        
        $this->status=array('รับออเดอร์','แพ็คสินค้า','ส่งแล้ว','ยกเลิก');
        $this->load->library('ckeditor');
        $this->load->model("orders/orders_m");
        $this->load->model("products/products_m");
        $this->load->model("cart/cart_m");
        $this->load->model("provinces/provinces_m");
        $this->load->model("info/info_m");
        $this->load->model("catalogs/catalogs_m");
    }
    
    public function index(){
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf' => site_url("{$this->router->class}/pdf"),
        );

        $action[0][] = action_printvat(site_url("{$this->router->class}"));

        $action[1][] = action_refresh(site_url("{$this->router->class}"));

        $data['boxAction'] = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));

        $data['dateRang']           = date('01/m/Y').' ถึง '.date('d/m/Y');
        $data['createStartDate']    = date('Y-m-01');
        $data['createEndDate']      = date('Y-m-d');

        // status

        $data['status'] =  $this->status;
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }   

    public function data_index(){
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->orders_m->get_rows($input);
        $infoCount = $this->orders_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $arr_url = array();
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->order_id);
            $sum_order = $this->orders_m->get_sum_order_detail($rs->order_id);

            $action = array();
            $action[1][] = table_edit(site_url("{$this->router->class}/edit/{$id}"));


            $action[1][] = table_action($arr_url);
            unset($arr_url);

            switch ($rs->order_status) {
                case 0 : $style = 'btn-default'; break; //กำลังดำเนินการ
                case 1 : $style = 'btn-warning'; break; //จัดเตรียม
                case 2 : $style = 'btn-success'; break; //ส่งแล้ว
                case 3 : $style = 'btn-danger disabled'; break;  //ยกเลิก
                default: $style = 'btn-default'; break;
            }
            $statusMethod = site_url("orders/action/status");
            $statusPicker =  $this->status[$rs->order_status];

            $total      = 0;
            $sum_total  = 0;
            //คำนวณยอดรวม
            $info_detail = $this->orders_m->get_detail($rs->order_id);
            foreach($info_detail->result() as $value){
                $total      = $value->product_price * $value->quantity;
                $sum_total += $total;
            }

            $active = $rs->active ? "checked" : null;
            $status_print = '';
            
            if(!empty($rs->vat_print)):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบกำกับภาษีอย่างย่อ</span><br>';
            endif;

            
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['order_code'] = $rs->order_code.'<br><small>'.$status_print.'</small>';
            $column[$key]['fullname'] = $rs->customer_fullname;
            $column[$key]['tel'] = $rs->customer_tel;
            $column[$key]['total_list'] = isset($sum_order->sum_qty)? $sum_order->sum_qty : 0;
            $column[$key]['total'] = number_format($sum_total,2);
            $column[$key]['tracking'] = $rs->tracking_code;
            $column[$key]['vat'] = number_format((($sum_total*7)/107),2);
            $column[$key]['buy'] = number_format($sum_total - (($sum_total*7)/107),2);
            $column[$key]['updated_at'] = datetime_table($rs->created_at);
            $column[$key]['active'] = $statusPicker;
            //$column[$key]['action'] = Modules::run('utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
    public function create(){
        $this->load->module('template');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function save(){
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $result = $this->vat_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
       } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
       }
       redirect(site_url("{$this->router->class}"));
   }

   public function edit($id=""){
    $this->load->module('template');

    $id = decode_id($id);
    $input['repoId'] = $id;
    $info = $this->vat_m->get_rows($input);
    if ( $info->num_rows() == 0) {
        Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
        redirect_back();
    }
    $info = $info->row();
    $data['info'] = $info;
    $data['grpContent'] = $this->_grpContent;
    $data['frmAction'] = site_url("{$this->router->class}/update");

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/form";

    $this->template->layout($data);
}

public function update(){
    $input = $this->input->post(null, true);
    $id = decode_id($input['id']);
    $value = $this->_build_data($input);
    $result = $this->vat_m->update($id, $value);
    if ( $result ) {
       Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
   } else {
       Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
   }
   redirect(site_url("{$this->router->class}"));
}

private function _build_data($input){

    $value['title'] = $input['title'];
    $value['excerpt'] = $input['excerpt'];
    $value['detail'] = $input['detail'];
    if ( $input['mode'] == 'create' ) {
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $this->session->users['user_id'];
    } else {
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
    }
    return $value;
}

public function excel(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }
    $info = $this->vat_m->get_rows($input);
    $fileName = "vat";
    $sheetName = "Sheet name";
    $sheetTitle = "Sheet title";

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $styleH = excel_header();
    $styleB = excel_body();
    $colums = array(
        'A' => array('data'=>'รายการ', 'width'=>50),
        'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
        'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
        'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
    );
    $fields = array(
        'A' => 'title',
        'B' => 'excerpt',
        'C' => 'created_at',            
        'D' => 'updated_at',
    );
    $sheet->setTitle($sheetName);

        //title
    $sheet->setCellValue('A1', $sheetTitle);
    $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
    $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
    $sheet->getRowDimension(1)->setRowHeight(20);

        //header
    $rowCount = 2;
    foreach ( $colums as $colum => $data ) {
        $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
        $sheet->SetCellValue($colum.$rowCount, $data['data']);
        $sheet->getColumnDimension($colum)->setWidth($data['width']);
    }

        // data
    $rowCount++;
    foreach ( $info->result() as $row ){
        foreach ( $fields as $key => $field ){
            $value = $row->{$field};
            if ( $field == 'created_at' || $field == 'updated_at' )
                $value = datetime_table ($value);
            $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
            $sheet->SetCellValue($key . $rowCount, $value); 
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);
        $rowCount++;
    }
    $sheet->getRowDimension($rowCount)->setRowHeight(20);  
    $writer = new Xlsx($spreadsheet);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
    $writer->save('php://output');
    exit; 
}

public function pdf(){
    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $input['recycle'] = 0;
    $input['grpContent'] = $this->_grpContent;
    if ( $this->session->condition[$this->_grpContent] ) {
        $input = $this->session->condition[$this->_grpContent];
        unset($input['length']);
    }

    $info = $this->vatrts_m->get_rows($input);

    $data['info'] = $info->result();

        //$data = [];
        //load the view and saved it into $html variable
    $html=$this->load->view("{$this->router->class}/pdf", $data, true);

        //this the the PDF filename that user will get to download
    $pdfFilePath = date('Ymdhis')."-order.pdf";

        //load mPDF library
    $this->load->library('m_pdf');

       //generate the PDF from the given html
    $this->m_pdf->pdf->WriteHTML($html);

        //download it.
    $this->m_pdf->pdf->Output($pdfFilePath, "D");   
}

public function trash(){
    $this->load->module('template');

        // toobar
    $action[1][] = action_list_view(site_url("{$this->router->class}"));
    $action[2][] = action_delete_multi(base_url("{$this->router->class}/action/delete"));
    $data['boxAction'] = Modules::run('utils/build_toolbar', $action);

        // breadcrumb
    $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
    $data['breadcrumb'][] = array("ถังขยะ", site_url("{$this->router->class}/trash"));

        // page detail
    $data['pageHeader'] = $this->_title;
    $data['pageExcerpt'] = $this->_pageExcerpt;
    $data['contentView'] = "{$this->router->class}/trash";

    $this->template->layout($data);
}

public function data_trash(){
    $input = $this->input->post();
    $input['recycle'] = 1;
    $info = $this->vat_m->get_rows($input);
    $infoCount = $this->vat_m->get_count($input);
    $column = array();
    foreach ($info->result() as $key => $rs) {
        $id = encode_id($rs->repoId);
        $action = array();
        $action[1][] = table_restore("{$this->router->class}/action/restore");         
        $active = $rs->active ? "checked" : null;
        $column[$key]['DT_RowId'] = $id;
        $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
        $column[$key]['title'] = $rs->title;
        $column[$key]['excerpt'] = $rs->excerpt;
        $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
        $column[$key]['action'] = Modules::run('utils/build_toolbar', $action);
    }
    $data['data'] = $column;
    $data['recordsTotal'] = $info->num_rows();
    $data['recordsFiltered'] = $infoCount;
    $data['draw'] = $input['draw'];
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
}    

public function action($type=""){
    if ( !$this->_permission ) {
        $toastr['type'] = 'error';
        $toastr['lineOne'] = config_item('appName');
        $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success'] = false;
        $data['toastr'] = $toastr;
    } else {
        $input = $this->input->post();
        foreach ( $input['id'] as &$rs ) 
            $rs = decode_id($rs);
        $dateTime = db_datetime_now();
        $value['updated_at'] = $dateTime;
        $value['updated_by'] = $this->session->users['user_id'];
        $result = false;
        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->vat_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $value['recycle_at'] = $dateTime;
            $value['recycle_by'] = $this->session->users['user_id'];
            $result = $this->vat_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->vat_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->vat_m->update_in($input['id'], $value);
        }   
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
    }
    $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));        
}  

public function sumernote_img_upload(){
		//sumernote/img-upload
  $path = 'content';
  $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

  if(isset($upload['index'])){
   $picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
}

echo $picture;

}

public function deletefile($ids){

  $arrayName = array('file' => $ids);

  echo json_encode($arrayName);
}

public function updateprintinvoice(){
    //update print_invoice 
    $input = $this->input->post();
    $value['vat_print']     = $input['vat_id'];
    $this->orders_m->update_vat($input['order_id'], $value);
}

public function printvat(){

    $this->load->module('template');

    $input = $this->input->post();
    $arrayId = $input['arr_code'];

    $column = array();

    if(sizeof($arrayId) === 0){
        redirect($uri = '/vat', $method = 'auto', $code = NULL);
    }

    $page = ceil(sizeof($arrayId)/4);
    $i = 0;

    foreach($arrayId as $key => $item_id){
      /*if($key == (sizeof($arrayId)/$page)+1){
         $i++;
      }*/
    

    $input['order_id'] = decode_id($item_id);

    $info = $this->orders_m->get_rows($input)->row();

    $info_detail = $this->orders_m->get_order_detail($info->order_id);
    $detail_arr = array();
    foreach($info_detail->result() as $item){
        array_push($detail_arr, array(
            'product_color' => $item->product_color,
            'quantity' => $item->quantity,
            'title' => $item->title,
            'product_price' => $item->product_price,
        ));
    }

            //ดึงจังหวัด ของลูกค้า
    $into_provin = $this->provinces_m->get_ProvincesBykey($info->provinces_id);
    $provinces   = $into_provin->row();

    $into_amphures = $this->provinces_m->get_AmphuresBykey($info->amphures_id);
    $amphures      = $into_amphures->row();

    $into_districts = $this->provinces_m->get_DistrictsBykey($info->districts_id);
    $districts      = $into_districts->row();

    $column[$key] = array(
        'order_code'        => $info->order_code,
        'customer_fullname' => $info->customer_fullname,
        'customer_tel'      => $info->customer_tel,
        'customer_address'  => $info->customer_address,
        'zip_code'          => $info->zip_code,
        'provinces'         => $provinces,
        'amphures'          => $amphures,
        'districts'         => $districts,
        'order_detail'      => $detail_arr,
        'order_date'        => $info->created_at

    );



}

// print_r($column);
// exit();

$data['info'] = $column;
$data['arrayId'] = $arrayId;

        //ดึงข้อมูลบริษัท
$info_com = $this->info_m->get_rows('');
$info_com = $info_com->row();
$data['company']  =  $info_com;

        // breadcrumb
$data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
$data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
$data['pageHeader'] = $this->_title;
$data['pageExcerpt'] = $this->_pageExcerpt;
$data['contentView'] = "{$this->router->class}/printvat";
$data['pageScript'] = "scripts/backend/vat/print.js";

$this->template->layout($data);
}

}


