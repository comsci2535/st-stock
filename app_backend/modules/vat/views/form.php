<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">

            </div>
        </div>


        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post')) ?>
        <div class="m-portlet__body">

            <div class="form-group m-form__group row">
                <div class="offset-sm-2 col-sm-7">
                    <div class="card">
                        <div class="card-header btn-info">
                            <h4>Order Code : <?php echo isset($info->order_code) ? $info->order_code : "ยังไม่ได้ระบุ" ?></h4>
                        </div>
                        <div class="card-body">
                            <p>เลขพัสดุ : <?php echo isset($info->tracking_code) ? $info->tracking_code : NULL ?></p>
                            <p>วันที่สั่งซื้อ : <?php echo isset($info->created_at) ? $info->created_at : NULL ?></p>
                            <p>สถานะ : <?php echo isset($status) ? $status : NULL ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">ชื่อนามสกุล</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->customer_fullname) ? $info->customer_fullname : NULL ?>"
                    type="text" class="form-control m-input m-input--square" name="customer_fullname"
                    id="customer_fullname" >
                    <label id="customer_fullname-error-dup" class="error-dup" for="customer_fullname"
                    style="display: none;">ชื่อนามสกุล</label>
                </div>
            </div>

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">เบอร์โทร</label>
                <div class="col-sm-7">
                    <input value="<?php echo isset($info->customer_tel) ? $info->customer_tel : NULL ?>" type="text"
                    class="form-control m-input m-input--square" name="customer_tel" id="customer_tel" >
                    <label id="customer_fullname-error-dup" class="error-dup" for="customer_tel"
                    style="display: none;">เบอร์โทร</label>
                </div>
            </div>
            

            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">ที่อยู๋</label>
                <div class="col-sm-7">
                    <textarea name="customer_address" rows="3" class="form-control" id="customer_address"
                    ><?php echo isset($info->customer_address) ? $info->customer_address : NULL ?></textarea>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <input type="hidden" id="datajson" name="datajson"
                value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                <label class="col-sm-2 col-form-label" for="title">ตำบล <span class="text-danger"> *</span></label>
                <div class="col-sm-7">
                    <input type="text" class="form-control m-input" id="district" placeholder="ตำบล"
                    name="districts_name"
                    value="<?php echo isset($districts->name_th) ? $districts->name_th : NULL ?>">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">อำเภอ <span class="text-danger"> *</span></label>
                <div class="col-lg-7">
                    <input type="text" class="form-control m-input" id="amphoe" placeholder="อำเภอ" name="amphures_name"
                    value="<?php echo isset($amphures->name_th) ? $amphures->name_th : NULL ?>">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">จังหวัด <span class="text-danger"> *</span></label>
                <div class="col-lg-7">
                    <input type="text" class="form-control m-input" id="province" placeholder="จังหวัด"
                    name="provinces_name"
                    value="<?php echo isset($provinces->name_th) ? $provinces->name_th : NULL ?>">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title">รหัสไปรษณีย์ <span class="text-danger">
                *</span></label>
                <div class="col-lg-7">
                    <input type="text" class="form-control m-input" id="zipcode" placeholder="รหัสไปรษณีย์"
                    name="zip_code" value="<?php echo isset($info->zip_code) ? $info->zip_code : NULL ?>">
                </div>
            </div>

            <?php //if($info->invoice > 0 && $info->invoice_type < 1){ ?>
                <!-- ใบกำกับภาษี -->
                <div class="form-group m-form__group row">
                    <label class="col-sm-2"></label>
                    <div class="col-lg-6 m-form__group-sub">
                        <label class="m-checkbox m-checkbox--state-primary">
                            <input type="checkbox" name="invoice" id="invoice" value="1"  <?php echo $info->invoice == 1 ? 'checked' : '' ?>> ต้องการใบกำกับภาษี
                            <span></span>
                        </label>
                    </div>
                </div>
                
                <!-- end -->


                <div class="m-view">  
                    <div class="bg-light pt-4 pb-3">

                        <div class="form-group m-form__group row">
                            <label class="col-sm-2"></label>
                            <div class="col-lg-6 m-form__group-sub">
                                <label class="m-checkbox m-checkbox--state-primary">
                                    <input type="checkbox" name="dupicate" id="dupicate" value="1" <?php echo $info->invoice_type == 1 ? 'checked' : '' ?>> ใช้ที่อยู่จัดส่ง
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-7 text-center">
                                <h5>ข้อมูลใบกำกับภาษี</h5>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row">
                            <label class="col-sm-2 form-col-form-label">เลขที่ผู้เสียภาษี<span class="text-danger"> *</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="vat_code" class="form-control m-input vat-code"
                                placeholder="เลขที่ผู้เสียภาษี" value="<?php echo isset($info->invoice_no) ? $info->invoice_no : '' ?>"  >
                            </div>
                        </div>
                        <div class="m-view-vat">   
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">เบอร์โทรศัพท์<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="invoice_tel" class="form-control m-input phone-with-add"
                                    placeholder="เบอร์โทรศัพท์" value="<?php echo isset($info->invoice_tel) ? $info->invoice_tel : '' ?>" >
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">ที่อยู่<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <textarea name="customer_address_vat"  rows="3" class="form-control m-input"
                                    placeholder="ที่อยู่" ><?php echo isset($info->invoice_address) ? $info->invoice_address : '' ?></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <input type="hidden" id="datajson_vat" name="datajson_vat"
                                value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="district_vat" placeholder="ตำบล"
                                    name="districts_name_vat" value="<?php echo isset($districts_v->name_th) ? $districts_v->name_th : NULL ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="amphoe_vat" placeholder="อำเภอ"
                                    name="amphures_name_vat" value="<?php echo isset($amphures_v->name_th) ? $amphures_v->name_th : NULL ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">จังหวัด<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="province_vat" placeholder="จังหวัด"
                                    name="provinces_name_vat" value="<?php echo isset($provinces_v->name_th) ? $provinces_v->name_th : NULL ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="zipcode_vat" placeholder="รหัสไปรษณีย์"
                                    name="zip_code_vat" value="<?php echo isset($info->invoice_zip_code) ? $info->invoice_zip_code : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php //}?>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="">รายการสั่งซื้อ</label>
                    <div class="col-sm-8">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-conter" style="width:10%">ลำดับ</th>
                                    <th class="text-left" style="width:30%">ชื่อสินค้า</th>
                                    <th class="text-conter" style="width:10%">จำนวน</th>
                                    <th class="text-conter" style="width:15%">ราคาขาย</th>
                                    <th class="text-conter" style="width:15%">จำนวนเงิน</th>
                                    <th class="text-conter" style="width:15%">ภาษีมูลค่าเพิ่ม</th>
                                    <th class="text-conter" style="width:10%">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(isset($info_detail)):
                                    $total = 0;
                                    $sumtotal = 0;
                                    foreach($info_detail->result() as $key => $value): 
                                        $total = $value->quantity*$value->product_price;
                                        $sumtotal += $total;
                                        $vat = (($sumtotal*7)/107);
                                        $buy = $sumtotal - $vat;
                                        ?>

                                        <tr>
                                            <td style="text-align: center;"><?=$key+1;?></td>
                                            <td><?php echo isset($value->title) ? $value->title.' ('.$value->product_color.')' : NULL ?></td>
                                            <td style="text-align: center;">
                                                <?php echo isset($value->quantity) ? $value->quantity : NULL ?></td>
                                                <td style="text-align: right;">
                                                    <?php echo isset($value->product_price) ? number_format($value->product_price,2) : NULL ?></td>
                                                    <td style="text-align: right;">
                                                    <?php echo isset($value->product_price) ? number_format($value->product_price*$value->quantity,2) : NULL ?></td>
                                                    <td style="text-align: right;"><?php echo isset($vat) ? number_format($vat,2) : 0 ?></td>
                                                    <td style="text-align: right;"><?php echo isset($total) ? number_format($total-$vat,2) : 0 ?></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6" style="text-align: right;">รวมทั้งหมด</td>
                                            <td style="text-align: right;"><?php echo isset($sumtotal) ? number_format($buy,2) : 0 ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">

                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-2">
                                </div>
                                <div class="col-10">
                                    <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                                </div>
                            </div>

                        </div>
                    </div>
                    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                    <input type="hidden" class="form-control" name="db" id="db" value="repo">
                    <input type="hidden" name="id" id="input-id"
                    value="<?php echo isset($info->order_id) ? encode_id($info->order_id) : 0 ?>">
                    <input type="hidden" name="order_code" id="order_code"
                    value="<?php echo isset($info->order_code) ? encode_id($info->order_code) : 0 ?>">
                    <?php echo form_close() ?>

                    <!--end::Form-->
                </div>
            </div>