
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <div class="btn-group mr-2" role="group" aria-label="1 group">
                   
                </div>
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <!-- <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label">วันที่</label>
                <div class="col-4">
                    <input type="text" name="dateRange" class="form-control" value="<?=$dateRang;?>" autocomplete="off"/>
                    <input type="hidden" name="createStartDate"  value="<?=$createStartDate?>"/>
                    <input type="hidden" name="createEndDate"  value="<?=$createEndDate?>"/>

                </div>
            </div> -->
            <div class="form-group m-form__group row">
                <label for="order_year" class="offset-2 col-1 col-form-label">ปี</label>
                <div class="col-4">
                    <select name="order_year" id="order_year" class="form-control m-input select2">
                        <?php
                        for($i = 2017; $i <= date('Y'); $i++):
                            $selected = '';
                            if($i == date('Y')):
                                $selected = 'selected';
                            endif;
                            echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                        endfor;
                        // if(isset($status) && count($status)):
                        //     foreach($status as $key => $item):
                        //     echo '<option value="'.$key.'">'.$item.'</option>';
                        //     endforeach;
                        // endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_month" class="offset-2 col-1 col-form-label">เดือน</label>
                <div class="col-4">
                    <select name="order_month" id="order_month" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        $month_arr = array( "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                        if(isset($month_arr) && count($month_arr)):
                            foreach($month_arr as $key => $item):
                                $selectedm = '';
                                if(($key+1) == date('m')):
                                    $selectedm = 'selected';
                                endif;
                                
                                echo '<option value="'.($key+1).'" '.$selectedm.'>'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_day" class="offset-2 col-1 col-form-label">วัน</label>
                <div class="col-4">
                    <select name="order_day" id="order_day" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        $day_arr = array();
                        for ($i=1; $i <= 31; $i++) { 
                            array_push($day_arr, $i);
                        }
                        if(isset($day_arr) && count($day_arr)):
                            foreach($day_arr as $key => $item):
                                $selectedm = '';
                                if(($key+1) == date('d')):
                                    $selectedm = 'selected';
                                endif;
                                
                                echo '<option value="'.($key+1).'" '.$selectedm.'>'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-1 col-form-label">สถานะ</label>
                <div class="col-4">
                    <select name="order_status" id="order_status" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        if(isset($status) && count($status)):
                            foreach($status as $key => $item):
                            echo '<option value="'.$key.'">'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-1 col-form-label"></label>
                <div class="col-4">
                    <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                    <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
                </div>
            </div>
            <br>
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable responsive " width="100%">
                    <thead>
                        <tr>
                            <th>
                                <input name="tbCheckAll" type="checkbox" class="icheck tb-check-all">
                            </th>
                            <th>OrderID</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทร</th>
                            <th>จำนวนรายการ (หน่วย)</th>
                            <th>จำนวนเงิน</th>
                            <th>ภาษีมูลค่าเพิ่ม</th>
                            <th>ราคาขาย</th>
                            <th>วันที่ซื้อ</th>
                            <th>สถานะ</th>
                            <th>Tracking</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>
        </div>
    </div>
</div>
