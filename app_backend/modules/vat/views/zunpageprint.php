<page size="A4">
	<div class="page col-12">

		
		<div class="row  fill-height-or-more">
			

			<div class="col-6 box">
				<div class="row">
					<div class="col-6">
						<p class="company"><?=$company->title?></p>
					</div>
					<div class="col-6">
						<p class="no">เลขที่ใบกำกับภาษี : xxx</p>
					</div>
				</div>
				<div class="row">
					<div class="col-10">
						<span><?=$company->excerpt?></span><br>
						<span>Tel. <?=$company->tel?></span><br>
						<span>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-center mt-3">
						<h4>ใบกำกับภาษีอย่างย่อ</h4>
						<span>TAX INVOICE</span>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right mt-3">
						<span>วันที่ : <?=date('d/m/Y H:i:s')?></span>
					</div>
				</div>
				<div class="row text-center bd-tb">
					<div class="col-1">
						ลำดับ
					</div>
					<div class="col-4">
						รายการสินค้า
					</div>
					<div class="col-2">
						จำนวน
					</div>
					<div class="col-2">
						หน่วยละ
					</div>
					<div class="col-3">
						จำนวนเงิน
					</div>
				</div>

				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-right">
						2
					</div>
					<div class="col-2 text-right">
						200
					</div>
					<div class="col-3 text-right">
						400
					</div>
				</div>
				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-center">
						2
					</div>
					<div class="col-2 text-center">
						200
					</div>
					<div class="col-3 text-center">
						400
					</div>
				</div>


				<div class="row bd-tb">
					<div class="col-6 text-center bd-r py-1">
						xxxxxxx
					</div>
					<div class="col-4 text-center bd-r py-1">
						xxx
					</div>
					<div class="col-2 text-center py-1">
						400
					</div>
				</div>

				<div class="row" style="margin-top: 30px;">
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับสินค้า
					</div>
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับเงิน
					</div>
				</div>
				<div class="row">
					<div class="col-6 ">
						วันที่______________________
					</div>
					<div class="col-6 ">
						วันที่______________________
					</div>
				</div>

			</div>

			<div class="col-6 box">
				<div class="row">
					<div class="col-6">
						<p class="company"><?=$company->title?></p>
					</div>
					<div class="col-6">
						<p class="no">เลขที่ใบกำกับภาษี : xxx</p>
					</div>
				</div>
				<div class="row">
					<div class="col-10">
						<span><?=$company->excerpt?></span><br>
						<span>Tel. <?=$company->tel?></span><br>
						<span>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-center mt-3">
						<h4>ใบกำกับภาษีอย่างย่อ</h4>
						<span>TAX INVOICE</span>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right mt-3">
						<span>วันที่ : <?=date('d/m/Y H:i:s')?></span>
					</div>
				</div>
				<div class="row text-center bd-tb">
					<div class="col-1">
						ลำดับ
					</div>
					<div class="col-4">
						รายการสินค้า
					</div>
					<div class="col-2">
						จำนวน
					</div>
					<div class="col-2">
						หน่วยละ
					</div>
					<div class="col-3">
						จำนวนเงิน
					</div>
				</div>

				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-right">
						2
					</div>
					<div class="col-2 text-right">
						200
					</div>
					<div class="col-3 text-right">
						400
					</div>
				</div>
				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-center">
						2
					</div>
					<div class="col-2 text-center">
						200
					</div>
					<div class="col-3 text-center">
						400
					</div>
				</div>


				<div class="row bd-tb">
					<div class="col-6 text-center bd-r py-1">
						xxxxxxx
					</div>
					<div class="col-4 text-center bd-r py-1">
						xxx
					</div>
					<div class="col-2 text-center py-1">
						400
					</div>
				</div>

				<div class="row" style="margin-top: 30px;">
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับสินค้า
					</div>
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับเงิน
					</div>
				</div>
				<div class="row">
					<div class="col-6 ">
						วันที่______________________
					</div>
					<div class="col-6 ">
						วันที่______________________
					</div>
				</div>

			</div>

		</div>

		<!-- ล่าง -->

		<div class="row  fill-height-or-more">
			

			<div class="col-6 box">
				<div class="row">
					<div class="col-6">
						<p class="company"><?=$company->title?></p>
					</div>
					<div class="col-6">
						<p class="no">เลขที่ใบกำกับภาษี : xxx</p>
					</div>
				</div>
				<div class="row">
					<div class="col-10">
						<span><?=$company->excerpt?></span><br>
						<span>Tel. <?=$company->tel?></span><br>
						<span>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-center mt-3">
						<h4>ใบกำกับภาษีอย่างย่อ</h4>
						<span>TAX INVOICE</span>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right mt-3">
						<span>วันที่ : <?=date('d/m/Y H:i:s')?></span>
					</div>
				</div>
				<div class="row text-center bd-tb">
					<div class="col-1">
						ลำดับ
					</div>
					<div class="col-4">
						รายการสินค้า
					</div>
					<div class="col-2">
						จำนวน
					</div>
					<div class="col-2">
						หน่วยละ
					</div>
					<div class="col-3">
						จำนวนเงิน
					</div>
				</div>

				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-right">
						2
					</div>
					<div class="col-2 text-right">
						200
					</div>
					<div class="col-3 text-right">
						400
					</div>
				</div>
				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-center">
						2
					</div>
					<div class="col-2 text-center">
						200
					</div>
					<div class="col-3 text-center">
						400
					</div>
				</div>


				<div class="row bd-tb">
					<div class="col-6 text-center bd-r py-1">
						xxxxxxx
					</div>
					<div class="col-4 text-center bd-r py-1">
						xxx
					</div>
					<div class="col-2 text-center py-1">
						400
					</div>
				</div>

				<div class="row" style="margin-top: 30px;">
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับสินค้า
					</div>
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับเงิน
					</div>
				</div>
				<div class="row">
					<div class="col-6 ">
						วันที่______________________
					</div>
					<div class="col-6 ">
						วันที่______________________
					</div>
				</div>

			</div>

			<div class="col-6 box">
				<div class="row">
					<div class="col-6">
						<p class="company"><?=$company->title?></p>
					</div>
					<div class="col-6">
						<p class="no">เลขที่ใบกำกับภาษี : xxx</p>
					</div>
				</div>
				<div class="row">
					<div class="col-10">
						<span><?=$company->excerpt?></span><br>
						<span>Tel. <?=$company->tel?></span><br>
						<span>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></span> 
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-center mt-3">
						<h4>ใบกำกับภาษีอย่างย่อ</h4>
						<span>TAX INVOICE</span>
					</div>
				</div>
				<div class="row">
					<div class="col-12 text-right mt-3">
						<span>วันที่ : <?=date('d/m/Y H:i:s')?></span>
					</div>
				</div>
				<div class="row text-center bd-tb">
					<div class="col-1">
						ลำดับ
					</div>
					<div class="col-4">
						รายการสินค้า
					</div>
					<div class="col-2">
						จำนวน
					</div>
					<div class="col-2">
						หน่วยละ
					</div>
					<div class="col-3">
						จำนวนเงิน
					</div>
				</div>

				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-right">
						2
					</div>
					<div class="col-2 text-right">
						200
					</div>
					<div class="col-3 text-right">
						400
					</div>
				</div>
				<div class="row bd-b">
					<div class="col-1 text-center ">
						1
					</div>
					<div class="col-4 text-left ">
						xxxx
					</div>
					<div class="col-2 text-center">
						2
					</div>
					<div class="col-2 text-center">
						200
					</div>
					<div class="col-3 text-center">
						400
					</div>
				</div>


				<div class="row bd-tb">
					<div class="col-6 text-center bd-r py-1">
						xxxxxxx
					</div>
					<div class="col-4 text-center bd-r py-1">
						xxx
					</div>
					<div class="col-2 text-center py-1">
						400
					</div>
				</div>

				<div class="row" style="margin-top: 30px;">
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับสินค้า
					</div>
					<div class="col-6 ">
						ลงชื่อ________________ผู้รับเงิน
					</div>
				</div>
				<div class="row">
					<div class="col-6 ">
						วันที่______________________
					</div>
					<div class="col-6 ">
						วันที่______________________
					</div>
				</div>

			</div>

			
		</div>
		


	</div>	
</page>
