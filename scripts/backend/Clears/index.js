"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        searching: false,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: false},
            {data: "total", className: "text-center", orderable: false},
            {data: "created_at", width: "100px", className: "", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
