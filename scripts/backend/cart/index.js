"use strict";
$(document).ready(function () {
    
     dataList.DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[6, "DESC"]],
        pageLength: 1000,
        columns: [
        //{data: "checkbox", width: "20px", className: "text-center", orderable: false},
        {data: "img", className: "text-center", orderable: false},
        {data: "title", className: "", orderable: false},
        {data: "brand", className: "", orderable: false},
        {data: "price_cost", width: "100px", className: "text-right", orderable: false},
        {data: "quantity", width: "100px", className: "text-center", orderable: false},
        {data: "input", width: "130px", className: "text-center", orderable: false},
        {data: "customer_name", width: "130px", className: "text-center", orderable: false},
        {data: "sell_name", width: "130px", className: "text-center", orderable: false},
        {data: "updated_at", width: "120px", className: "", orderable: true},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


$(document).on('click', '.btn-add-cart', function () {

    var qty = 0;
    var id       = $(this).attr('data-id');
    var product_id       = $(this).attr('data-product-id');
    var row      = $(this).attr('data-row-id');
    var cart = $('#'+row).find('.text-cart').val();
    var max = $('#'+row).find('.text-cart').attr('max');

    if(cart == ''){
        toastr["error"]("กรุณาจำนวนที่คืน","");
        $('#'+row).find('.text-cart').focus();
        return false;
    }

    max = max.replace(",", "");
    cart = cart.replace(",", "");

    if(parseInt(cart) > parseInt(max)){
      toastr["error"]("คุณกรอกสินค้าเกินจำนวนที่มีอยู่", "");
      $('#'+row).find('.text-cart').focus();
      return false;  
    }
     if(parseInt(cart) < 0){
      toastr["error"]("คุณกรอกสินค้าจำนวนไม่ถูกต้อง", "");
      $('#'+row).find('.text-cart').focus();
      return false; 
    }
    qty = parseInt(max)-parseInt(cart); 

    $('#span-'+row).text(qty)

  $.ajax({
    url: controller+"/action/uncart",
    type: 'POST',
    data: {
        id : id,
        product_id : product_id,
        balance : qty,
        cart_qty : cart,
        mode : 'cart',
        csrfToken:get_cookie('csrfCookie')
    },
    success: function(result){
        if (result.success === true) {
            toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
            $('#'+row).find('.btn-add-cart').removeClass('btn-danger').addClass('btn-dark');
            $('#'+row).find('.btn-add-cart').attr('disabled', 'true');
        }

    }
}).fail(function () {
    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
});

});

//
$(document).on('click', '.btn-add-draw', function () {

    var id       = $(this).attr('data-draw-id');
    var row      = $(this).attr('data-draw-row-id');
    var draw = $('#'+row).find('.text-draw').val();
     
    if(draw <= 0){
        toastr["error"]("กรุณากรอกจำนวนที่จะเบิก","");
        $('#'+row).find('.text-draw').focus();
        return false;
    }

    var nummax = $('#span-text-'+id).text();
    if(parseInt(draw) > parseInt(nummax)){
      toastr["error"]("คุณกรอกสินค้าเกินจำนวนที่มีอยู่", "");
      $('#'+row).find('.text-cart').focus();
      return false;  
    }

  
    $.ajax({
        url: controller+"/action/draw",
        type: 'POST',
        data: {
            id : id,
            draw : draw,
            mode : 'draw',
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            if (result.success === true) {
                toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                $('#'+row).find('.btn-add-cart').removeClass('btn-danger').addClass('btn-dark');
                $('#'+row).find('.btn-add-cart').attr('disabled', 'true');
            }

        }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

});