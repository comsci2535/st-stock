"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            managearticle_id: {
                required:true
            },
            excerpt: {
                required:true
            },
            file: {
                required:true
            }
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    //zun
    $.Thailand({
        database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#district'), // input ของตำบล
        $amphoe: $('#amphoe'), // input ของอำเภอ
        $province: $('#province'), // input ของจังหวัด
        $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
            console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
