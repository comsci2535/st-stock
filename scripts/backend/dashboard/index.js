"use strict";
$(document).ready(function () {

    load_datatable(); // load datable
    
   

})

function load_datatable(){
   
    var dataList = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        processing: false,
        ajax: {
            url: controller+"/data_orders",
            type: 'POST',
            data: {
                csrfToken    : get_cookie('csrfCookie')
            },
        },
        order: [[1, "desc"]],
        pageLength: 25,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "order_code",width: "150px", className: "", orderable: false},
            {data: "fullname", className: "", orderable: false},
            {data: "tel", className: "", orderable: false},
            {data: "total_list", className: "text-center", orderable: false},
            {data: "total", className: "text-center", orderable: false},
            {data: "vat", width: "5%", className: "text-center", orderable: false},
            {data: "buy", width: "5%", className: "text-center", orderable: true},
            {data: "active", width: "150px", className: "text-center", orderable: false}
            
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
            var total1 = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var total2 = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var pageTotal = api
                .column( 5 , { page: 'current'})
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                 
            $( api.column( 0 ).footer() ).html('รวม : ');
            $( api.column( 4 ).footer() ).html(numberWithCommas(total1)+' บาท');
            $( api.column( 5 ).footer() ).html(numberWithCommas(total2)+' หน่วย');
           
         }
       
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
            $('.statusPicker').selectpicker({width: '100%'});
        }
    })
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
        if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }
 
        return a + b;
    }, 0 );
});



