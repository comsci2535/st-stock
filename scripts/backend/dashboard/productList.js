"use strict";
$(document).ready(function () {
    dataList.DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_product",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 100,
        columns: [
        {data: "checkbox", width: "20px", className: "text-center", orderable: false},
        {data: "img", className: "", orderable: true},
        {data: "title", className: "", orderable: true},
        {data: "brand", className: "", orderable: true},
        {data: "price_cost", width: "100px", className: "", orderable: true},
        {data: "price_sell", width: "100px", className: "", orderable: true},
        {data: "quantity", width: "100px", className: "", orderable: true},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });


    $("#provinces").select2({
        width:"100%",
        dropdownParent: $("#session-buy")
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

/**เมื่อกดสั่งซื้อ*/
var arrayId = [];
$(document).on('click', '#btn-buy', function () {
    var set = $('#data-list .tb-check-single')
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'))
        }
    });
    
    if (arrayId.length > 0) {
        $.ajax({
            url: controller+"/data_productid",
            type: 'POST',
            data: {
                arrayId : arrayId,
                csrfToken:get_cookie('csrfCookie')
            },
            success: function(result){
                var html ='';
                if(result.data.length > 0){
                    var n = 0;
                    $(result.data).each(function(key, val){
                        html +='<tr id="tr-'+n+'">';
                        html +='<td scope="row"><button class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air btn-delete" data-tr="'+n+'" ><i class="la la-trash"></i></button></td>';
                        html +='<td>'+val.title+'</td>';
                        html +='<td class="text-center">'+val.product_price_sell+'</td>';
                        html +='<td class="text-center product_quantity">'+val.product_quantity+'</td>';
                        html +='<td width="25%">';

                        html +='<div class="input-group">';
                        html +='<span class="input-group-btn">';
                        html +='<button type="button" class="btn btn-success m-btn--square btn-number"  data-type="minus" data-field="quantity['+n+']" pro-id="'+val.DT_RowId+'" data-tr-id="tr-'+n+'">';
                        html +='<span class="glyphicon glyphicon-minus"></span>';
                        html +='</button>';
                        html +='</span>';
                        html +='<input type="text" id="quantity-'+n+'" name="quantity['+n+']" class="form-control input-number text-center" oldValue="0" value="0" min="0" max="'+val.product_quantity+'" autocomplete="off" readonly>';
                        html +='<span class="input-group-btn">';
                        html +='<button type="button" class="btn btn-success m-btn--square btn-number" data-type="plus" data-field="quantity['+n+']" pro-id="'+val.DT_RowId+'" data-tr-id="tr-'+n+'">';
                        html +='<span class="glyphicon glyphicon-plus"></span>';
                        html +='</button>';
                        html +='</span>';
                        html +='</div>';
                        html +='<input id="productId-'+n+'" type="hidden" value="'+val.DT_RowId+'" name="product_id[]">';
                        html +='<input type="hidden" value="'+val.product_price_sell+'" name="product_price_sell[]">';
                        html +='</td>';
                        html +='</tr>';
                        n++;
                    });
                }

                $('#table-product tbody').html(html);
                
                $('#session-buy').modal({backdrop: 'static', keyboard: false})  
            },
            error: function (result) {
                console.log('An error occurred.');
            },
        }); 
        arrayId = [];
    } else {
        //alert('กรุณาเลือกรายการที่ต้องการลบ')
        alert_box('กรุณาเลือกรายการสินค้าที่ต้องการ');
    }
})


$(document).on('click', '.btn-number', function(){
    var fieldName = $(this).attr('data-field');
    var type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
                load_product($(this).attr('pro-id'),0, $(this).attr('data-tr-id'));
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
                load_product($(this).attr('pro-id'), 1, $(this).attr('data-tr-id'));
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }
        }
    } else {
        input.val(0);
    }
});

$(document).on('focusin', '.input-number', function(){
   $(this).data('oldValue', $(this).val());
});

$(document).on('change', '.input-number', function(){

    var minValue =  parseInt($(this).attr('min'));
    var maxValue =  parseInt($(this).attr('max'));
    var valueCurrent = parseInt($(this).val());
    
    var name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        $(this).val($(this).data('oldValue'));
    }
});

/*$(document).on('keypress',function(e) {
    if(e.which == 13 || e.which == 32) {
        return true;
    }
});*/



/**เมื่อกดเพิ่ม-ลบ จำนวนของสินค้า */
function load_product(id, type, tr){
    $.ajax({
        url: controller+"/data_productnum",
        type: 'POST',
        data: {
            pro_id : id,
            type : type,
            mode : 'create',
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            $('#table-product tr#'+tr).find('.product_quantity').html(result.qty);
        }
    });

    
}

/**เมื่อกดลบสินค้า*/
$(document).on('click', '.btn-delete', function() {
    var data_tr     = $(this).attr('data-tr');
    var quantity    = '';
    var productId   = '';

    quantity  = $('input#quantity-'+data_tr).val();
    productId = $('input#productId-'+data_tr).val();
    $('table#table-product tbody tr#tr-'+data_tr).remove();

    $.ajax({
        url: controller+"/delete_product_list",
        type: 'POST',
        data: {
            pro_id   : productId,
            quantity : quantity,
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            console.log(result);
        }
    });
});

/**เมื่อกดลบสินค้า*/
$(document).on('click', '#btn-order-delete-product', function() {

    $.ajax({
        url: controller+"/delete_product_order_list",
        type: 'POST',
        data: {
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            if(result.data){
                dataList.DataTable().draw();
            }
        }
    });
});

/**เมื่อกดเลือกจังหวัด*/
$(document).on('change', '#provinces', function() {
    var provinces_id = $("#provinces option:selected").val();
    var html = '';

    $.ajax({
        url: "provinces/viewAmphures",
        type: 'POST',
        data: {
            provinces_id   : provinces_id,
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            if(result.amphures.length > 0){
                html +='<option value="">เลือก</option>';
                $(result.amphures).each(function(key, val){
                    html += '<option value="'+val.id+'">'+val.name_th+'</option>';
                });
                $('select#amphures').html(html);
            }
        }
    });
});

/**เมื่อกดเลือกอำเภอ*/
$(document).on('change', '#amphures', function() {
    var districts_id = $("#amphures option:selected").val();
    var html = '';

    $.ajax({
        url: "provinces/viewDistricts",
        type: 'POST',
        data: {
            districts_id   : districts_id,
            csrfToken:get_cookie('csrfCookie')
        },
        success: function(result){
            if(result.districts.length > 0){
                html +='<option value="">เลือก</option>';
                $(result.districts).each(function(key, val){
                    html += '<option value="'+val.id+'">'+val.name_th+'</option>';
                });
                $('select#districts').html(html);
            }
        }
    });
});

//zun
$.Thailand({
    database: $('#datajson').val(),
    autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
    $district: $('#district'), // input ของตำบล
    $amphoe: $('#amphoe'), // input ของอำเภอ
    $province: $('#province'), // input ของจังหวัด
    $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
    onComplete: function(data) {
       console.log('Autocomplete is ready!')
   },
   onDataFill: function(data){
    console.log(data);
}
});

$.Thailand({
    database: $('#datajson').val(),
    autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
    $district: $('#district_vat'), // input ของตำบล
    $amphoe: $('#amphoe_vat'), // input ของอำเภอ
    $province: $('#province_vat'), // input ของจังหวัด
    $zipcode: $('#zipcode_vat'), // input ของรหัสไปรษณีย์
    onComplete: function(data) {
       console.log('Autocomplete is ready!')
   },
   onDataFill: function(data){
    console.log(data);
}
});

$(".phone-with-add").inputmask({
    "placeholder": "",
    "mask" : "999 999 9999",
});

$(".vat-code").inputmask({
    "placeholder": "",
    "mask" : "9999999999999",
});




$('#zipcode').inputmask({
    "placeholder": "",
    "mask" : "99999",
});
var i = 0;
$('.m-view').hide()
$('#invoice').click(function(event) {
  if($(this).is(':checked')){
      $('.m-view').show()  
  }else{
   $('.m-view').hide()  
  }
});

$('#dupicate').click(function(event) {
   if($(this).is(':checked')){
      $('.m-view-vat').hide()  
  }else{
   $('.m-view-vat').show()  
  }
});