"use strict";
$(document).ready(function () {

    dataList.DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 1000,
        columns: [
        {data: "checkbox", width: "20px", className: "text-center", orderable: false},
        {data: "order_code", className: "", orderable: true},
        {data: "draw_date", width: "50px", className: "text-center", orderable: false},
        //{data: "action", width: "30px", className: "text-center", orderable: false},


        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

$(document).on('click', '.drowdetail-multi', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint-check" action="'+baseUrlfull+''+controller+'/drowdetailhistory" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint-check').submit();
    } else {
        alert_box('กรุณาเลือกรายการที่จะดูรายละเอียด');
    }
})


