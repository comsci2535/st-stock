"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        // language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[5, "asc"]],
        pageLength: 200,
        columns: [
            // {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "img", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "product_quantity", className: "text-center", orderable: true},
            {data: "product_price_cost", className: "text-center", orderable: true},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "updated_at", width: "100px", className: "", orderable: true},
            //{data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
