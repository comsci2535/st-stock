"use strict";
$(document).ready(function () {

    load_datatable();
    // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { 
            // cancelLabel: 'ยกเลิก',
            //  applyLabel:'ตกลง', 
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
               ],
               "monthNames": [
               "มกราคม",
               "กุมภาพันธ์",
               "มีนาคม",
               "เมษายน",
               "พฤษภาคม",
               "มิถุนายน",
               "กรกฎาคม",
               "สิงหาคม",
               "กันยายน",
               "ตุลาคม",
               "พฤศจิกายน",
               "ธันวาคม"
              ],
        },
        autoApply : true,
        autoUpdateInput: false,
        showDropdowns: true,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' ถึง ' + picker.endDate.format('DD/MM/YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
        load_datatable();
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    });
    
})

function load_datatable(){
    var createDateRange = $('input[name="dateRange"]').val();
    var createStartDate = $('input[name="createStartDate"]').val();
    var createEndDate   = $('input[name="createEndDate"]').val();
    var id              = $('#text-id').val();
    var dataList = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        // language: {url: "scripts/backend/Thai.json"},
        searching: false,
        serverSide: true,
        processing: true,
        "scrollX": true,
        ajax: {
            url: controller+"/data_view",
            type: 'POST',
            data: {
                csrfToken        : get_cookie('csrfCookie'),
                createStartDate  : createStartDate,
                createEndDate    : createEndDate,
                createDateRange  : createDateRange,
                id: id
            },
        },
        order: [[0, "DESC"]],
        pageLength: 200,
        columns: [
        {data: "created_at", className: "", orderable: true},
        {data: "qty", className: "text-center", orderable: true},
            {"targets": 2,
            "data": "type",
            "className": "text-center",
            "render": function ( data, type, row, meta ) {
                var txt ='';
                if(data == 'cancle'){
                  txt = 'ยกเลิกออร์เดอร์'          
              }else if(data == 'append'){
                  txt = 'เพิ่มจำนวนสินค้าเพิ่มเติม'   
              }else if(data == 'cut'){
                  txt = 'ลดจำนวนสินค้าลง'   
              }else if(data == 'sell'){
                  txt = 'ขายแล้ว'   
              }else if(data == 'add'){
                  txt = 'สินค้าใหม่'   
              }else if(data == 'cart'){
                  txt = 'จอง'   
              }else if(data == 'draw'){
                  txt = 'เบิก'   
              }else if(data == 'uncart'){
                  txt = 'คืนสินค้า'   
              }

              return txt;
          }

      },
      {data: "action", className: "text-center", orderable: false},
      ],
      createdRow: function ( row, data, index ) {
        console.log(data)
        if ( data.type == 'cancle' ) {
            $('td', row).addClass('table-secondary');
        }else if(data.type == 'append'){
            $('td', row).addClass('table-success'); 
        }else if(data.type == 'cut'){
            $('td', row).addClass('table-secondary'); 
        }else if(data.type == 'sell'){
            $('td', row).addClass('table-danger');   
        }else if(data.type == 'add'){
           $('td', row).addClass('table-info'); 
        }else if(data.type == 'cart'){
           $('td', row).addClass('table-warning'); 
        }else if(data.type == 'draw'){
           $('td', row).addClass('table-primary'); 
        }
 }

}).on('draw', function () {
    $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
    $('.tb-check-single').iCheck(iCheckboxOpt);
}).on('processing', function(e, settings, processing) {
    if ( processing ) {
        $('#overlay-box').removeClass('hidden');
    } else {
        $('#overlay-box').addClass('hidden');
    }
}).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
    $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
    $('.tb-check-single').iCheck(iCheckboxOpt);
});
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
