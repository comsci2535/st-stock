
"use strict";
$(document).ready(function () {

  //  alert(1);
    
    dataList.DataTable({
       // language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {'csrfToken': get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 100,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "descript", className: "", orderable: true},
            {data: "createDate", width: "130px", className: "", orderable: true},
            {data: "updateDate", width: "130px", className: "", orderable: true},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
