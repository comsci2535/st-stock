"use strict";

$(document).ready(function () {

    // form validate editor
    $('form').each(function () {
      if ($(this).data('validator'))
        $(this).data('validator').settings.ignore = ".note-editor *";
    });

    //zun
    $.Thailand({
      database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#district'), // input ของตำบล
        $amphoe: $('#amphoe'), // input ของอำเภอ
        $province: $('#province'), // input ของจังหวัด
        $zipcode: $('#zipcode'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
          console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
          console.log(data);
        }
      });

    $.Thailand({
      database: $('#datajson').val(),
    autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
    $district: $('#district_vat'), // input ของตำบล
    $amphoe: $('#amphoe_vat'), // input ของอำเภอ
    $province: $('#province_vat'), // input ของจังหวัด
    $zipcode: $('#zipcode_vat'), // input ของรหัสไปรษณีย์
    onComplete: function(data) {
     console.log('Autocomplete is ready!')
   },
   onDataFill: function(data){
    console.log(data);
  }
});


  $('.m-view').hide()
   if($("#invoice").is(':checked')){
    $('.m-view').show()
  }


  $('#invoice').click(function(event) {
    if($(this).is(':checked')){
      $('.m-view').show()  
    }else{
     $('.m-view').hide()  
   }
 });

  $('.m-view-sup').hide()
   if($("#payment").is(':checked')){
    $('.m-view-sup').show()
  }


  $('#payment').click(function(event) {
    if($(this).is(':checked')){
      $('.m-view-sup').show()  
    }else{
     $('.m-view-sup').hide()  
   }
 });


  $('.m-view-vat').show()  
  if($("#dupicate").is(':checked')){
    $('.m-view-vat').hide()
  }  

  $('#dupicate').click(function(event) {
   if($(this).is(':checked')){
    $('.m-view-vat').hide()  
  }else{
   $('.m-view-vat').show()  
 }
});
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
