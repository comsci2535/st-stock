"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            'product_color[]': {
                required:true
            },
            'product_price_cost[]': {
                required:true
            },
            'product_price_sell[]': {
                required:true
            },
            'product_quantity[]': {
                required:true
            }
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

   //fileinput;

    if(method == 'create'){
        set_fileinput();
    }

    if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:20240,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showCancel:false,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }

    $(document).on('click','#btn-detail-add', function(){
		var html 	= '';
		var n 		= ($('.more-details').length+1)-1;
        html +='<div class="row bg-light p-3 mt-4 more-details" id="tr-'+n+'">';
        html +='<div class="col-sm-10">';
        html +='<div class="row">';
        html +='<div class="col-sm-6">';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">สี</label>';
        html +='<input value="" type="text" class="form-control m-input m-input--square product_color" name="product_color['+n+']" placeholder="ระบุสี" required>';
        html +='</div>';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">จำนวน</label>';
        html +='<input value="" type="text" class="form-control m-input m-input--square product_quantity amount" name="product_quantity['+n+']" placeholder="ระบุจำนวน" required>';
        html +='</div>';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">บาร์โค้ด</label>';
        html +='<input value="" type="text" class="form-control m-input m-input--square barcode" maxlength="15" placeholder="บาร์โค้ด 15 ตัวอักษร" name="barcode['+n+']" required>';
        html +='</div>';
        html +='</div>';
        html +='<div class="col-sm-6">';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">ราคาต้นทุน</label>';
        html +='<input value="" type="text" class="form-control m-input m-input--square product_price_cost amount" name="product_price_cost['+n+']" placeholder="ระบุราคาต้นทุน" required>';
        html +='</div>';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">ราคาขาย</label>';
        html +='<input value="" type="text" class="form-control m-input m-input--square product_price_sell amount" name="product_price_sell['+n+']" placeholder="ระบุราคาขาย">';
        html +='</div>';
        html +='</div>';
        html +='<div class="col-sm-12 mt-3">';
        html +='<div class="form-group m-form__group">';
        html +='<label for="">รูปภาพ</label>';
        html +='<input name="file_'+n+'" type="file" class="file_create" data-preview-file-type="text">';
        html +='</div>';
        html +='</div>';
        html +='</div>';
        html +='</div>';
        html +='<div class="col-sm-2 align-self-center">';
        html +='<div class="form-group m-form__group">';
        html +='<button type="button" class="btn btn-danger btn-sm btn-detail-delete" data-id="tr-'+n+'"><i class="fa fa-trash"></i></button>';
        html +='</div>';
        html +='</div>';
        html +='</div>';

        $('.list-count').append(html);
        set_fileinput();
    });

    /** ลบรายละเอียด */
    $(document).on('click','.btn-detail-delete', function(){
		$('div.more-details#'+$(this).attr('data-id')).remove();
        
	    var n = 0;
		$("div.more-details").each(function(){
			$(this).attr('id', 'tr-'+n);
			$(this).find('.btn-detail-delete').attr('data-id', 'tr-'+n);
			$(this).find('.file_create').attr('name', 'file_'+n);
			$(this).find('.product_color').attr('name', 'product_color['+n+']');
			$(this).find('.product_price_cost').attr('name', 'product_price_cost['+n+']');
			$(this).find('.product_price_sell').attr('name', 'product_price_sell['+n+']');
			$(this).find('.product_quantity').attr('name', 'product_quantity['+n+']');
			$(this).find('.barcode').attr('name', 'barcode['+n+']');
			n++;
		})
    });
    
})

function set_fileinput(){
    $('.file_create').fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        showCancel:false,
        required: true,
        showPreview:true,
        initialPreviewAsData: true,
        maxFileSize:20240,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        showCancel:false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


