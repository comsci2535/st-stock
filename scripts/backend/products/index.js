"use strict";
$(document).ready(function () {

    dataList.DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[0, "asc"]],
        pageLength: 10,
        columns: [
        {data: "checkbox", width: "20px", className: "text-center", orderable: false},
        {data: "img", className: "text-center",  width: "100px", orderable: false},
        {data: "title", className: "", width: "200px", orderable: false},
        {data: "brand", className: "", orderable: false},
        {data: "price_cost", width: "100px", className: "text-right", orderable: false},
        {data: "price_sell", width: "100px", className: "text-right", orderable: false},
        {data: "quantity", width: "100px", className: "text-center", orderable: false},
        {data: "input", width: "s35px", className: "text-center", orderable: false},
        {data: "created_at", width: "120px", className: "", orderable: false},
        {data: "updated_at", width: "120px", className: "", orderable: false},
        {data: "active", width: "50px", className: "text-center", orderable: false},
        {data: "action", width: "30px", className: "text-center", orderable: false},
        ]  
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

$(document).on('click', '.barcode-multi', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint-check" action="'+baseUrlfull+''+controller+'/barcodemulti" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint-check').submit();
    } else {
        alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ');
    }
})


$(document).on('click', '.btn-add-cart-open-modal', function (e) {
    var id= $(this).data('id');
    var max = $(this).data('max');
    console.log("id1:"+id)
    var mymodal = $('#cart_modal').modal('show');
    mymodal.find('.modal-body #cart-value-label').text(max)
    mymodal.find('.modal-body input#cart-value').attr('max',max)
    mymodal.find('.modal-body input#cart-max').val(max)
    mymodal.find('.modal-body input#cart-id').val(id)
});

$(document).on('click', '#btn-cart-send', function () {
        var id = $('#cart-id').val();
        var cart = $('#cart-value').val();
        var max = $('#cart-max').val();
        var sell_name = $('#sell-name').val();
        var customer_name = $('#customer-name').val();
        var row = "text-"+id;

        if(parseInt(cart.replace(",", "")) > parseInt(max.replace(",", ""))){
          toastr["error"]("คุณกรอกสินค้าเกินจำนวนที่มีอยู่", "");
          $('#'+row).find('.text-cart').focus();
          return false;  
        }
        if(cart == '' || cart < 0){
            toastr["error"]("คุณกรอกจำนวนสินค้าไม่ถูกต้อง", "");
            $('#cart-value').focus();
            return false;
        }
        if(sell_name == ''){
            toastr["error"]('กรุณากรอกชื่อของท่าน');
            $('#sell-name').focus();
            return false;
        }
        if(customer_name == ''){
            toastr["error"]('กรุณากรอกข้อมูลลูกค้า');
            $('#customer-name').focus();
            return false;
        }

        var qty = parseInt(max.replace(",", "")) - parseInt(cart.replace(",", ""));
       
        $.ajax({
            url: controller+"/action/cart",
            type: 'POST',
            data: {
                id : id,
                balance :qty,
                cart_qty : parseInt(cart.replace(",", "")),
                mode : 'cart',
                sell_name : sell_name,
                customer_name : customer_name,
                csrfToken:get_cookie('csrfCookie')
            },
            success: function(result){
                if (result.success === true) {
                    $('#span-'+row).text(qty)
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                    $('#btn-'+id).removeClass('btn-info').addClass('btn-pink');
                    $('#'+row).find('.btn-add-cart').attr('disabled', 'true');
                }
                $('#cart_modal').modal('hide')

            }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

});



$(document).on('click', '.btn-add-cart', function () {

    var qty = 0;
    var id       = $(this).attr('data-id');
    var row      = $(this).attr('data-row-id');
    var cart = $('#'+row).find('.text-cart').val();
    var max = $('#'+row).find('.text-cart').attr('max');

    if(cart == ''){
        toastr["error"]("กรุณากรอกข้อมูล","");
        $('#'+row).find('.text-cart').focus();
        return false;
    }

    max = max.replace(",", "");
    cart = cart.replace(",", "");

    if(parseInt(cart) > parseInt(max)){
      toastr["error"]("คุณกรอกสินค้าเกินจำนวนที่มีอยู่", "");
      $('#'+row).find('.text-cart').focus();
      return false;  
    }
     if(parseInt(cart) < 0){
      toastr["error"]("คุณกรอกสินค้าจำนวนไม่ถูกต้อง", "");
      $('#'+row).find('.text-cart').focus();
      return false; 
    }
    qty = parseInt(max)-parseInt(cart); 
    $('#span-'+row).text(qty)
    
  $.ajax({
    url: controller+"/action/cart",
    type: 'POST',
    data: {
        id : id,
        balance : qty,
        cart_qty : cart,
        mode : 'cart',
        csrfToken:get_cookie('csrfCookie')
    },
    success: function(result){
        if (result.success === true) {
            toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
            $('#'+row).find('.btn-add-cart').removeClass('btn-info').addClass('btn-pink');
            $('#'+row).find('.btn-add-cart').attr('disabled', 'true');
        }

    }
}).fail(function () {
    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
});

})
