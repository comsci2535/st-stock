"use strict";
$(document).ready(function () {

    load_datatable(); // load datable

    $(document).on('click','#btn-search-order', function(){
        load_datatable();
    });

    $(document).on('click','#btn-search-cancel', function(){
        $('input[name="dateRange"]').val('');
        $('input[name="createStartDate"]').val('');
        $('input[name="createEndDate"]').val('');
        load_datatable();
    });

    // create Daterange
    $('input[name="dateRange"]').daterangepicker({
        locale: {
            // cancelLabel: 'ยกเลิก',
            //  applyLabel:'ตกลง',
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "อา",
            "จ",
            "อ",
            "พ",
            "พฤ",
            "ศ",
            "ส"
           ],
           "monthNames": [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
          ],
        },
        showDropdowns: true,
        autoApply : true,
        autoUpdateInput: false,
    })
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' ถึง ' + picker.endDate.format('DD/MM/YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')
    });

    function load_datatable(){
        var createDateRange = $('input[name="dateRange"]').val();
        var createStartDate = $('input[name="createStartDate"]').val();
        var createEndDate   = $('input[name="createEndDate"]').val();
        var dataList = $('#data-list').DataTable();
        dataList.destroy();
        dataList = $('#data-list').DataTable({
            language: {url: "scripts/backend/Thai.json"},
            serverSide: true,
            processing: true,
            "scrollX": true,
            ajax: {
                url: controller+"/data_index",
                type: 'POST',
                data: {
                    csrfToken    : get_cookie('csrfCookie'),
                    createStartDate  : createStartDate,
                    createEndDate  : createEndDate,
                    createDateRange : createDateRange
                },
            },
            order: [[0, "asc"]],
            pageLength: 100,
            columns: [
                {data: "title", className: "", orderable: true},
                {data: "brand", className: "", orderable: false},
                {data: "product_price_cost", className: "text-right", orderable: false},
                {data: "product_price", className: "text-right", orderable: false},
                {data: "count_product", className: "text-center", orderable: false},
                {data: "total", className: "text-right", orderable: false},
                {data: "vat", className: "text-center", orderable: false},
                {data: "buy", className: "text-center", orderable: true},
                {data: "profit", className: "text-center", orderable: false},
            ]
        }).on('draw', function () {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        }).on('processing', function(e, settings, processing) {
            if ( processing ) {
                $('#overlay-box').removeClass('hidden');
            } else {
                $('#overlay-box').addClass('hidden');
            }
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        });
    }

})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

$(document).on("change","#m_datepicker_2", function () {
    dataList.DataTable().draw()
})
