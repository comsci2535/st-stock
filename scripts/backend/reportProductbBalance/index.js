"use strict";
$(document).ready(function () {
    load_datatable();
    // dataList.DataTable({
    //     language: {url: "scripts/backend/Thai.json"},
    //     serverSide: true,
    //     ajax: {
    //         url: controller+"/data_index",
    //         type: 'POST',
    //         data: {csrfToken:get_cookie('csrfCookie')},
    //     },
    //     order: [[1, "asc"]],
    //     pageLength: 10,
    //     columns: [
    //         // {data: "img", className: "", orderable: true},
    //         {data: "title", className: "", orderable: true},
    //         {data: "brand", className: "", orderable: true},
    //         {data: "price_cost", className: "", orderable: true},
    //         {data: "price_sell", className: "", orderable: true},
    //         {data: "quantity", className: "", orderable: true},
    //     ]
    // }).on('draw', function () {
    //     $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
    //     $('.tb-check-single').iCheck(iCheckboxOpt);
    // }).on('processing', function(e, settings, processing) {
    //     if ( processing ) {
    //         $('#overlay-box').removeClass('hidden');
    //     } else {
    //         $('#overlay-box').addClass('hidden');
    //     }
    // })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


$(document).on('click','#btn-search-order', function(){
    load_datatable();
});

$(document).on('click','#btn-search-cancel', function(){
    $('#pro_title').val('');
    $("#catalog_id").val(null).trigger('change');
    load_datatable();
});

function load_datatable(){
    var pro_title   =  $('#pro_title').val();
    var catalog_id  =  $("#catalog_id option:selected").val();

    var dataList = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        language: {url: "scripts/backend/Thai.json"},
        searching: false,
        serverSide: true,
        processing: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {
                csrfToken   : get_cookie('csrfCookie'),
                title       : pro_title,
                catalog_id  : catalog_id,
            },
        },
        order: [[0, "desc"]],
        pageLength: 10,
        columns: [
            {data: "title", className: "", orderable: false},
            {data: "brand", className: "", orderable: false},
            {data: "price_cost", className: "text-right", orderable: false},
            {data: "price_sell", className: "text-right", orderable: false},
            {data: "quantity", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
            $('.statusPicker').selectpicker({width: '100%'});
        }
    })
}
