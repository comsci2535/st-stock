"use strict";
$(document).ready(function () {

    load_datatable(); // load datable

    $(document).on('click','#btn-search-order', function(){
        load_datatable();
    });

    $(document).on('click','#btn-search-cancel', function(){
        $('input[name="dateRange"]').val('');
        $('input[name="createStartDate"]').val('');
        $('input[name="createEndDate"]').val('');
        $("#order_status").val(null).trigger('change');
        load_datatable();
    });

    // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { 
            // cancelLabel: 'ยกเลิก',
            //  applyLabel:'ตกลง', 
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
               ],
               "monthNames": [
               "มกราคม",
               "กุมภาพันธ์",
               "มีนาคม",
               "เมษายน",
               "พฤษภาคม",
               "มิถุนายน",
               "กรกฎาคม",
               "สิงหาคม",
               "กันยายน",
               "ตุลาคม",
               "พฤศจิกายน",
               "ธันวาคม"
              ],
        },
        autoApply : true,
        autoUpdateInput: false,
        showDropdowns: true,
    }) 

    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' ถึง ' + picker.endDate.format('DD/MM/YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    });


    function load_datatable(){
        var createDateRange = $('input[name="dateRange"]').val();
        var createStartDate = $('input[name="createStartDate"]').val();
        var createEndDate   = $('input[name="createEndDate"]').val();
        var order_status    = $('#order_status option:selected').val();
        var dataList = $('#data-list').DataTable();
        dataList.destroy();
        dataList = $('#data-list').DataTable({
            language: {url: "scripts/backend/Thai.json"},
            serverSide: true,
            processing: true,
            "scrollX": true,
            ajax: {
                url: controller+"/data_index",
                type: 'POST',
                data: {
                    csrfToken    : get_cookie('csrfCookie'),
                    createStartDate  : createStartDate,
                    createEndDate  : createEndDate,
                    createDateRange : createDateRange,
                    order_status : order_status
                },
            },
            order: [[1, "desc"]],
            pageLength: 100,
            columns: [
                {data: "no",width: "5%", className: "text-center", orderable: false},
                {data: "order_code",width: "10%", className: "", orderable: true},
                {data: "fullname",width: "20%", className: "", orderable: true},
                {data: "tel", width: "13%", className: "", orderable: false},
                {data: "total_list",width: "5%", className: "text-center", orderable: false},
                {data: "total", width: "5%", className: "text-center", orderable: false},
                {data: "updated_at", width: "10%", className: "text-center", orderable: false},
                {data: "active", width: "12%", className: "text-center", orderable: false},
                {data: "input", width: "20%", className: "text-center", orderable: false},
                ]
            }).on('draw', function () {
                $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
                $('.tb-check-single').iCheck(iCheckboxOpt);
            }).on('processing', function(e, settings, processing) {
                if ( processing ) {
                    $('#overlay-box').removeClass('hidden');
                } else {
                    $('#overlay-box').addClass('hidden');
                    $('.statusPicker').selectpicker({width: '100%'});
                }
            })
    }

    $(window).on("load", function () {
    })

    $(window).on("scroll", function () {
    })

    $(document).on('click', '.text-tracking', function () {
        
    });
    
    $(document).on('click', '.btn-add-tracking', function () {
        var id       = $(this).attr('data-id');
        var row      = $(this).attr('data-row-id');
        var tracking = $('#'+row).find('.text-tracking').val();
        if(tracking == ''){
            alert('กรุณากรอกข้อมูล');
            $('#'+row).find('.text-tracking').focus();
            return false;
        }
        $.ajax({
            url: controller+"/action/tracking",
            type: 'POST',
            data: {
                id : id,
                tracking_code : tracking,
                mode : 'tracking',
                csrfToken:get_cookie('csrfCookie')
            },
            success: function(result){
                if (result.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                    $('#'+row).find('.btn-add-tracking i').removeClass('fa-save').addClass('fa-check');
                    $('#'+row).find('.btn-add-tracking').removeClass('btn-primary').addClass('btn-success');
                }
                
            }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

    })

});
