"use strict";
$(document).ready(function () {

    load_datatable(); // load datable

    $(document).on('change','.statusPicker', function(){
        if(jQuery.type($(this).data('method')) != "undefined"){
            var style
            $('#overlay-box').removeClass('hidden');
            switch ($(this).val())
            { 
                case "0" : style = 'btn-default'; break; //ยังไม่ขาย
                case "1" : style = 'btn-warning'; break;
                case "2" : style = 'btn-success'; break; 
                case "3" : style = 'btn-danger'; break; //ขายแล้ว
                default : style = 'btn-default'; 
            }   

            $(this).selectpicker('setStyle', 'btn-danger btn-warning btn-success btn-info btn-default', 'remove')
            $(this).selectpicker('setStyle', style)
            $(this).selectpicker('setStyle', 'btn-flat', 'add')
            arrayId.push($(this).data('id'))
            var url = $(this).data('method');
            var status = $(this).val();

            //change status
            if(status == 3)
              select_status(url,status)
          else
              send_data(url,status)   
      }   
    })

    $(document).on('click','#btn-search-order', function(){
        load_datatable();
    });

    $(document).on('click','#btn-search-cancel', function(){
        
        $("#order_month").val(null).trigger('change');
        $("#order_status").val(null).trigger('change');
        load_datatable();
    });

     // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { 
            // cancelLabel: 'ยกเลิก',
            //  applyLabel:'ตกลง', 
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
               ],
               "monthNames": [
               "มกราคม",
               "กุมภาพันธ์",
               "มีนาคม",
               "เมษายน",
               "พฤษภาคม",
               "มิถุนายน",
               "กรกฎาคม",
               "สิงหาคม",
               "กันยายน",
               "ตุลาคม",
               "พฤศจิกายน",
               "ธันวาคม"
              ],
        },
        autoApply : true,
        autoUpdateInput: false,
        showDropdowns: true,
    })   
    $('input[name="dateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' ถึง ' + picker.endDate.format('DD/MM/YYYY'))
        $(this).nextAll().eq(0).val(picker.startDate.format('YYYY-MM-DD'))
        $(this).nextAll().eq(1).val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="dateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $(this).nextAll().eq(0).val('')
        $(this).nextAll().eq(1).val('')        
    });

    $(document).on('change', '.calendar.left .monthselect', function () {
        $('input[name="dateRange"]').val('');
        $('input[name="createStartDate"]').val('')
        $('input[name="createEndDate"]').val('') 
    }); 

})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


function load_datatable(){

    var order_year      = $('#order_year option:selected').val();
    var order_month     = $('#order_month option:selected').val();
    var order_day     = $('#order_day option:selected').val();
    var order_status    = $('#order_status option:selected').val();
    var dataList        = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        language: {url: "scripts/backend/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {
                csrfToken    : get_cookie('csrfCookie'),
                order_year   : order_year,
                order_month  : order_month,
                order_day    : order_day,
                order_status : order_status
            },
        },
        order: [[1, "ASC"]],
        pageLength: 500,
        columns: [
        {data: "checkbox", width: "5%", className: "text-center", orderable: false},
        {data: "order_code",width: "10%", className: "", orderable: true},
        {data: "fullname",width: "20%", className: "", orderable: true},
        {data: "tel", width: "15%", className: "", orderable: false},
        {data: "total_list",width: "5%", className: "text-center", orderable: false},
        {data: "total", width: "5%", className: "text-center", orderable: false},
        {data: "vat", width: "5%", className: "text-center", orderable: false},
        {data: "buy", width: "5%", className: "text-center", orderable: false},
        {data: "updated_at", width: "10%", className: "text-center", orderable: true},
        {data: "active", width: "15%", className: "text-center", orderable: false},
        {data: "tracking", width: "10%", className: "", orderable: true},
        //{data: "action", width: "10%", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    });
    
}

var arrayId = [];
$(document).on('click', '.multi-print', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint" action="'+baseUrlfull+''+controller+'/printboxmultiple" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint').submit();
    } else {
        alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ');
    }
});

$(document).on('click', '.multi-print-check', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint-check" action="'+baseUrlfull+''+controller+'/printcheckmultiple" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint-check').submit();
    } else {
        alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ');
    }
})

$(document).on('click', '.multi-print-checkandbox', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint-check" action="'+baseUrlfull+''+controller+'/printvat" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint-check').submit();
    } else {
        alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ');
    }
})



