
var WizardDemo=function() {
    $("#m_wizard");
    var e,
    r,
    i=$("#m_form");
    return {
        init:function() {
            var n;

            var x = true;
            var y = true;


            $("#m_wizard"),
            i=$("#m_form"),
            (r=new mWizard("m_wizard", {
                startStep: 1
            }
            )).on("beforeNext", function(r) {

                var product = $("#table-product tbody tr").length;
                $("#table-product tbody tr .input-number").each(function(){
                    //console.log($(this).val())
                    if($(this).val() < 1){
                        x = false
                    }else{
                        x = true
                    }
                });

                $("#table-product tbody tr .product_price").each(function(){
                    var str = $(this).val();
                    //console.log(parseInt(str.replace(',','')))
                    if((isNaN(parseInt(str.replace(',','')))) || parseInt(str.replace(',','')) < 1 || str == ''){
                        y = false
                    }else{
                        y = true
                    }                    
                    
                });
                
                
                if((x == false || product == 0) || (y == false)){
                    !0 !== false && r.stop()
                    //alert('กรุณาระบุราคาสินค้า');
                    toastr["error"]("กรุณาระบุราคาสินค้าและจำนวนสินค้า", "");
                }else{
                    !0 !== e.form() && r.stop()
                }
                //!0!==e.form()&&r.stop()
            }
            ),
            r.on("change", function(e) {
                mUtil.scrollTop()
            }
            ),
            r.on("change", function(e) {
                1===e.getStep()
            }
            ),
            e=i.validate( {
                ignore:":hidden", 
                rules: {
                    vat_code:{
                        required : true,
                    },
                    'product_price_sell[]': {
                        required: true
                    },
                    districts_name :
                    {
                        required:function(){
                         if($('#district').val() > 0 ){
                             return false;
                         }else{
                            return true;
                        }
                    }
                },
                amphures_name :
                {
                    required:function(){
                     if($('#amphoe').val() > 0 ){
                         return false;
                     }else{
                        return true;
                    }
                }
            },
            provinces_name :{
                required:function(){
                 if($('#province').val() > 0 ){
                     return false;
                 }else{
                    return true;
                }
            }
        },
        zip_code:{
            required:function(){
             if($('#zipcode').val() > 0 ){
                 return false;
             }else{
                return true
            }
        }
    },
    districts_name_vat :
    {
        required:function(){
         if($('#district_vat').val() > 0 ){
             return false;
         }else{
            return true;
        }
    }
},
amphures_name_vat :
{
    required:function(){
     if($('#amphoe_vat').val() > 0 ){
         return false;
     }else{
        return true;
    }
}
},
provinces_name_vat :{
    required:function(){
     if($('#province_vat').val() > 0 ){
         return false;
     }else{
        return true;
    }
}
},
zip_code_vat:{
    required:function(){
     if($('#zipcode_vat').val() > 0 ){
         return false;
     }else{
        return true;
    }
}
},


}
, messages: {
    "account_communication[]": {
        required: "You must select at least one communication option"
    }
    , accept: {
        required: "You must accept the Terms and Conditions agreement!"
    }
}
, invalidHandler:function(e, r) {
    mUtil.scrollTop(), swal( {
        title: "", text: "There are some errors in your submission. Please correct them.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
    }
    )
}
, submitHandler:function(e) {
    console.log($('#zip_code').val())
    return false;
}
}
),
            (n=i.find('[data-wizard-action="submit"]')).on("click", function(r) {
                r.preventDefault(), e.form()&&(mApp.progress(n),
                   i.ajaxSubmit( {
                    success:function(data) {
                        mApp.unprogress(n)
                        console.log(data)
                        if(data == 'error'){
                            alert_box("จำนวนสินค้าที่ใส่ไปเป็น 0")
                        }else{
                            //location.reload();
                            location.href = "orders";
                        }
                        
                    },
                }
                ))
                //$('#m_form').submit();
            }
            )
        }
    }
}

();
jQuery(document).ready(function() {
    WizardDemo.init()
}

);